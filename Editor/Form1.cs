﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Editor
{
    public partial class Form1 : Form
    {
        string currentPath;

        SQLiteConnection db_Graphics_Connection;
        string db_GraphicsRelativePath;

        System.Windows.Forms.Timer timer;
        Image ImageString;
        Image StripColor;
        Image[] Animation;
        int cur = 0;
        int begin = 0;
        int end = 0;
        int width = 32;
        int speed = 100;
        bool program_change = false;
        bool is_saved = false;
        PictureBox Strip;

        public Form1()
        {
            InitializeComponent();

            currentPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            
            try
            {
                string text = System.IO.File.ReadAllText("DBpaths.txt");
                
                int startIndex = text.IndexOf("Graphics:") + "Graphics:".Length;
                db_GraphicsRelativePath = text.Substring(startIndex).Split('\n')[0];
                //MessageBox.Show(db_GraphicsRelativePath); //debug
            }
            catch (Exception)
            {
                //MessageBox.Show("File with paths to databases not found!");
                State.Text = "File with paths to databases not found!";
                return;
            }

            textBoxSpeed.Text = "100";

            timer = new System.Windows.Forms.Timer();
            timer.Interval = Convert.ToInt32(textBoxSpeed.Text);
            timer.Tick += new EventHandler(timer_Tick);
            
            width = 32;

            db_Graphics_Connection = new SQLiteConnection("Data Source=Graphics.sqlite;Version=3;");
            db_Graphics_Connection.Open();

            string sql = "select SpriteSheet from Mobs";
            SQLiteCommand command = new SQLiteCommand(sql, db_Graphics_Connection);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                listBoxRows.Items.Add(reader["SpriteSheet"]);
            }


            sql = "select * from Mobs";
            command = new SQLiteCommand(sql, db_Graphics_Connection);
            reader = command.ExecuteReader();
            for(var i = 2; i < reader.FieldCount; i++)
            {
                listBoxColumns.Items.Add(reader.GetName(i));
            }
            
            db_Graphics_Connection.Close();
            State.Text = "Ok";
        }
    }
}
