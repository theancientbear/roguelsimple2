﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Editor
{
    public partial class Form1 : Form
    {
        private static Image cropImage(Image img, Rectangle cropArea, Size size)
        {
            Bitmap bmpImage = new Bitmap(img);
            Bitmap bmpCrop = bmpImage.Clone(cropArea,
            bmpImage.PixelFormat);

            Bitmap result = new Bitmap(size.Width, size.Height);
            using (Graphics g = Graphics.FromImage(result))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                g.DrawImage(bmpCrop, 0, 0, size.Width, size.Height);
            }
            return (Image)(result);
        }

        private void buttonChoose_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string filename = openFileDialog1.FileName;
            if (filename != "openFileDialog1")
            {
                ImageString = Image.FromFile(filename);

                pictureBoxString.Image = ImageString;
                int count = ImageString.Width / 32;
                textBoxFirst.Text = "0";
                textBoxLast.Text = (count - 1).ToString();

                Animation = new Image[count];
                for (int i = 0; i < count; i++)
                    Animation[i] = cropImage(ImageString, new Rectangle(i * width, 0, width, ImageString.Height), new Size(pictureBoxAnimation.Width, pictureBoxAnimation.Height));

                is_saved = false;
                show_strip();
                State.Text = "Ok";
                timer.Start();
            }
            else
                State.Text = "You didn't choose the file!";
        }

        void timer_Tick(object sender, EventArgs e)
        {
            pictureBoxAnimation.Image = Animation[cur];
            if (end - begin != 0)
                cur = begin + (cur + 1 - begin) % (end - begin);
            else
                cur = begin;
        }

        private void textBoxFirst_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int tmp = Convert.ToInt32(textBoxFirst.Text);
                if (tmp >= 0 && tmp < ImageString.Width / 32 && (tmp <= end || program_change == true))
                {
                    begin = tmp;
                    is_saved = false;
                    show_strip();
                    State.Text = "Ok";
                }
                else if (tmp > end)
                    State.Text = "Error: last is bigger than first!";
                else
                    State.Text = "Invalid argument: first!";

            }
            catch (Exception)
            {
                //MessageBox.Show("Invalid argument!");
                State.Text = "Invalid argument!";
                return;
            }
            cur = begin;
        }

        private void textBoxLast_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int tmp = Convert.ToInt32(textBoxLast.Text);
                if (tmp >= 0 && tmp < ImageString.Width / 32 && (tmp >= begin || program_change == true))
                {
                    end = tmp;
                    is_saved = false;
                    show_strip();
                    State.Text = "Ok";
                }
                else if (tmp < begin)
                    State.Text = "Error: last is bigger than first!";
                else
                    State.Text = "Invalid argument: last!";
            }
            catch (Exception)
            {
                //MessageBox.Show("Invalid argument!");
                State.Text = "Invalid argument!";
                return;
            }
            cur = begin;
        }

        private void textBoxSpeed_TextChanged(object sender, EventArgs e)
        {
            if (timer != null)
            {
                timer.Stop();
                try
                {
                    int tmp = Convert.ToInt32(textBoxSpeed.Text);
                    if (tmp > 0)
                    {
                        timer.Interval = tmp;
                        speed = tmp;
                        is_saved = false;
                        show_strip();
                        State.Text = "Ok";
                    }
                    else
                        State.Text = "Invalid arguement: speed <= 0!";
                }
                catch (Exception)
                {
                    //MessageBox.Show("Invalid argument!");
                    State.Text = "Invalid argument!";
                    return;
                }
                timer.Tick += new EventHandler(timer_Tick);
                if (Animation != null)
                    timer.Start();
            }

        }

        private void show_strip()
        {
            try
            {
                if (is_saved)
                    StripColor = Image.FromFile("green.png");
                else
                    StripColor = Image.FromFile("yellow.png");
            }
            catch (Exception)
            {
                //MessageBox.Show("File with color not found!");
                State.Text = "File with color not found!";
                return;
            }

            if(Strip != null)
            {
                //remove from form
                this.Controls.Remove(Strip);
                //release memory by disposing
                Strip.Dispose();
            }

            //MessageBox.Show(end.ToString() + " " + begin.ToString());
            int strip_width = (end - begin + 1) * 32;
            int relative_begin = begin * 32 + panel1.AutoScrollPosition.X;
            Point loc = new System.Drawing.Point(relative_begin, 0);
            Strip = new PictureBox
            {
                Name = "pictureBoxStrip",
                Size = new System.Drawing.Size(strip_width, 5),
                SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage,
                Location = loc,
                Image = StripColor,
                Anchor = System.Windows.Forms.AnchorStyles.Top,

            };
            panel1.Controls.Add(Strip);

        }
        private void buttonSave_Click(object sender, EventArgs e)
        {
            db_Graphics_Connection.Open();
            try
            {
                string row = listBoxRows.SelectedItem.ToString();
                string column = listBoxColumns.SelectedItem.ToString();
                string string_to_save = begin.ToString() + "-" + end.ToString() + ":" + speed.ToString();
                string filename = openFileDialog1.SafeFileName;
                string sql = "update Mobs set " + column + " = '" + string_to_save + "' where SpriteSheet = '" + row + "'";

                //MessageBox.Show(sql); //debug

                SQLiteCommand command = new SQLiteCommand(sql, db_Graphics_Connection);
                command.ExecuteNonQuery();
                State.Text = "Saved";
                is_saved = true;
                show_strip();
            }

            catch (Exception)
            {
                //MessageBox.Show("You must select the place to save!");
                State.Text = "You must select the place to save!";
                db_Graphics_Connection.Close();
                return;
            }
            db_Graphics_Connection.Close();

        }

        private void buttonAddRow_Click(object sender, EventArgs e)
        {
            db_Graphics_Connection.Open();
            try
            {
                string filename = openFileDialog1.SafeFileName;
                string sql = "";
                if (filename != "openFileDialog1")
                {
                    sql = "insert into Mobs (Spritesheet, Size) values ('Sprites/" + filename + "', '32')";
                    //MessageBox.Show(sql); //debug
                    SQLiteCommand command = new SQLiteCommand(sql, db_Graphics_Connection);
                    command.ExecuteNonQuery();
                    State.Text = "Added";
                }
                else
                    State.Text = "First choose the spritesheet!";
            }
            catch (Exception)
            {
                //MessageBox.Show("Error!");
                State.Text = "Error with adding to DB!";
                db_Graphics_Connection.Close();
                return;
            }
            db_Graphics_Connection.Close();
        }

        private void listBoxRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string db_GraphicsPath = currentPath + db_GraphicsRelativePath;
                // MessageBox.Show(currentPath);    debug
                string safe_filename = listBoxRows.SelectedItem.ToString();
                safe_filename = safe_filename.Replace('/', '\\');
                string full_filename = db_GraphicsPath + "\\" + safe_filename;
                ImageString = Image.FromFile(full_filename);
                //ImageString = StripColor;

                pictureBoxString.Image = ImageString;
                int count = ImageString.Width / 32;

                textBoxFirst.Text = "0";
                textBoxLast.Text = (count - 1).ToString();

                Animation = new Image[count];
                for (int i = 0; i < count; i++)
                    Animation[i] = cropImage(ImageString, new Rectangle(i * width, 0, width, ImageString.Height), new Size(pictureBoxAnimation.Width, pictureBoxAnimation.Height));

                is_saved = true; //it's from DB, OF COURSE IT'S SAVED
                show_strip();
                timer.Start();
                State.Text = "Ok";

            }
            catch(Exception)
            {
                //MessageBox.Show("File not found!");
                State.Text = "File not found!";
                return;
            }

        }
        private void listBoxColumns_SelectedIndexChanged(object sender, EventArgs e)
        {
            db_Graphics_Connection.Open();
            try
            {
                string column = listBoxColumns.SelectedItem.ToString();
                string row = listBoxRows.SelectedItem.ToString();
                string sql = "select " + column + " from Mobs where SpriteSheet = '" + row + "'";
                //MessageBox.Show(sql); //debug
                SQLiteCommand command = new SQLiteCommand(sql, db_Graphics_Connection);
                SQLiteDataReader reader = command.ExecuteReader();
   
                string tmp;
                if(reader.Read())
                {
                    tmp = reader[column].ToString();
                    //MessageBox.Show(tmp); debug
                    if(tmp != "")
                    {
                        program_change = true;
                        textBoxFirst.Text = tmp.Split('-')[0]; //begin
                        textBoxLast.Text = tmp.Split('-')[1].Split(':')[0]; //end
                        textBoxSpeed.Text = tmp.Split('-')[1].Split(':')[1]; //speed
                        program_change = false;
                        is_saved = true; //it's from DB, OF COURSE IT'S SAVED
                        show_strip();
                    }
                }

                State.Text = "Ok";
            }
            catch (Exception)
            {
                //MessageBox.Show("Error1!");
                State.Text = "Error while reading DB!";
                db_Graphics_Connection.Close();
                return;
            }
            db_Graphics_Connection.Close();
        }
        private void pictureBoxString_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            if (me.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point coordinates = me.Location;
                int tmp = coordinates.X / 32;
                if(tmp <= end || program_change == true)
                {
                    begin = tmp;
                    textBoxFirst.Text = begin.ToString();
                    //is_saved = false;
                    //show_strip();
                    //State.Text = "Ok";
                }
                else if (tmp > end)
                    State.Text = "Error: last is bigger than first!";

            }
            if (me.Button == System.Windows.Forms.MouseButtons.Right)
            {
                Point coordinates = me.Location;
                int tmp = coordinates.X / 32;
                if (tmp >= begin || program_change == true)
                {
                    end = tmp;
                    textBoxLast.Text = end.ToString();
                    //is_saved = false;
                    //show_strip();
                    //State.Text = "Ok";
                }
                else if (tmp < begin)
                    State.Text = "Error: last is bigger than first!";
            }
        }
    }
}

