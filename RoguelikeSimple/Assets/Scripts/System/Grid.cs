﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Grid : MonoBehaviour {

   public Dictionary<IntegerVector, Tile> PointToTile = new Dictionary<IntegerVector, Tile>();
   public Dictionary<Tile, IntegerVector> TileToPoint = new Dictionary<Tile, IntegerVector>();
    public Dictionary<IntegerVector, FogOfWar> Fog = new Dictionary<IntegerVector, FogOfWar>();
    public GameObject FogPrefab;
	// Use this for initialization
	void Start () {
      
	
	}


    public void CreateFog()
    {
        foreach(IntegerVector vec in PointToTile.Keys)
            Fog.Add(vec, (Instantiate(FogPrefab, (vec.ToVector3() + 2 * Vector3.back), new Quaternion(0, 0, 0, 0)) as GameObject).GetComponent<FogOfWar>());
        Player.Main.CheckView();
    }


    public List<IntegerVector> GetRing(IntegerVector pos, int rad)
    {
        List<IntegerVector> output = new List<IntegerVector>();
        IntegerVector point = new IntegerVector(0, rad);
        IntegerVector point2 = new IntegerVector(0, -rad);
        int delta = 1 - 2 * rad;
        int error = 0;
        while(point.y>=0)
        {
            output.Add(pos + point);
            output.Add(pos - point);
            output.Add(pos + point2);
            output.Add(pos - point2);
            error = 2 * (delta + point.y) - 1;
            if ((delta < 0) && (error <= 0))
            {
                point = new IntegerVector(point.x + 1, point.y);
                point2 = new IntegerVector(point.x, -point.y);
                delta += 2 * point.x + 1;
                continue;
            }
            error = 2 * (delta - point.x) - 1;
            if ((delta > 0) && (error > 0))
            {
                point = new IntegerVector(point.x, point.y-1);
                point2 = new IntegerVector(point.x, -point.y);
                delta += 1 - 2 * point.y;
                continue;

            }
            point = new IntegerVector(point.x + 1, point.y);
            point2 = new IntegerVector(point.x, -point.y);
            delta += 2 * (point.x - point.y);
            point = new IntegerVector(point.x, point.y - 1);
            point2 = new IntegerVector(point.x, -point.y);
            

        }

        return output;

    }


    List<Vector3> cubes = new List<Vector3>();
    private void OnDrawGizmos()
    {
        for (int i = 0; i < cubes.Count; i++)
        {
            Gizmos.DrawCube(cubes[i], Vector3.one);
            //Debug.Log("DrawnPoint");

        }
    }
    public List<IntegerVector> CheckVisibility(IntegerVector pos, int rad)
    {
        List<IntegerVector> Circle;
        if (rad > 2)
        {
           Circle = GetRing(pos, rad - 1);
            int l = Circle.Count;
            for(int i = 0; i<l; i++)
            {
                Circle.Add(Circle[i] + new IntegerVector(1, 0));
                Circle.Add(Circle[i] + new IntegerVector(-1, 0));
                Circle.Add(Circle[i] + new IntegerVector(0, -1));
                Circle.Add(Circle[i] + new IntegerVector(0, 1));
            }
        }
        else
        Circle =  GetRing(pos, rad);

        List<IntegerVector> output = new List<IntegerVector>();
        foreach(IntegerVector i in Circle)
        {
            bool over = false;
            float angle = Vector3.Angle((pos - i).ToVector3(), Vector3.right) * Mathf.Deg2Rad * Mathf.Sign(Mathf.Sin(Vector3.Angle((pos-i).ToVector3(), Vector3.right)));
            for (int j = 0; Mathf.Abs(j) < Mathf.Abs(pos.x - i.x) - 1; j += (int)Mathf.Sign(pos.x - i.x))
            {

                for (float k = 0; Mathf.Abs(k) <= Mathf.Abs(Mathf.Tan(angle) * (float)j); k += 1 * Mathf.Sign(pos.y - i.y))
                {

                    output.Add(new IntegerVector(pos.x + j, pos.y + (int)k));
                    try
                    {
                        if (!PointToTile[new IntegerVector(pos.x + j, pos.y + (int)k)].Transparent)
                        {
                            over = true;
                            break;
                            
                        }
                    }
                    catch
                    {

                        over = true;
                        break;
                    }
                    
                }
                if (over)
                    break;


            }
            over = false;

            angle = Vector3.Angle((pos - i).ToVector3(), Vector3.up) * Mathf.Deg2Rad * Mathf.Sign(Mathf.Sin(Vector3.Angle((pos - i).ToVector3(), Vector3.up)));
            for (int j = 0; Mathf.Abs(j) < Mathf.Abs(pos.y - i.y) - 1; j += (int)Mathf.Sign(pos.y - i.y))
            {

                for (float k = 0; Mathf.Abs(k) <= Mathf.Abs(Mathf.Tan(angle) * (float)j); k += 1 * Mathf.Sign(pos.x - i.x))
                {

                    output.Add(new IntegerVector(pos.x + (int)k, pos.y + j));
                    try
                    {
                        if (!PointToTile[new IntegerVector(pos.x + (int)k, pos.y + j)].Transparent)
                        {
                            over = true;
                            break;
                        }
                    }
                    catch
                    {
                        over = true;
                        break;
                    }

                }
                if (over)
                    break;

            }

        }


        return output;
    }


    public void ResetFog()
    {

        for (int i = 0; i < Fog.Values.Count; i++)
            Destroy(Fog.Values.ToList<FogOfWar>()[i].gameObject);
        Fog.Clear();
    }


    public void ChangeVisibility(List<IntegerVector> vecs)
    {
        foreach (FogOfWar i in Fog.Values)
            if (i.CurrentState == FogOfWar.State.Visible)
                i.CurrentState = FogOfWar.State.Known;
        foreach(IntegerVector i in vecs)
                if (Fog.ContainsKey(i) && PointToTile.ContainsKey(i))
                    Fog[i].CurrentState = FogOfWar.State.Visible;

    }


    public void ChangeVisibility(IntegerVector center, int radius)
    {
        foreach (FogOfWar i in Fog.Values)
            if (i.CurrentState == FogOfWar.State.Visible)
                i.CurrentState = FogOfWar.State.Known;
        for(int i = -radius;i<= radius;i++)
        {
            for(int k = -radius;k<= radius;k++)
            {
                if (Fog.ContainsKey(center + new IntegerVector(i, k))&&PointToTile.ContainsKey(center + new IntegerVector(i, k)))
                    Fog[center + new IntegerVector(i, k)].CurrentState = FogOfWar.State.Visible;
            }

        }
    }

    public Tile GetTile(IntegerVector vec)
    {
        return PointToTile[vec];
    }
	
    public IntegerVector GetPoint(Tile til)
    {
        return TileToPoint[til];
    }


    public void CreateTile(TileData til, IntegerVector vec)
    {

    }

    public void InsertTile(IntegerVector vec, Tile til)
    {
        try {
            Destroy(PointToTile[vec].gameObject);
        }
        catch
        {

        }
            
        
        PointToTile[vec] = til;
        TileToPoint[til] = vec;
        til.transform.position = vec.ToVector3();
    }


    public bool CheckPassability(IntegerVector a)
    {
        try
        {
            return PointToTile[a].Passable;
        }
        catch
        {

        }

        return false;
    }

    public void Clear()
    {
        List<IntegerVector> list = PointToTile.Keys.ToList<IntegerVector>();
        foreach (IntegerVector k in list)
        {
            Destroy(PointToTile[k].gameObject); 

        }
        PointToTile.Clear();
        TileToPoint.Clear();
    }


    public bool CheckPassability(IntegerVector a, IntegerVector b)
    {
        int lasty = 0;
        float angle = Vector3.Angle((b-a).ToVector3(),Vector3.right)*Mathf.Deg2Rad;
     
        for(int i = (int)Mathf.Sign(b.x - a.x); Mathf.Abs(i)<Mathf.Abs(b.x-a.x)-1;i+=(int)Mathf.Sign(b.x-a.x))
        {
          
            for (float k = lasty; Mathf.Abs(k)<=Mathf.Abs(Mathf.Tan(angle)*(float)i) ; k+=1*Mathf.Sign(b.y-a.y))
            { 
                if(!CheckPassability(new IntegerVector(a.x+i,(int)k+a.y)))
                {
                    
                    return false;
                }
                lasty = (int)k;
            }

        }
        return true;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
