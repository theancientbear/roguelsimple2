﻿using UnityEngine;
using System.Collections;
using SQLite;

public struct IntegerVector
{
    public int x;
    public int y;
  
   public IntegerVector(int a, int b)
    {
        x = a;
        y = b;
    }

   public Vector3 ToVector3()
    {
        return new Vector3(x, y);

    }

    public static IntegerVector ConvertFromVector3(Vector3 vec)
    {
        return new IntegerVector((int)vec.x, (int)vec.y);
    }

    public  static IntegerVector operator+(IntegerVector a, IntegerVector b)
    {
        return new IntegerVector(a.x + b.x, a.y + b.y);
    }
    public static IntegerVector operator -(IntegerVector a, IntegerVector b)
    {
        return new IntegerVector(a.x - b.x, a.y - b.y);
    }

    public static IntegerVector operator*(IntegerVector a, int b)
    {

        return new IntegerVector(b * a.x, b * a.y);
    }
    public static IntegerVector operator /(IntegerVector a, int b)
    {

        return new IntegerVector( a.x/b,  a.y/b);
    }
    public int Trace ()
    {
        
        return x*x+y* y;
    }
    public  static bool operator==(IntegerVector a, IntegerVector b)
    {
        if (a.x == b.x && a.y == b.y)
            return true;
        else
            return false;
    }
    public static bool operator !=(IntegerVector a, IntegerVector b)
    {
        if (a.x == b.x && a.y == b.y)
            return false;
        else
            return true;
    }


    public override bool Equals(object obj)
    {


        if (!(obj is IntegerVector))
            return false;
        IntegerVector b = (IntegerVector)obj;
        if (b == this)
            return true;
        else
            return false;
    }

    
    public override int GetHashCode()
    {
        return x*1000+y;
    }

    public static int Distance(IntegerVector a, IntegerVector b)
    {
        return (int)Mathf.Max(Mathf.Abs(a.x - b.x), Mathf.Abs(a.y - b.y));
    }



}



public class FloorMobData
{
   public string ID { get; set; }
   public float Chance { get; set; }
}

public class ItemData
{
    
    public string ID{ get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
    public string Description { get; set; }
    public string Effect { get; set; }
    public string Sprite { get; set; }
    public string Slots { get; set; }
    public bool Multislot { get; set; }
    public int Strength { get; set; }
    public int Agility { get; set; }
    public int Wisdom { get; set; }
 

}

public class WeaponData:ItemData
{
   
    public int MinDamage { get; set; }
    public int MaxDamage { get; set; }
    public string DamageType { get; set; }
    public int Range { get; set; }
    public string Projectile { get; set; }
    public string DisplaySprite { get; set; }
    public string AltDisplaySprite { get; set;}
}



public class GenericItem:ItemData
{

}



public class FloorItemData
{
   public string ItemId { get; set; }
    public float Chance { get; set; }


}




public class LevelData
{
   public string Tileset {
        get; set; }
   public int Chance
    {
        get; set;
    }
    public string Itemset
    {
        get; set;
    }
    public string Mobset
    {
        get; set;
    }
    public int MobAmountAv
    {
        get; set;
    }
    public int MobAmountDelta
    {
        get; set;
    }
    public int Rooms
    {
        get; set;
    }
    public int RoomsDelta
    {
        get; set;
    }
    public int Roomsize
    {
        get; set;
    }
    public int RoomsizeDelta
    {
        get; set;
    }
    public int CorrWidth
    {
        get; set;
    }
    public int ItemNumber
    {
        get; set;
    }

    public int AvChestNumber { get; set; }
    public int DeltaChestNumber { get; set; }
    public string ChestSet { get; set; }



}

public class MobAnimData
{

    public string SpriteSheet
    {
        get; set;
    }

    public int Size
    {
        get; set;
    }

    public string Idle
    {
        get; set;
    }
    public string Walk
    {
        get; set;
    }
    public string Attack
    {
        get; set;
    }
    public string Death
    {
        get; set;
    }
    public string Corpse
    {
        get; set;
    }



    public string Ranged
    {
        get; set;
    }

    public string Action1
    {
        get; set;
    }
    public string Action2
    {
        get; set;
    }
    public string Action3
    {
        get; set;
    }
    public string Action4
    {
        get; set;
    }
    public string Action5
    {
        get; set;
    }
    public string Action6
    {
        get; set;
    }
    public string Action7
    {
        get; set;
    }
    public string Action8
    {
        get; set;
    }
    public string Action9
    {
        get; set;
    }
    public string Action10
    {
        get; set;
    }
    public string Action11
    {
        get; set;
    }

    public string Action12
    {
        get; set;
    }

}




public class EffectData
{

    public string Name
    {
        get; set;
    }
    public string ID
    {
        get; set;
    }
    public string Sprite
    {
        get; set;
    }
    public string Type
    { get; set; }
    public string Grade1
    {
        get; set;
    }
    public string Grade2
    {
        get; set;
    }
    public string Grade3
    {
        get; set;
    }

    public string Grade4
    {
        get; set;
    }

    public string Grade5
    {
        get; set;
    }

    public string Grade6
    {
        get; set;
    }

    public string Grade7
    {
        get; set;
    }

    public string Grade8
    {
        get; set;
    }
}




public class SoundSequence
{
    public string Action { get; set; }
    public string SoundList { get; set; }



}

public class TileData
{

    public string Sprite { get; set; }
    public string Type { get; set; }
}

public class CreatureData
{
    public string ID { get; set; }
    public string Name { get; set; }
    public int HP { get; set; }
    public int MinDamage { get; set; }
    public int MaxDamage { get; set; }
    public string DamageType { get; set; }
    public  string Effect { get; set; }
    public string AI { get; set; }
    public string Sprite { get; set; }
    public string LootTable { get; set; }
    public string Description { get; set; }
    public int Range { get; set; }
    public string Projectile { get; set; }
    public string Params { get; set; }
}


public class ArmorData:ItemData
{

    public string DescrType { get; set; }
    public int MaxBlock { get; set; }
    public int MinBlock { get; set; }
    public float DodgeBonus { get; set; }
    public string DisplaySprite { get; set; }



}


public class CharacterData
{
    public string Name { get; set; }
    public string Sprite { get; set; }
    public string SoundPack { get; set; }
    public int Strength { get; set; }
    public int Agility { get; set; }
    public int Intellect { get; set; }
    public string Slot1 { get; set; }
    public string Slot2 { get; set; }
    public string Slot3 { get; set; }
    public string Slot4 { get; set; }
    public string Slot5 { get; set; }
    public string Slot6 { get; set; }
    public string Slot7 { get; set; }
    public string Slot8 { get; set; }
    public string Effect { get; set; }
    public string History { get; set; }




}



public class DefAnimData
{
    public int Slot { get; set; }
    public string SpriteSheet { get; set; }




}



public class SoundData
{


    public string Id { get; set; }
    public string Path { get; set; }
}


public class FloorChestData
{
    public string ID { get; set; }
    public float Chance { get; set; }
}

public class ChestData
{


    public string id { get; set; }
    public string Sprite { get; set; }
    public string LootTable { get; set; }
}



public class DecorationData
{
    public string id { get; set; }
    public string Type { get; set; }
    public string Sprite { get; set; }
    public string LootTable { get; set; }
    public bool Destructable { get; set; }
    public bool Passable { get; set; }
    public string UseText { get; set; }
    public string Effect { get; set; }




}



public static class Extensions
{
    public static IntegerVector ToVectorInteger(this Vector3 a)
    {
        return new IntegerVector((int)a.x, (int)a.y);
    }
    public static IntegerVector ToVectorInteger(this Vector2 a)
    {
        return new IntegerVector((int)a.x, (int)a.y);
    }
}

