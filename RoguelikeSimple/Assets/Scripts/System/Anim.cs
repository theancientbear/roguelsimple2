﻿using UnityEngine;
using System.Collections;

public class Anim  {


    public float period;

    public bool Loop;
    public Sprite[] sprites;

    string Pars = "";
    public Anim(string p, float per, Sprite[] spr)
    {
        Pars = p;
        period = per;
        sprites = spr;

    }


   


    public virtual string SaveFormat()
    {
        string output = "";
        output += sprites[0].texture.name +"|";
        output += Pars;


        return output;
    }
   

}
