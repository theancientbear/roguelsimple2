﻿using UnityEngine;
using System.Collections;

public interface IOnNextTurn  {

    void OnNextTurn();
}
