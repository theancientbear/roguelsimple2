﻿using UnityEngine;

using System.Collections.Generic;
using System.Linq;


public class Astar  {

    class NodeComparer : IComparer<Node>
    {
        int IComparer<Node>.Compare(Node a, Node b)
        {
            return a.dist - b.dist;
        }
    }

    class Node
    {
       public IntegerVector coords;
        public bool Done = false;
        public Node Previous;
        public int estLength;
        public int dist;

 
        public List<Node> GetNeigh()
        {
            List<Node> Neigh = new List<Node>();
            for (int j = -1; j <= 1; j++)
                for (int k = -1; k <= 1; k++)
                {
                    if (Core.MainGrid.CheckPassability(new IntegerVector(coords.x + j, coords.y + k)))
                        Neigh.Add(new Node() { coords = new IntegerVector(coords.x + j, coords.y + k), Previous = this, estLength = this.estLength + 1,dist = (new IntegerVector(coords.x + j, coords.y + k)-Astar.ennd).Trace() });

                    if (new IntegerVector(coords.x + j, coords.y + k) == Astar.ennd)
                        return new List<Node> { new Node() { coords = new IntegerVector(coords.x + j, coords.y + k), Previous = this, estLength = this.estLength + 1, dist = (new IntegerVector(coords.x + j, coords.y + k) - Astar.ennd).Trace() } };
                }
            return Neigh;
        }

    }

    public static IntegerVector ennd;
    public static int MaxTrace;

    public static List<IntegerVector> FindThePath(IntegerVector start, IntegerVector end, int mt = 10000)
    {

        int OpenLength = 0;
        Node currNode = new Node();
        MaxTrace = mt;
        bool FoundPath = false;
       Astar.ennd = end;
        Node startN = new Node()
        {
            coords = start,
            Previous = null,
            Done = false,
            estLength = 0,
            dist = (start- end).Trace()

        };
        List<Node> close = new List<Node>();
        List<Node> open = new List<Node>();
        open.Add(startN);
        OpenLength++;
        List<Node> path = new List<Node>();
        

        while (open.Count>0)
        {

            open.Sort(new NodeComparer());
           currNode =open[0];
            if (currNode.coords == end)
            {
                FoundPath = true;
                break;
            }
            open.Remove(currNode);
            OpenLength--;
            close.Add(currNode);
            foreach(var Neigh in currNode.GetNeigh())
            {
                //if (close.Count(nd => nd.coords == Neigh.coords)>0)
                //    continue;
                bool a = false;
                Node opnd =null;
                foreach (Node nd in close)
                    if (nd.coords == Neigh.coords)
                    {
                       a = true;
                        break;
                    }
                if (a)
                    continue;

                foreach (Node nd in open)
                    if (nd.coords == Neigh.coords)
                    {
                        opnd = nd;
                       
                        break;
                    }

                if (opnd == null)
                {
                    if((Neigh.coords- start).Trace()<MaxTrace)
                    open.Add(Neigh);
                }
                else
                    if (opnd.estLength > Neigh.estLength)
                {
                    opnd.Previous = currNode;
                    opnd.estLength = Neigh.estLength;
                }

            }

           
                

        }
        if (FoundPath)
        {
            List<IntegerVector> result = new List<IntegerVector>();
            while (currNode != null)
            {
                result.Add(currNode.coords);
                currNode = currNode.Previous;
                
            }
            result.Reverse();
            result.RemoveAt(0);
            return result;
        }

        return null;
    }


    
}
