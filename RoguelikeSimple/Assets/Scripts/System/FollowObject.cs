﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour {

    public Transform aim;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.gameObject != null)
            transform.position = aim.position;
        else
            GameObject.Destroy(this.gameObject);
	}
}
