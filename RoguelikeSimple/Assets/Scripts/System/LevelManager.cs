﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SQLite;
public static class LevelManager {


    public static void Load(string Level)
    {

        string[] Data = Level.Split(';');

        //-----------------
        //Loading Tileset
        //-----------------
        string[] tileset= Data[0].Split(',');
        Material[] materials = new Material[tileset.Length];
        for(int i =0; i<materials.Length;i++)
        {
            Material m = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
            m.name = tileset[i];
            if(tileset[i].StartsWith("anim:"))
            {
                m.mainTexture = Core.GetTextureFromFile(tileset[i].Replace("anim:", ""));
                m.mainTextureScale = new Vector2((float)m.mainTexture.height / (float)m.mainTexture.width, 1);
            }
            else
            {
                m.mainTexture = Core.GetTextureFromFile(tileset[i]);
            }

            materials[i] = m;
        }



        //--------------------------------------
        //Loading Map
        //---------------------------------------------
        string[] LayoutData = Data[1].Split(',');

        foreach(string tiledata in LayoutData)
        {
            string[] tiledatadata = tiledata.Split('#');
            Tile t = (GameObject.Instantiate(Core.Main.tileprefab) as GameObject).GetComponent<Tile>();
            t.GetComponent<Renderer>().material = materials[int.Parse(tiledatadata[0])];

            //Core.MainGrid.Fog.Add(vec, (GameObject.Instantiate(Core.MainGrid.FogPrefab, vec.ToVector3(), new Quaternion) as GameObject).GetComponent<FogOfWar>());
            t.Transparent = bool.Parse(tiledatadata[3]);
            t.Passable = bool.Parse(tiledatadata[3]);
            Core.MainGrid.InsertTile(new IntegerVector(int.Parse(tiledatadata[1].Split('|')[0]), int.Parse(tiledatadata[1].Split('|')[1])), t);
            if (tiledatadata[4] == "l")
                t.gameObject.AddComponent< LadderUpTile > ();
            if(tiledatadata[5]!="null")
            {

                List<Effect> efs = Effect.Parse(tiledatadata[5]);
                foreach(Effect ef in efs)
                {
                    t.AddEffect(ef);
                }
            }
                
           
        }



        //--------------------------------------------------------
        //Loading Mobs
        //-------------------------------------------------------



        string[] mobdata = Data[2].Split(',');
        foreach (string mob in mobdata)
            CreateMob(mob);


        //-----------------
        //Loading Items
        //-------------------


        string[] itemdata = Data[3].Split(',');
        foreach (string it in itemdata)
            LoadItem(it);



        //----------
        //Loading Decorations
        //---------------------
                string[] decordata = Data[4].Split(',');
        foreach (string it in decordata)
            LoadDecoration(it);



    }





    public static string GetSaveCode()
    {



        //----------------------------------------------------------
        //Saving tileset
        //---------------------------------------------------------
        string result = "";
        List<string> tileset = new List<string>();
        foreach (Tile t in Core.MainGrid.TileToPoint.Keys)
        {
            if (t != null)
            {
                string h = t.GetComponent<Renderer>().material.name;
                if (!tileset.Contains(h))
                    tileset.Add(h);
            }
        }



        for (int i = 0; i < tileset.Count - 1; i++)
            result += tileset[i].Replace(" (Instance)", "") + ",";
        result += tileset[tileset.Count - 1].Replace(" (Instance)", "") + ";\n";

        //---------------------------------------------------
        //Saving Map
        //-------------------------------------------------------



        foreach (Tile t in Core.MainGrid.TileToPoint.Keys)
        {
            if (t != null)
            {

                string temp = "";
                for (int i = 0; i < tileset.Count; i++)
                    if (t.GetComponent<Renderer>().material.name.Contains(tileset[i]))
                    {
                        temp += i.ToString();
                        break;
                    }
                temp += "#";
                temp += Core.MainGrid.TileToPoint[t].x.ToString() + "|" + Core.MainGrid.TileToPoint[t].y.ToString() + "#";
                temp += t.Passable.ToString() + "#" + t.Transparent.ToString() + "#";
                if (t.GetComponent<LadderUpTile>() == null)
                    temp += "t";
                else
                    temp += "l";

                if (t.effects.Count > 0)
                {
                    temp += "#";
                    if (t.effects.Count > 1)
                        for (int k = 0; k < t.effects.Count - 1; k++)
                        {
                            if (t.effects[k] != null)
                                temp += t.effects[k].SaveFormat() + "@";
                        }
                    if (t.effects[t.effects.Count - 1] != null)
                        temp += t.effects[t.effects.Count - 1].SaveFormat();
                    else
                        temp.Remove(temp.Length - 1);
                }
                else
                {
                    temp += "#null";
                }
                temp += ",";
                result += temp;


            }
        }
        result = result.Remove(result.Length - 1);
        result += ";";

        //---------------------------------------------------------------
        // Saving mobs
        // ---------------------------------------------------------------
        BasicMob[] mobarray = GameObject.FindObjectsOfType<BasicMob>() as BasicMob[];

        foreach (BasicMob m in mobarray)
            result += m.SaveFormat() + ",";
        if (result[result.Length - 1] == ',')
            result = result.Remove(result.Length - 1);
        result += ";";



        //---------------------------------------------------------------
        // Saving items
        // ---------------------------------------------------------------

        Item[] ItemArray = GameObject.FindObjectsOfType<Item>() as Item[];
        foreach (Item i in ItemArray)
            if (i.Owner == null)
                result += i.SaveFormat() + ",";
        if (result[result.Length - 1] == ',')
            result = result.Remove(result.Length - 1);
        result += ";";




        //-------------------------
        // Saving Decorations
        //-------------------------
        Decoration[] DecorArray = GameObject.FindObjectsOfType<Decoration>() as Decoration[];

        foreach (Decoration i in DecorArray)
                result += i.SaveFormat() + ",";
        if (result[result.Length - 1] == ',')
            result = result.Remove(result.Length - 1);

        result += ";";

        return result;

    }






        public static Effect LoadEffect(string efdata)
    {
        return null;
    }

    public static BasicMob LoadMob(string mobdata)
    {
        return null;
    }

    public static Item LoadItem(string itdata)
    {
        string ItType = itdata.Split('#')[0];
        switch(ItType)
        {

            case "Weapon":
                {
                    Weapon s = (GameObject.Instantiate(Core.Main.ItemPrefab, Vector3.zero, Core.Main.ItemPrefab.transform.rotation) as GameObject).AddComponent<Weapon>();
                    s.LoadFormat(itdata);
                    return s;
                    break;
                }

            case "Armor":
                {
                    Armor s = (GameObject.Instantiate(Core.Main.ItemPrefab, Vector3.zero, Core.Main.ItemPrefab.transform.rotation) as GameObject).AddComponent<Armor>();
                    s.LoadFormat(itdata);
                    return s;
                    break;
                }
            case "Item":
                {
                    Item s = (GameObject.Instantiate(Core.Main.ItemPrefab, Vector3.zero, Core.Main.ItemPrefab.transform.rotation) as GameObject).AddComponent<Item>();
                    s.LoadFormat(itdata);
                    return s;
                    break;
                }

        }

        return null;

    }


    /*
     *         string output = "BasicMob#"+Coords.x.ToString()+"#"+Coords.y.ToString()+"#"
     *         +MinDamage.ToString()+"#"+MaxDamage.ToString()+"#
     *         "+Health.ToString()+"#"+
            MaxHealth.ToString()+"#"+Range.ToString()+"#"+WeaponType+"#"+LootTable+"#
            " + animations[0].sprites[0].texture.name+"#";
     */


    public static BasicMob CreateMob(string mobdata)
    {

        string[] data = mobdata.Split('#');

        CreatureData i = new CreatureData();
        i.MinDamage = int.Parse(data[3]);
        i.MaxDamage= int.Parse(data[4]);
        i.HP = int.Parse(data[6]);
        i.Range = int.Parse(data[7]);
        i.DamageType = data[8];
        i.LootTable = data[9];
        i.AI = data[0];
        i.Sprite = data[10];
        BasicMob d=null;
        IntegerVector coords = new IntegerVector(int.Parse(data[1]), int.Parse(data[2])); 
            switch (i.AI)
            {
                case "BasicMob":
                    {

                        BasicMob c = (GameObject.Instantiate(Core.Main.MobPrefab, coords.ToVector3(), new Quaternion(0, 0, 0, 0)) as GameObject).AddComponent<BasicMob>();
                        c.MaxHealth = i.HP;
                        c.name = i.Name;
                        c.ID = i.ID;
                        c.Health = i.HP;
                        c.MaxDamage = i.MaxDamage;
                        c.MinDamage = i.MinDamage;
                        c.WeaponType = i.DamageType;
                        c.LootTable = i.LootTable;
                        c.name = i.Name;
                        Texture2D m = Core.GetTextureFromFile(i.Sprite);
                        MobAnimData animData;
                        c.Range = i.Range;

                        using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
                        {
                            animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + i.Sprite + "'")[0];

                        }


                        c.animations[0] = Core.ImportMobAnim(m, animData.Idle, animData.Size, true);
                        c.animations[1] = Core.ImportMobAnim(m, animData.Walk, animData.Size, false);
                        c.animations[2] = Core.ImportMobAnim(m, animData.Attack, animData.Size, false);
                        c.animations[3] = Core.ImportMobAnim(m, animData.Death, animData.Size, false);
                        c.animations[4] = Core.ImportMobAnim(m, animData.Corpse, animData.Size, true);
                        if (animData.Ranged != null)
                            c.animations[5] = Core.ImportMobAnim(m, animData.Ranged, animData.Size, false);

                        c.animated = true;
                    d = c;
                    //c.gameObject.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m, new Rect(0, 0, m.width, m.height), new Vector2(0.5f, 0.5f));
                    break;
                    }

                case "BoltMob":
                    {
                        BoltMob c = (GameObject.Instantiate(Core.Main.MobPrefab, coords.ToVector3(), new Quaternion(0, 0, 0, 0)) as GameObject).AddComponent<BoltMob>();
                        c.MaxHealth = i.HP;
                        c.ID = i.ID;
                        c.Health = i.HP;
                        c.MaxDamage = i.MaxDamage;
                        c.MinDamage = i.MinDamage;
                        c.LootTable = i.LootTable;
                        c.name = i.Name;
                        string[] pars = data;
                    /*
        string output = base.SaveFormat().Replace("BasicMob","BoltMob")+ "#" + BoltCharges.ToString()+"#"+
    BoltRange.ToString()+"#"+BoltReload.ToString()+"#"+BoltRLD.ToString()+"#"+MaxBoltDamage.ToString()+"#"+MinBoltDamage.ToString()+"#"+BoltProjectile.texture.name+"#"+
    DamageType.ToString()+"#"+ExplosionEffect+"#"+ExplosionRadius.ToString();
                     * */
                    c.BoltCharges = int.Parse(pars[12]);
                        c.BoltRange = int.Parse(pars[13]);
                        c.BoltReload = int.Parse(pars[14]);
                    c.BoltRLD= int.Parse(pars[15]);
                    c.MaxBoltDamage = int.Parse(pars[16]);
                        c.MinBoltDamage = int.Parse(pars[17]);
                        Texture2D txt = Core.GetTextureFromFile(pars[18]);
                        c.BoltProjectile = Sprite.Create(txt, new Rect(0, 0, txt.width, txt.height), new Vector2(0.5f, 0.5f));
                        c.DamageType = pars[19];
                        c.ExplosionRadius = int.Parse(pars[21]);
                        c.ExplosionEffect = pars[20];



                        /*
                         *     public int BoltCharges = 2;
    public int BoltRange = 3;
    public int BoltReload = 4;
    private int BoltRLD = 0;
    public int MaxBoltDamage=0;
    public int MinBoltDamage;
    public Sprite BoltProjectile;
    public string DamageType;
    public int ExplosionRadius;
    public string ExplosionEffect;
    */
                        Texture2D m = Core.GetTextureFromFile(i.Sprite);
                        MobAnimData animData;
                        c.Range = i.Range;

                        using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
                        {
                            animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + i.Sprite + "'")[0];

                        }


                        c.animations[0] = Core.ImportMobAnim(m, animData.Idle, animData.Size, true);
                        c.animations[1] = Core.ImportMobAnim(m, animData.Walk, animData.Size, false);
                        c.animations[2] = Core.ImportMobAnim(m, animData.Attack, animData.Size, false);
                        c.animations[3] = Core.ImportMobAnim(m, animData.Death, animData.Size, false);
                        c.animations[4] = Core.ImportMobAnim(m, animData.Corpse, animData.Size, true);
                        if (animData.Ranged != null)
                            c.animations[5] = Core.ImportMobAnim(m, animData.Ranged, animData.Size, false);
                    d = c;
                        c.animated = true;
                    //c.gameObject.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m, new Rect(0, 0, m.width, m.height), new Vector2(0.5f, 0.5f));
                    break;
                    }

                default:
                    break;
            }

        if(d!=null)
        d.Health = int.Parse(data[5]);

        List<Effect> effects = Effect.Parse(data[11]);
        foreach (Effect ef in effects)
            d.AddEffect(ef);
        

        return d;
    }


    public static Decoration LoadDecoration(string info)
    {
       
                if(info.Split('#')[0]=="Chest")
                {
                Chest c = (GameObject.Instantiate(Core.Main.ChestPrefab, Vector3.zero, new Quaternion(0, 0, 0, 0)) as GameObject).GetComponent<Chest>();
                c.LoadFormat(info);
                return c;
                }
      
        return null;
    }



}
