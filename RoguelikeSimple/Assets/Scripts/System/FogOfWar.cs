﻿using UnityEngine;
using System.Collections;

public class FogOfWar : MonoBehaviour {

public enum State
        {
        Unknown,
        Known,
        Visible

         }
    public Material Unknown;
    public Material Known;

    public bool NeedsChange;

   private State CurrState;
    public State CurrentState
    {
        get { return CurrState; }
        set { NeedsChange = true; CurrState = value; }

    }

    public void Update()
    {

        if(NeedsChange)
        switch(CurrState)
        {
            case State.Unknown:
                NeedsChange = false;
                this.GetComponent<Renderer>().enabled = true;
                this.GetComponent<Renderer>().material = Unknown;
                break;

            case State.Known:
                NeedsChange = false;
                this.GetComponent<Renderer>().enabled = true;
                this.GetComponent<Renderer>().material = Known;
                break;
            case State.Visible:
                NeedsChange = false;
                    this.GetComponent<Renderer>().enabled = false;
                    break;
        }

    }

}
