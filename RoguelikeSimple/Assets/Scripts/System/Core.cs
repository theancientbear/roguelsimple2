﻿using UnityEngine;
using System.Collections.Generic;
using SQLite;
using System;
using System.IO;
using System.Text;

using UnityEngine.UI;






public class Core : MonoBehaviour {

    public static Core Main;
    public GameObject BasicMob;
    public GameObject MobPrefab;
    public GameObject DeathScreen;
    public int TurnCounter;
    public bool GamePause = false;
    public delegate void TurnContainer();
    public event TurnContainer OnNextTurn;
    public static Grid MainGrid;
    public GameObject tileprefab;
    public bool OnInterface = false;
    public GameObject textprefab;
    public Image LoadScreen;
    public List<Item> Itemlist = new List<Item>();
    List<string> texts = new List<string>();
    List<Vector3> vecs = new List<Vector3>();
    List<Color> colors = new List<Color>();
    public Canvas WorldCanvas;
    public GameObject HealthBarPrefab;
    public GameObject ProjectilePref;
    public GameObject ItemPrefab;
    public int ScheduledTurns = 0;
    public static CharacterData mainCharacter;
    public GameObject ChestPrefab;
    public bool Editing;
    public Dictionary<string, Texture2D> TexturePool = new Dictionary<string, Texture2D>();

    public static int FloorNum = 1;
    public bool SelectionInProgress = false;
    public bool CoolDown;
    public GameObject FollowingSpritePrefab;
    public bool Freezed = false;
    public List<Effect> WaitingForSelection = new List<Effect>();
    public List<IOnNextTurn> NextTurn = new List<IOnNextTurn>();
    public GameObject DecorationUseButton;
    public Dictionary<string, AudioClip> AudioCollection = new Dictionary<string, AudioClip>();
    public GameObject SoundEffectPrefab;
    public GameObject DecorationPrefab;

    public bool MakingTurn = false;
    public int TurnsInOneFrame = 20000;


    public void AskForSelection(Effect ef)
    {
        Debug.Log("Asked for selection");
        WaitingForSelection.Add(ef);
        SelectionInProgress = true;
    }


    public Tile AddTile(TileData tile,IntegerVector vec,Material m)
    {
        Tile t = (GameObject.Instantiate(Core.Main.tileprefab) as GameObject).GetComponent<Tile>();

        t.GetComponent<Renderer>().material = m;

        //Core.MainGrid.Fog.Add(vec, (GameObject.Instantiate(Core.MainGrid.FogPrefab, vec.ToVector3(), new Quaternion) as GameObject).GetComponent<FogOfWar>());
        if (m.name.StartsWith("anim:"))
        {
            t.Animated = true;
            m.mainTextureScale = new Vector2((float)m.mainTexture.height / (float)m.mainTexture.width, 1);
        }

        Core.MainGrid.InsertTile(vec, t);
        return t;
    }

    public void SelectionDone(Tile t)
    {
        SelectionInProgress = false;
        foreach(Effect i in WaitingForSelection)
        {
            i.OnSelection(t);
        }
        WaitingForSelection.Clear();
        Player.Main.Action();
    }

    // Use this for initialization
    void Start() {
        using (var SoundDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Sounds.sqlite"))
        {
            List<SoundData> SoundList = SoundDB.Query<SoundData>("SELECT * FROM SoundList");
            foreach (SoundData i in SoundList)
            {

                try
                {
                    AudioCollection.Add(i.Id, GetAudioFromFile(i.Path));
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                }

            }



        }


        Main = this;
        MainGrid = this.GetComponent<Grid>();


        if (mainCharacter == null)
        {
            List<CharacterData> chars = new List<CharacterData>();
            using (var CharDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Core.sqlite"))
            {
                chars = CharDB.Query<CharacterData>("SELECT * FROM PlayerCharacters");

            }
            mainCharacter = chars[0];
        }


        if (!Editing)
        {
            LoadFloor(FloorNum);


            MakeTurn();
        }
       
        
    }



    public void CreateProjectile(Vector3 start, Vector3 end, float velocity, Sprite spr)
    {
        Projectile j = (Instantiate(ProjectilePref) as GameObject).GetComponent<Projectile>();
        j.transform.position = start;
        j.aim = end;
        j.velocity = velocity;
        j.GetComponent<SpriteRenderer>().sprite = spr;
        Quaternion rotation = Quaternion.LookRotation
            (j.aim- j.transform.position, j.transform.TransformDirection(Vector3.up));
       j.transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
        Debug.Log("Launched projectile to " + end.ToString());
    }



    public void LoadLevel(string info,bool transint = false)
    {
        if(LoadScreen!=null)
        LoadScreen.enabled = true;
        MainGrid.Clear();
        if(!Editing)
        MainGrid.ResetFog();
        NextTurn.Clear();


        FollowObject[] fs= GameObject.FindObjectsOfType < FollowObject >() as FollowObject[];
        for(int i =0; i<fs.Length; i++)
            Destroy(fs[i].gameObject);
        foreach (Creature c in FindObjectsOfType<Creature>())
        {

            if (c.GetComponent<Player>() == null)
            {
                Destroy(c.gameObject);
            }
            else
            {

                Player.Main = c as Player;
                Player.Main.Start();

            }

        }
        foreach (Item i in FindObjectsOfType<Item>())
            if (i.Owner == null)
                Destroy(i.gameObject);



        if (!Editing)
        {
            Player.Main.Coords = new IntegerVector(0, 0);
            Player.Main.Path = null;
            Player.Main.transform.position = Vector3.zero;
        }


        LevelManager.Load(info);
        if(!Editing)
        MainGrid.CreateFog();


        if (!Editing)
        {
            Player.Main.CheckView();
            Player.Main.Strength += Player.Main.StrExp / 10000;
            Player.Main.Agility += Player.Main.AgiExp / 10000;
            Player.Main.Wisdom += Player.Main.IntExp / 10000;
            Player.Main.StrExp -= (Player.Main.StrExp / 10000) * 10000;
            Player.Main.AgiExp -= (Player.Main.AgiExp / 10000) * 10000;
            Player.Main.IntExp -= (Player.Main.IntExp / 10000) * 10000;
            StartCoroutine(LoadFreeze(1));
        }

    }

    public void LoadFloor(int num)
    {
        if (LoadScreen != null)
            LoadScreen.enabled = true;
        MainGrid.Clear();
        MainGrid.ResetFog();
        NextTurn.Clear();
        foreach (Creature c in FindObjectsOfType<Creature>())
        {
            
            if (c.GetComponent<Player>() == null)
            {
                Destroy(c.gameObject);
            }
            else
            {
                Player.Main = c as Player;
                //Player.Main.Start();

            }

        }
        foreach (Item i in FindObjectsOfType<Item>())
            if (i.Owner == null)
                Destroy(i.gameObject);

        if (!Editing)
        {
            Player.Main.Coords = new IntegerVector(0, 0);
            Player.Main.Path = null;
            Player.Main.transform.position = Vector3.zero;
        }
        
        List<LevelData> levl;
        using (var LevDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Floors.sqlite"))
        {
            levl = LevDB.Query<LevelData>("SELECT * FROM Floor" + num.ToString());
        }
        Debug.Log(("Mob number: "+levl[0].MobAmountAv.ToString()));
        RoomGenerator.SetGen((int)UnityEngine.Random.Range(levl[0].Rooms, levl[0].RoomsDelta + levl[0].Rooms), levl[0].Roomsize, levl[0].RoomsizeDelta, levl[0].CorrWidth);
        RoomGenerator.LevelData = levl[0];

        RoomGenerator.GenerateMap(levl[0].Tileset, (int)UnityEngine.Random.Range(0, 10000));
        if (!Editing)
        {
            MainGrid.CreateFog();

            //MakeTurn();
          //  Player.Main.CheckView();
            Player.Main.Strength += Player.Main.StrExp / 10000;
            Player.Main.Agility += Player.Main.AgiExp / 10000;
            Player.Main.Wisdom += Player.Main.IntExp / 10000;
            Player.Main.StrExp -= (Player.Main.StrExp / 10000) * 10000;
            Player.Main.AgiExp -= (Player.Main.AgiExp / 10000) * 10000;
            Player.Main.IntExp -= (Player.Main.IntExp / 10000) * 10000;
            StartCoroutine(LoadFreeze(1));
        }

    }


    public HealthBar AddHealthBar(Creature c)
    {
        HealthBar s = (Instantiate(HealthBarPrefab) as GameObject).GetComponent<HealthBar>();
        s.transform.parent = WorldCanvas.transform;
        s.Aim = c;
        return s;
    }

    public UseDecorationButton AddDecorationButton(Decoration c)
    {
        UseDecorationButton s = (Instantiate(DecorationUseButton) as GameObject).GetComponent<UseDecorationButton>();
        s.transform.parent = WorldCanvas.transform;
        s.Owner = c;
        return s;
    }
	
    public void AddFloatText(string msg,Vector3 pos)
    {
        vecs.Add(pos);
        texts.Add(msg);
        colors.Add(Color.red);


    }
    public void AddFloatText(string msg, Vector3 pos,Color col)
    {
        vecs.Add(pos);
        texts.Add(msg);
        colors.Add(col);

    }

    public void MakeTurn()
    {
        if (!Editing)
        {
            if (!MakingTurn)
            {
                TurnCounter++;
                Player.Main.CountEffects();
                //if(OnNextTurn!=null)
                //OnNextTurn();
                objsleft = NextTurn.Count;
                MakingTurn = true;

            }
            else
            {
                ScheduledTurns++;
            }
        }
        
    }




    public static Anim ImportMobAnim(Texture2D sheet, string animData, int size, bool Loop)
    {
        string[] temp = animData.Split(':');
        string[] temp2 = temp[0].Split('-');
       

        int startframe = Convert.ToInt32(temp2[0]);
        int endframe = Convert.ToInt32(temp2[1]);
        float period = (float)Convert.ToInt32(temp[1]);
        Anim output = new Anim(animData,period, new Sprite[endframe - startframe + 1]);
       output.period = period / 1000;

        for (int k = 0; k < endframe - startframe+1; k++)
        {
            output.sprites[k] = Sprite.Create(sheet, new Rect(size * (startframe + k), 0, size, size), new Vector2(0.5f, 0.5f), 32f);

        }
        output.Loop = Loop;
        return output;
    }




    public Chest CreateChest(string ID, IntegerVector coords)
    {
        using (var EntDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Entities.sqlite"))
        {
            List<ChestData> list = EntDB.Query<ChestData>("SELECT * FROM Chests WHERE id ='" + ID + "'");
            if (list.Count > 0)
            {
                ChestData i = list[0];
                Chest c = (Instantiate(ChestPrefab, coords.ToVector3(), new Quaternion(0, 0, 0, 0)) as GameObject).GetComponent<Chest>();
                c.Coords = coords;
                c.LootTable = i.LootTable;
                Texture2D m = Core.GetTextureFromFile(i.Sprite);
                c.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m, new Rect(0, 0, m.width, m.height), new Vector2(0.5f, 0.5f), 32f);
                return c;
            }
            else
            {
                Debug.Log("Failed creating chest " + ID);
            }
        
        }
        return null;
    }


        public Decoration CreateDecoration(string ID, IntegerVector coords)
    {
        using (var EntDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Entities.sqlite"))
        {
            List<DecorationData> list = EntDB.Query<DecorationData>("SELECT * FROM Decorations WHERE id ='" + ID + "'");
            if (list.Count > 0)
            {
                DecorationData i = list[0];
                Decoration c = (Instantiate(DecorationPrefab, coords.ToVector3(), new Quaternion(0, 0, 0, 0)) as GameObject).GetComponent<Decoration>();
                c.Coords = coords;
                c.LootTable = i.LootTable;
                c.Destructable = i.Destructable;
                c.Passable = i.Passable;
                c.UseText = i.UseText;
                string[] efs = i.Effect.Split(',');
                foreach (string temp in efs)
                    c.AddEffect(CreateTierEffect(temp));
                Texture2D m = Core.GetTextureFromFile(i.Sprite);
                c.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m, new Rect(0, 0, m.width, m.height), new Vector2(0.5f, 0.5f), 32f);
                return c;
            }
            else
            {
                Debug.Log("Failed creating Decoration " + ID);
            }
        
        }
        return null;
    }


    

    public Creature CreateMob(string ID, IntegerVector coords)
    {

        using (var EntDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Entities.sqlite"))
        {
            CreatureData i = EntDB.Query<CreatureData>("SELECT * FROM Mobs WHERE ID ='" + ID + "'")[0];
            switch (i.AI)
            {
                case "BasicMob":
                    {
                       
                        BasicMob c = (Instantiate(MobPrefab, coords.ToVector3(), new Quaternion(0, 0, 0, 0)) as GameObject).AddComponent<BasicMob>();
                        c.MaxHealth = i.HP;
                        c.name = i.Name;
                        c.ID = i.ID;
                        c.Health = i.HP;
                        c.MaxDamage = i.MaxDamage;
                        c.MinDamage = i.MinDamage;
                        c.WeaponType = i.DamageType;
                        c.LootTable = i.LootTable;
                        c.name = i.Name;
                        Texture2D m = Core.GetTextureFromFile(i.Sprite);
                        MobAnimData animData;
                        c.Range = i.Range;

                        using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
                        {
                            animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + i.Sprite + "'")[0];

                        }


                        c.animations[0] = ImportMobAnim(m, animData.Idle, animData.Size, true);
                        c.animations[1] = ImportMobAnim(m, animData.Walk, animData.Size, false);
                        c.animations[2] = ImportMobAnim(m, animData.Attack, animData.Size, false);
                        c.animations[3] = ImportMobAnim(m, animData.Death, animData.Size, false);
                        c.animations[4] = ImportMobAnim(m, animData.Corpse, animData.Size, true);
                        if (animData.Ranged != null)
                            c.animations[5] = ImportMobAnim(m, animData.Ranged, animData.Size, false);

                        c.animated = true;

                        //c.gameObject.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m, new Rect(0, 0, m.width, m.height), new Vector2(0.5f, 0.5f));
                        return c;
                    }

                case "BoltMob":
                    {
                         BoltMob c= (Instantiate(MobPrefab, coords.ToVector3(), new Quaternion(0, 0, 0, 0)) as GameObject).AddComponent<BoltMob>();
                        c.MaxHealth = i.HP;
                        c.ID = i.ID;
                        c.Health = i.HP;
                        c.MaxDamage = i.MaxDamage;
                        c.MinDamage = i.MinDamage;
                        c.LootTable = i.LootTable;
                        c.name = i.Name;
                        string[] pars = i.Params.Split(',');

                        c.BoltCharges = int.Parse(pars[0]);
                        c.BoltRange = int.Parse(pars[1]);
                        c.BoltReload = int.Parse(pars[2]);
                        c.MaxBoltDamage = int.Parse(pars[3]);
                        c.MinBoltDamage= int.Parse(pars[4]);
                        Texture2D txt = Core.GetTextureFromFile(pars[5]);
                        c.BoltProjectile = Sprite.Create(txt, new Rect(0, 0, txt.width, txt.height), new Vector2(0.5f, 0.5f));
                        c.DamageType = pars[6];
                        c.ExplosionRadius = int.Parse(pars[7]);
                        c.ExplosionEffect = pars[8];



                        /*
                         *     public int BoltCharges = 2;
    public int BoltRange = 3;
    public int BoltReload = 4;
    private int BoltRLD = 0;
    public int MaxBoltDamage=0;
    public int MinBoltDamage;
    public Sprite BoltProjectile;
    public string DamageType;
    public int ExplosionRadius;
    public string ExplosionEffect;
    */
                        Texture2D m = Core.GetTextureFromFile(i.Sprite);
                        MobAnimData animData;
                        c.Range = i.Range;

                        using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
                        {
                            animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + i.Sprite + "'")[0];

                        }


                        c.animations[0] = ImportMobAnim(m, animData.Idle, animData.Size, true);
                        c.animations[1] = ImportMobAnim(m, animData.Walk, animData.Size, false);
                        c.animations[2] = ImportMobAnim(m, animData.Attack, animData.Size, false);
                        c.animations[3] = ImportMobAnim(m, animData.Death, animData.Size, false);
                        c.animations[4] = ImportMobAnim(m, animData.Corpse, animData.Size, true);
                        if (animData.Ranged != null)
                            c.animations[5] = ImportMobAnim(m, animData.Ranged, animData.Size, false);

                        c.animated = true;
                        //c.gameObject.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m, new Rect(0, 0, m.width, m.height), new Vector2(0.5f, 0.5f));
                        return c;
                    }

                default:
                    break;
            }
        }

            return null;
    }



    public void PlaySound(string Sound, IntegerVector vec)
    {

        AudioSource aud = Instantiate(SoundEffectPrefab, vec.ToVector3(), SoundEffectPrefab.transform.rotation).GetComponent<AudioSource>();
        aud.clip = AudioCollection[Sound];
        aud.Play();

    }


    //ЗАРЕФАКТОРИТЬ, ЕПТА, ЭТОТ БЫДЛОКОДИЩЕ,
   //ААААААААААААААААААААААААААААААААААААААААААААААА!!!!!!!!!!!
   // ПИИИИЗДЕЕЕЕЦ!
   //Какая же я ленивая жопа... Но мне так лень, так лень... Оно же работает!!! Эх...
    public Item CreateItem(string ItemID,IntegerVector coords)
    {
        Item it = null;
        using (var ItemDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Items.sqlite"))
        {

            string[] pars = ItemID.Split('.');
            ItemData itdata = new ItemData();
            MobAnimData animData;
            switch (pars[0])
            {
                case "Melee":
                    {
                        List<WeaponData> i = ItemDB.Query<WeaponData>("SELECT * FROM " + pars[0] + " WHERE ID ='" + pars[1] + "'");
                        Weapon s = (Instantiate(ItemPrefab, coords.ToVector3() -Vector3.forward*0.2f, new Quaternion(0, 0, 0, 0)) as GameObject).AddComponent<Weapon>();
                        Debug.Log("Spawned " + ItemID);
                        s.Range = i[0].Range;
                        s.Name = i[0].Name;
                        string[] slw = i[0].Slots.Split(',');
                        s.slots = new int[slw.Length];
                        for (int k = 0; k < slw.Length; k++)
                            s.slots[k] = Convert.ToInt32(slw[k]);
                        s.MultiSlot = i[0].Multislot;

                        s.MinDamage = i[0].MinDamage;
                        s.MaxDamage = i[0].MaxDamage;
                        s.Strength = i[0].Strength;
                        s.Wisdom = i[0].Wisdom;
                        s.Agility = i[0].Agility;
                        s.Type = i[0].Type;
                        s.type = i[0].DamageType;
                        s.Description = i[0].Description;
                        s.Range = 1;
                        Texture2D m = Core.GetTextureFromFile(i[0].Sprite);
                        s.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m, new Rect(0, 0, m.width, m.height), new Vector2(0.5f, 0.5f));
                        s.transform.localScale *= 64 / (float)m.width;
                        itdata = i[0];

                        if (Core.mainCharacter != null)
                        {
                            using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
                            {
                                animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + Core.mainCharacter.Sprite + "'")[0];

                            }
                            if (i[0].DisplaySprite != null)
                            {
                                if (i[0].DisplaySprite != "")
                                {
                                    Texture2D f = GetTextureFromFile(i[0].DisplaySprite);
                                    Debug.Log(i[0].DisplaySprite);
                                    if (f != null&&animData!=null)
                                    {
                                        s.mainanims[0] = ImportMobAnim(f, animData.Idle, 32, false);
                                        s.mainanims[1] = ImportMobAnim(f, animData.Walk, 32, false);
                                        s.mainanims[2] = ImportMobAnim(f, animData.Attack, 32, false);
                                        s.mainanims[3] = ImportMobAnim(f, animData.Death, 32, false);
                                        s.mainanims[4] = ImportMobAnim(f, animData.Corpse, 32, false);
                                        s.mainanims[5] = ImportMobAnim(f, animData.Ranged, 32, false);
                                    }
                                }
                            }

                            if (i[0].AltDisplaySprite != null)
                            {
                                if (i[0].AltDisplaySprite != "")
                                {
                                    Texture2D f = GetTextureFromFile(i[0].AltDisplaySprite);
                                    Debug.Log(i[0].AltDisplaySprite);
                                    s.altanims[0] = ImportMobAnim(f, animData.Idle, 32, false);
                                    s.altanims[1] = ImportMobAnim(f, animData.Walk, 32, false);
                                    s.altanims[2] = ImportMobAnim(f, animData.Attack, 32, false);
                                    s.altanims[3] = ImportMobAnim(f, animData.Death, 32, false);
                                    s.altanims[4] = ImportMobAnim(f, animData.Corpse, 32, false);
                                    s.altanims[5] = ImportMobAnim(f, animData.Ranged, 32, false);
                                }
                            }

                        }


                        s.Load();
                        it = s;
                    }

                    break;
                case "Ranged":
                    {
                        List<WeaponData> i = ItemDB.Query<WeaponData>("SELECT * FROM " + pars[0] + " WHERE ID ='" + pars[1] + "'");
                        Weapon s = (Instantiate(ItemPrefab, coords.ToVector3() - Vector3.forward * 0.2f, new Quaternion(0, 0, 0, 0)) as GameObject).AddComponent<Weapon>();
                        Debug.Log("Spawned " + ItemID);



                        s.Range = i[0].Range;
                        s.Name = i[0].Name;
                        string[] slw = i[0].Slots.Split(',');
                        s.slots = new int[slw.Length];
                        for (int k = 0; k < slw.Length; k++)
                            s.slots[k] = Convert.ToInt32(slw[k]);
                        s.MultiSlot = i[0].Multislot;
                        s.projectile = i[0].Projectile;
                        s.MinDamage = i[0].MinDamage;
                        s.MaxDamage = i[0].MaxDamage;
                        s.Strength = i[0].Strength;
                        s.Wisdom = i[0].Wisdom;
                        s.Agility = i[0].Agility;
                        s.Type = i[0].Type;
                        s.type = i[0].DamageType;
                        s.Description = i[0].Description;

                        if (Core.mainCharacter != null)
                        {
                            using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
                            {
                                animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + Core.mainCharacter.Sprite + "'")[0];

                            }
                            Debug.Log(i[0].DisplaySprite + " this is what will be displayed :3");
                            if (i[0].DisplaySprite != null)
                            {
                                if (i[0].DisplaySprite != "")
                                {
                                    Texture2D f = GetTextureFromFile(i[0].DisplaySprite);
                                    Debug.Log(i[0].DisplaySprite);
                                    s.mainanims[0] = ImportMobAnim(f, animData.Idle, 32, false);
                                    s.mainanims[1] = ImportMobAnim(f, animData.Walk, 32, false);
                                    s.mainanims[2] = ImportMobAnim(f, animData.Attack, 32, false);
                                    s.mainanims[3] = ImportMobAnim(f, animData.Death, 32, false);
                                    s.mainanims[4] = ImportMobAnim(f, animData.Corpse, 32, false);
                                    s.mainanims[5] = ImportMobAnim(f, animData.Ranged, 32, false);
                                }
                            }

                            if (i[0].AltDisplaySprite != null)
                            {
                                if (i[0].DisplaySprite != "")
                                {
                                    Texture2D f = GetTextureFromFile(i[0].AltDisplaySprite);
                                    Debug.Log(i[0].DisplaySprite);
                                    s.altanims[0] = ImportMobAnim(f, animData.Idle, 32, false);
                                    s.altanims[1] = ImportMobAnim(f, animData.Walk, 32, false);
                                    s.altanims[2] = ImportMobAnim(f, animData.Attack, 32, false);
                                    s.altanims[3] = ImportMobAnim(f, animData.Death, 32, false);
                                    s.altanims[4] = ImportMobAnim(f, animData.Corpse, 32, false);
                                    s.altanims[5] = ImportMobAnim(f, animData.Ranged, 32, false);
                                }
                            }

                        }


                        s.Load();
                        Texture2D m = Core.GetTextureFromFile(i[0].Sprite);
                        s.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m, new Rect(0, 0, m.width, m.height), new Vector2(0.5f, 0.5f));
                        s.transform.localScale *= 64 / (float)m.width;
                        itdata = i[0];
                        it = s;
                    }

                    break;


                case "Armor":
                    {
                        Debug.Log(pars[0] + pars[1]);
                        ArmorData armor = ItemDB.Query<ArmorData>("SELECT * FROM " + pars[0] + " WHERE ID ='" + pars[1] + "'")[0];
                        Armor armobj = (Instantiate(ItemPrefab, coords.ToVector3() - Vector3.forward * 0.2f, new Quaternion(0, 0, 0, 0)) as GameObject).AddComponent<Armor>();

                        armobj.Name = armor.Name;
                        armobj.Type = armor.Type;
                        armobj.MinBlock = armor.MinBlock;
                        armobj.MaxBlock = armor.MaxBlock;
                        armobj.Strength = armor.Strength;
                        armobj.Wisdom = armor.Wisdom;
                        armobj.Agility = armor.Agility;
                        armobj.Description = armor.Description;
                        if (armor.DescrType[0] == 'H')
                            armobj.IsHeavy = true;
                        string[] sla = armor.Slots.Split(',');
                        armobj.slots = new int[sla.Length];
                        for (int k = 0; k < sla.Length; k++)
                            armobj.slots[k] = Convert.ToInt32(sla[k]);
                        armobj.MultiSlot = armor.Multislot;
                        Texture2D m1 = Core.GetTextureFromFile(armor.Sprite);
                        armobj.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m1, new Rect(0, 0, m1.width, m1.height), new Vector2(0.5f, 0.5f));
                        armobj.Load();
                        (armobj.effects[0] as ArmorEffect).owner = armobj;
                        armobj.transform.localScale *= 64 / (float)m1.width;
                        it = armobj;
                        
                        if (Core.mainCharacter != null)
                        {
                            using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
                            {
                                animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + Core.mainCharacter.Sprite + "'")[0];

                            }
                            if (armor.DisplaySprite != null)
                            {
                                if (armor.DisplaySprite != "")
                                {
                                    Texture2D f = GetTextureFromFile(armor.DisplaySprite);
                                    Debug.Log(armor.DisplaySprite);
                                    armobj.anims[0] = ImportMobAnim(f, animData.Idle, 32, false);
                                    armobj.anims[1] = ImportMobAnim(f, animData.Walk, 32, false);
                                    armobj.anims[2] = ImportMobAnim(f, animData.Attack, 32, false);
                                   armobj.anims[3] = ImportMobAnim(f, animData.Death, 32, false);
                                    armobj.anims[4] = ImportMobAnim(f, animData.Corpse, 32, false);
                                   armobj.anims[5] = ImportMobAnim(f, animData.Ranged, 32, false);
                                }
                            }
                            else
                            {
                                Debug.Log("Item has no visual effects");
                            }
                        }
                    
            }
                    break;


                default:
                    {
                        List<GenericItem> b = ItemDB.Query<GenericItem>("SELECT * FROM " + pars[0] + " WHERE ID ='" + pars[1] + "'");
                        Item p = (Instantiate(ItemPrefab, coords.ToVector3() - Vector3.forward * 0.2f, new Quaternion(0, 0, 0, 0)) as GameObject).AddComponent<Item>();
                        Texture2D m2 = Core.GetTextureFromFile(b[0].Sprite);
                        p.GetComponent<SpriteRenderer>().sprite = Sprite.Create(m2, new Rect(0, 0, m2.width, m2.height), new Vector2(0.5f, 0.5f));
                        
                        p.transform.localScale *= 64 / (float)m2.width;
                        p.Description = b[0].Description;
                        p.Strength = b[0].Strength;
                        p.Wisdom = b[0].Wisdom;
                        p.Agility = b[0].Agility;
                        p.Type = b[0].Type;
                        Debug.Log(b[0].Slots);
                        string[] slw;
                        if (b[0].Slots != null)
                        {
                            slw = b[0].Slots.Split(',');
                            p.slots = new int[slw.Length];
                            for (int k = 0; k < slw.Length; k++)
                                p.slots[k] = Convert.ToInt32(slw[k]);
                        }
                        p.MultiSlot = b[0].Multislot;
                        p.Name = b[0].Name;
                        it = p;
                        itdata = b[0];
                    }
                    break;

            }

            if (itdata.Effect != null)
            {
                string[] effects = itdata.Effect.Split(',');
                foreach (string ef in effects)
                {
                    string[] concrete = ef.Split(':');
                    List<EffectData> b = ItemDB.Query<EffectData>("SELECT * FROM Effect" + " WHERE ID ='" + concrete[0] + "'");
                    Debug.Log(ef);
                    string h = "";
                    if (concrete.Length > 1)
                    {
                       
                        it.AddEffect(Core.Main.CreateTierEffect(ef));
                    }

                }
            }
        }

        if (it != null)
            return it;
        else
            return null;
    }




    public AudioClip GetAudioFromFile(string Path)
    {
        WWW audio = new WWW("file://" + Application.dataPath + "/StreamingAssets/" + Path);
        AudioClip clip = audio.GetAudioClip();
        clip.LoadAudioData();
        while (clip.loadState != AudioDataLoadState.Loaded);

        Debug.Log(Path);
      
        clip.LoadAudioData();
        
        return clip;

        return null;
    }


    
    public Effect CreateTierEffect(string TableID)
    {
        string[] temp = TableID.Split(':');
        try
        {
            using (var ItemDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Items.sqlite"))
            {
                List<EffectData> b = ItemDB.Query<EffectData>("SELECT * FROM Effect" + " WHERE ID ='" + temp[0] + "'");
                if (temp.Length > 1)
                    switch (temp[1])
                    {
                        case "1":
                            return CreateEffect(b[0].Type, b[0].Grade1, b[0].Sprite);
                            break;
                        case "2":
                            return CreateEffect(b[0].Type, b[0].Grade2, b[0].Sprite);
                            break;
                        case "3":
                            return CreateEffect(b[0].Type, b[0].Grade3, b[0].Sprite);
                            break;
                        case "4":
                            return CreateEffect(b[0].Type, b[0].Grade4, b[0].Sprite);
                            break;
                        case "5":
                            return CreateEffect(b[0].Type, b[0].Grade5, b[0].Sprite);
                            break;
                        case "6":
                            return CreateEffect(b[0].Type, b[0].Grade6, b[0].Sprite);
                            break;
                        case "7":
                            return CreateEffect(b[0].Type, b[0].Grade7, b[0].Sprite);
                            break;
                        case "8":
                            return CreateEffect(b[0].Type, b[0].Grade8, b[0].Sprite);
                            break;
                        default:
                            return null;
                            break;


                    }
                else
                {
                    Debug.Log("Something wrong with" + TableID);
                    return null;
                }
            }
        }
        catch (Exception e)
        {

            Debug.LogWarning(TableID + "  could not be created at all");
            return null;
        }
        
    }

    public Effect CreateEffect(string ID, string param,string effect = null)
    {
        Texture2D txt;
        txt = null;
        if (effect != null && effect != "")
        {
            txt = Core.GetTextureFromFile(effect);
            txt.name = effect;
        }
        using (var ItemDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Items.sqlite"))
        {
            if (param != null)
            {
                string[] pars = param.Split(',');
                if (pars != null)
                    try
                    {
                        switch (ID)
                        {


                            case "areaeffect":
                                {

                                    return new AreaEffect(int.Parse(pars[0]), int.Parse(pars[1]), int.Parse(pars[2]), int.Parse(pars[3]), int.Parse(pars[4]), pars[5], pars[6], float.Parse(pars[7]), int.Parse(pars[8]), txt);
                                }


                            case "dummyuse":
                                {
                                    if(pars.Length==1)
                                    return new PlaceEffect(pars[0]);
                                    else
                                    return new PlaceEffect(pars[0],true,Convert.ToInt32(pars[1]));
                                }




                            case "dummyattack":
                                {
                                    return new DummyAttack(float.Parse(pars[0]), pars[1]);
                                }

                            case "damageovertime":

                                return (new Poisoned(Convert.ToInt32(pars[0]), Convert.ToInt32(pars[1]),pars[2],txt));
                            case "poisontouch":

                                return (new PoisonousTouch(Convert.ToInt32(pars[0]), Convert.ToInt32(pars[1]),effect));

                            case "fireball":
                                return (new Fireball(Convert.ToInt32(pars[0]), Convert.ToInt32(pars[1])));

                            case "stun":
                                return (new Stun(Convert.ToInt32(pars[0]),txt));

                            case "heal":
                                return (new Heal(Convert.ToInt32(pars[0]), txt));
                            case "consumable":
                                return (new Consumable(Convert.ToInt32(pars[0])));
                            case "rechargeable":
                                return (new Rechargeable(Convert.ToInt32(pars[0])));
                            case "boltspell":
                                return (new BoltEffect(Convert.ToInt32(pars[0]), Convert.ToInt32(pars[1]), Convert.ToInt32(pars[2]), Convert.ToInt32(pars[3]), Convert.ToInt32(pars[4]), pars[5], pars[6],(float) Convert.ToDouble(pars[7]),txt));
                            case "food":
                                return new Food(Convert.ToInt32(pars[0]), Convert.ToInt32(pars[1]), txt);
                            case "regeneration":
                                return new Regeneration(Convert.ToInt32(pars[0]), Convert.ToInt32(pars[1]), txt);
                            case "armorignore":
                                return new ArmorIgnore((float)Convert.ToDouble(pars[0]));
                            case "resisstance":
                                return new Resisstance(pars[0], (float)Convert.ToDouble(pars[1]));
                            case "thorns":
                                return new Thorns(Convert.ToInt32(pars[0]), Convert.ToInt32(pars[1]));
                            case "inflamation":
                                return new InflamationEffect(Convert.ToInt32(pars[0]), Convert.ToInt32(pars[1]), Convert.ToInt32(pars[2]), pars[3], txt);
                            case "hpupgrade":
                                return new HPUpgrade(Convert.ToInt32(pars[0]));


                            default:
                                break;


                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Log("Error  creating " + ID + " with parametre" + pars);
                    }
            }
        }
        Debug.Log("Something went awfully wrong with" + ID);
        return null;

    }
    

    public static Texture2D GetTextureFromFile(string Path)
    {
        Path = Path.Replace((char)92,'/');
        Texture2D temp = new Texture2D(4,4);
        WWW spr = new WWW("file://" + Application.dataPath + "/StreamingAssets/" + Path);
        while (!spr.isDone) ;

        
        spr.LoadImageIntoTexture(temp);
        temp.filterMode = FilterMode.Point;
        temp.name = Path;
        return temp;
    }



    public AnimatedSprite CreateFollowingSprite(Texture2D k, GameObject aim, bool sfd = false)

    {

        Anim an = new Anim("1-10:100",0.1f, new Sprite[k.width / k.height]);
        an.period = 0.1f;
        AnimatedSprite animspr = (Instantiate(FollowingSpritePrefab)).GetComponent<AnimatedSprite>();
     
        for(int i = 0; i< an.sprites.Length; i++)
        {
            an.sprites[i] = Sprite.Create(k, new Rect(i * k.height,0, k.height, k.height),Vector2.one/2,32f);
        }
        animspr.animating = true;
        animspr.PlayAnim(an);
        animspr.GetComponent<FollowObject>().aim = aim.transform;
        animspr.SelfDestruct = sfd;
        return animspr;

    }

    float temp = 0;

    // Update is called once per frame


    public void LoadScene(int id)
    {
        Application.LoadLevel(id);
    }

    int objsleft = 0;
	void LateUpdate () {


        if (!GamePause)
        {



            if (Input.GetKeyDown(KeyCode.R))
                LoadFloor(1);
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("Save started");
                string s = LevelManager.GetSaveCode();

                FileStream sv = File.Open("savetest.map",FileMode.Create,
                                       FileAccess.ReadWrite,
                                       FileShare.ReadWrite);

                Debug.Log("Writing Tiles");
                if (sv.CanWrite)
                    sv.Write(Encoding.ASCII.GetBytes(s), 0, s.Length);
                while (!sv.CanWrite) ;



                sv.Close();

                Debug.Log("Save succesful");

            }



            if (Input.GetKeyDown(KeyCode.P))
            {
                PlaySound("footstep1", Player.Main.Coords);
            }

                if (Input.GetKeyDown(KeyCode.Q))
            {
                Debug.Log("Loading map");
             
                
                StreamReader read = new StreamReader("savetest.map");
                string lev = read.ReadToEnd();
                while (!read.EndOfStream) ;
                Debug.Log("Map loaded");
                read.Close();
               
                LoadLevel(lev);
                Debug.Log("Map constructed");

            }



            if (MakingTurn)
                for (int i = 0; i < TurnsInOneFrame; i++)
                {
                    if (objsleft > 0)
                    {
                        objsleft--;
                        NextTurn[objsleft].OnNextTurn();

                    }


                }

            if (objsleft <= 0 && MakingTurn)
            {
                if (!CoolDown)
                {
                    CoolDown = true;
                    StartCoroutine(TurnCoolDown());

                }
            }


            temp += Time.deltaTime;
            if (temp > 0.1f)
            {
                temp = 0;
                if (vecs.Count > 0)
                {


                    GameObject s = GameObject.Instantiate(textprefab, vecs[0], new Quaternion(0, 0, 0, 0)) as GameObject;
                    s.GetComponent<DMGText>().text = texts[0];
                    s.GetComponent<DMGText>().col = colors[0];
                    vecs.RemoveAt(0);
                    texts.RemoveAt(0);
                    colors.RemoveAt(0);
                }
            }


        }


    



    }


    System.Collections.IEnumerator LoadFreeze(float fr)
    {
        GamePause = true;
        yield return new WaitForSeconds(fr);
        if (LoadScreen != null)
            LoadScreen.enabled = false;
        GamePause = false;


    }

    System.Collections.IEnumerator Freeze(float fr)
    {
        GamePause = true;
        yield return new WaitForSeconds(fr);
        GamePause= false;


    }


    System.Collections.IEnumerator TurnCoolDown()
    {
        yield return new WaitForSeconds(0.1f);
        MakingTurn = false;
        CoolDown = false;
        ScheduledTurns--;
        if (ScheduledTurns > 0)
            MakeTurn();
        else
            ScheduledTurns = 0;

    }




}
