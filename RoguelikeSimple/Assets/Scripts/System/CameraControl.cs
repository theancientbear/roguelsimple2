﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {


        Camera.main.orthographicSize += ((Input.GetAxis("Mouse ScrollWheel")+ Input.GetAxis("Zoom"))) * Time.deltaTime * 100;
        if (
        Camera.main.orthographicSize < 3)

            Camera.main.orthographicSize = 3;
        if (
   Camera.main.orthographicSize > 30)

            Camera.main.orthographicSize = 30;

        if(Core.Main.Editing)
        {


            this.transform.position += new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }


    }
}
