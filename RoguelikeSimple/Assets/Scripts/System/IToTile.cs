﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IToTile {

    void ToTile(IntegerVector vec);
}
