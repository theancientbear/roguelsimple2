﻿using UnityEngine;
using System.Collections;

public class AnimatedSprite : MonoBehaviour {



    public Anim animation;
    public bool animating;
    public bool SelfDestruct;
    protected int animstep;
    protected float animtime = 0;
    SpriteRenderer r;
    // Use this for initialization
    void Start () {
        animating = true;
        r = GetComponent<SpriteRenderer>();
	
	}
	
	// Update is called once per frame
	void Update () {
        if (animation != null)
        {
            if (animating)
            {
                if (animtime > animation.period)
                {
                    animtime = 0;
                    animstep++;
                    if (animstep >= animation.sprites.Length)
                    {


                        if(SelfDestruct)
                        {
                            Destroy(this.gameObject);
                        }

                        PlayAnim(animation);

                    }
                    if(animstep<animation.sprites.Length)
                       (r as SpriteRenderer).sprite = animation.sprites[animstep];


                }
                else
                {
                    animtime += Time.deltaTime;
                }

            }
        }
        else
            (r as SpriteRenderer).sprite = null;
    }

    public void Stop()
    {
        animation = null;
    }

    public void PlayAnim(Anim an)
    {
        

        if (animating)
        {
            if (animation != an)
            {
                animstep = 0;
                animtime = 0;
                animation = an;
            }

            else if (animstep >= animation.sprites.Length)
            {
                if(!SelfDestruct)
                animstep = 0;
            }
        }

    }
}
