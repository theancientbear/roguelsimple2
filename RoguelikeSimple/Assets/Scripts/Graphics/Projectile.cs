﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public float velocity = 100;
    public Vector3 aim;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


        this.transform.position += (aim - transform.position).normalized * velocity*Time.deltaTime;
        if (Vector3.Distance(aim, transform.position) < velocity*Time.deltaTime)
            Destroy(gameObject);
	
	}
}
