﻿using UnityEngine;
using System.Collections;

public class Armor : Item {

    public int MinBlock;
    public int MaxBlock;
    public bool IsHeavy = false;

    public void Load()
    {

        Effect tmp = new ArmorEffect(MinBlock, MaxBlock);
        tmp.owner = this;
        effects.Add(tmp);
    }


    public override void Start()
    {
        base.Start();



    }


    public override string GenerateDescription()
    {
        string s = "";
        s += Name;
        s += '\n';
        s += Type;
        s += '\n';
        s += MinBlock.ToString() + "-" + MaxBlock.ToString();
        s += '\n';
        s += Description;
        return s;
    }



    public override string SaveFormat()
    {
        string output = base.SaveFormat().Replace("Item", "Armor") + "#" + MinBlock.ToString() + "#" + MaxBlock.ToString()+"#"+IsHeavy.ToString();
        return output;
    }


    public override void LoadFormat(string info)
    {
        base.LoadFormat(info);

        string[] data = info.Split('#');
        MinBlock = int.Parse(data[10]);
        MaxBlock = int.Parse(data[11]);
        IsHeavy= bool.Parse(data[12]);

        Load();
    }



}
