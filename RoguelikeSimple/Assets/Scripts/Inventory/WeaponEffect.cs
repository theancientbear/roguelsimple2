﻿using UnityEngine;
using System.Collections;

public class WeaponEffect : Effect {


    public int MaxDamage = 0;
    public int MinDamage = 0;
    public string DamageType;
    public int Range = 1;
    public Item own;
    public string projectile = "Sprites\arrow.png";

    public WeaponEffect(int min, int max,int rng,string type,Item owne, string proj = null)
    {
        MaxDamage = max;
        MinDamage = min;
        Range = rng;
        own = owne;
        if(proj!=null)
        projectile = proj;
        this.DamageType = type;
    }

    public override void OnAttack(Creature Aim, ref int damage, ref string type,ref float armorignore, Creature attacker)
    {
        Debug.Log("Attacking with ffct");

       

        base.OnAttack(Aim, ref damage, ref type, ref armorignore, attacker);
        if ((IntegerVector.Distance(Aim.Coords, attacker.Coords)<=1 &&Range==1) || ((IntegerVector.Distance(Aim.Coords, attacker.Coords) >1 && Range > 1)))
        {
            if (Core.MainGrid.CheckPassability(attacker.Coords, Aim.Coords))
            {

                foreach (Effect i in own.effects)
                {
                  if(i!=null)
                    i.OnHit(Aim, ref damage, ref type, attacker);
                }


                if (own.MultiSlot)
                    Player.Main.StrExp += 50;
                else
                {
                    if (Range <= 1)
                        Player.Main.AgiExp += 35;
                    else
                        Player.Main.AgiExp += 50;
                }


                int dmg = (int)Random.Range(MinDamage + (int)Mathf.Floor(((float)attacker.Strength-10)*0.005f), MaxDamage + (int)Mathf.Floor(((float)attacker.Strength)*0.005f));
                if (0.05f + 0.005f*((float)attacker.Agility-10) > Random.Range(0.0f, 1.0f))
                {
                    dmg *= 2;
                   
                    Core.Main.AddFloatText("Crit!", attacker.transform.position,Color.magenta);
                }
                if (owner == null)
                    Debug.Log("Weapon intilization failed");
                Aim.Damage(attacker, dmg, this.DamageType,armorignore,this.owner);
                Debug.Log(owner.Name);
                if (projectile != null)
                {
                    if (Range > 1)
                    {
                        Texture2D m = Core.GetTextureFromFile(projectile);
                        Sprite s = Sprite.Create(m, new Rect(0, 0, m.width, m.height), new Vector2(0.5f, 0.5f), 32);
                        Core.Main.CreateProjectile(attacker.transform.position, Aim.transform.position, 30, s);
                    }

                }
                if (Range > 1)
                {

                }

            }
            
        }
    }
}
