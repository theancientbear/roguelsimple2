﻿using UnityEngine;
using System.Collections;

public class Weapon : Item {
    public int MinDamage;
    public int MaxDamage;
    public int Range;
    public string type;
    public Anim[] mainanims = new Anim[20];
    public Anim[] altanims = new Anim[20];
    public string projectile;
    public override string GenerateDescription()
    {
        string s = "";
        s += Name;
        s += '\n';
        s += Type;
        s += '\n';
        s += type;
        s +='\n';
        s += MinDamage.ToString() + "-" + MaxDamage.ToString();
        s += '\n';
        s += Description;
         
        return s;
    }


    
    public void Load()
    {

        Debug.Log("CreatedWeapon" + type);

        Effect f = new WeaponEffect(MinDamage, MaxDamage, Range, type, this, projectile);
        f.owner = this;
       AddEffect(f);
    }


    public override void OnEquip()
    {
        base.OnEquip();

      
    }

    public override void LoadAnims()
    {
        base.LoadAnims();



            if (!MultiSlot)
            {
                if (mainanims != null)
                {

                Debug.Log(this.Name + " has the id " + idnum.ToString());
                    if (Player.Main.Equipment[1] != null)
                        if (Player.Main.Equipment[1].idnum == this.idnum)
                        {

                            if (altanims != null)
                                anims = altanims;
                            else
                                anims = mainanims;

                            Debug.Log("altanims not empty");
                        }


                    if (Player.Main.Equipment[0] != null)
                        if (Player.Main.Equipment[0].idnum == this.idnum)
                        {
                            anims = mainanims;
                            Debug.Log("mainanims not empty");
                            return;
                        }
                    

                }


            }
            else
            {
                anims = mainanims;
            }
        
    }

    public override string SaveFormat()
    {
        string output;
        if(projectile==null||projectile=="")
      output = base.SaveFormat().Replace("Item", "Weapon") + "#" + MinDamage.ToString() + "#" + MaxDamage.ToString() + "#" + Range.ToString() + "#"  + "null" + "#";
        else
            output = base.SaveFormat().Replace("Item", "Weapon") + "#" + MinDamage.ToString() + "#" + MaxDamage.ToString() + "#" + Range.ToString() + "#"  + projectile.ToString() + "#";
        if (mainanims != null)
            
            output += mainanims[0].sprites[0].texture.name + "#";
        else
            output += "null#";
        if(altanims!=null)
            output += altanims[0].sprites[0].texture.name + "#";
        else
            output += "null";

        return output;

    }


    public override void LoadFormat(string info)
    {
        base.LoadFormat(info);
        string[] data = info.Split('#');
        MinDamage = int.Parse(data[10]);
        MaxDamage = int.Parse(data[11]);
        Range = int.Parse(data[12]);
        if(data[13]!="null")
        projectile = data[13];
        if(data[14]!= "null")
        {
            mainanims = Item.GetAnim(data[14]);
        }
        if (data[15] != "null")
        {
            altanims = Item.GetAnim(data[15]);
        }
        Load();
    }
}
