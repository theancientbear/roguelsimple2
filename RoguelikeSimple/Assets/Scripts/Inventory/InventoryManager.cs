﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class InventoryManager : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Button[] ItemIcons = new Button[8];
    public static InventoryManager Main;
    public List<Button> ItemButts = new List<Button>();
    public GameObject prefab;
    public RectTransform Descr;

    public void OnPointerEnter(PointerEventData eventData)
    {
        Core.Main.OnInterface = true;

    }

    public void OnPointerExit(PointerEventData eventData)
    {

        Core.Main.OnInterface = false;
    }

    public void OnButtClick(int id)
    {
        if(!Core.Main.GamePause)
        if(Player.Main.Equipment[id]!= null)
        {
            Player.Main.DropItem(id);
        }

    }





    void Start()
    {
        Main = this;
        UpdateInv();

    }


    public void UpdateInv()
    {
        int[] length = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
       foreach(Button i in ItemButts)
        {
            i.GetComponent<ItemInfo>().OnPointerExit(null);
            Destroy(i.gameObject);
           

        }
        ItemButts.Clear();
        foreach(Item i in Core.Main.Itemlist)
        {
            if (i.coords == Player.Main.Coords && i.Owner == null)
            {

                
                foreach (int k in i.slots)
                {
                    ItemInfo m = (Instantiate(prefab) as GameObject).GetComponent<ItemInfo>();
                    try {
                        m.transform.SetParent(this.transform);
                        m.it = i;
                        m.transform.position = ItemIcons[k].transform.position + Vector3.right * 30+ Vector3.right * 50 * length[k];
                        m.slotnum = k;
                        m.GetComponent<Image>().overrideSprite = i.GetComponent<SpriteRenderer>().sprite;
                        m.GetComponentInChildren<Text>().text = i.Name;

                        ItemButts.Add(m.GetComponent<Button>());
                        length[k]++;
                    }
                    catch
                    {
                        Destroy(m.gameObject);
                    }
                }
             
            
            }
        }
    }

    void FixedUpdate()
    {
        for(int i =0; i<Player.Main.Equipment.Length;i++)
        {
            if (Player.Main.Equipment[i] != null)
            {
                ItemIcons[i].GetComponent<ItemInfo>().it = Player.Main.Equipment[i];
                ItemIcons[i].GetComponentInChildren<Text>().text = "";
                ItemIcons[i].GetComponent<Image>().color = new Color(1, 1, 1, 1);
                ItemIcons[i].GetComponent<Image>().overrideSprite = Player.Main.Equipment[i].GetComponent<SpriteRenderer>().sprite;
            }
            else
            {
                ItemIcons[i].GetComponentInChildren<Text>().text = "";
                ItemIcons[i].GetComponent<Image>().color = new Color(0, 0, 0, 0);
                ItemIcons[i].GetComponent<Image>().overrideSprite = null;
            }
            
        }

    }
}
