﻿using UnityEngine;
using System.Collections.Generic;
using SQLite;
using System.Collections;

public class Item:MonoBehaviour, IEffectStuff {



    public IntegerVector coords;
    public Creature Owner;
    public List<Effect> effects = new List<Effect>();
    public string ID;
    public string Name;
    public string Type;
    protected Renderer ren;
    public int[] slots;
    public int Strength;
    public int Wisdom;
    public int Agility;
    public string Description;
    public bool MultiSlot = false;
    public Anim[] anims = new Anim[18];
    public static int ovid = 0;
    public int idnum;
    public float DodgeBonus;



    public virtual void LoadAnims()
    {
        idnum = ovid;
        ovid++;
    }
	// Use this for initialization
	public virtual void Start () {

        Core.Main.Itemlist.Add(this);
        ren = GetComponent<Renderer>();
        coords = transform.position.ToVectorInteger();
        if(!Core.Main.Editing)
        InventoryManager.Main.UpdateInv();
	
	}
    public virtual void  OnAwake()
    {

    }



    public void Use()
    {
        Debug.Log("Ussssing " + Name);
        foreach (Effect f in effects)
        {
            if(f!=null)
            f.OnUse(Owner);
        }
    }



    public virtual Effect GetEffect(string ID)
    {
        return null;
    }
    public virtual Effect AddEffect (string ID, int Grade)
    {

        return null;
    }
    public virtual Effect AddEffect(string ID,string param,string Sprite = null)
    {
        effects.Add(Core.Main.CreateEffect(ID, param,Sprite));
        if (effects[effects.Count - 1]!=null)
        effects[effects.Count - 1].owner = this;
            return null;
    }


    public virtual void AddEffect(Effect f)
    {
        effects.Add(f);

        if (effects[effects.Count - 1] != null)
            effects[effects.Count - 1].owner = this;
        
    }

    public virtual void OnEquip()
    {
          for(int i = 0; i<Player.Main.Equipment.Length;i++)
        { 
           
            if(Player.Main.Equipment[i]!= null)
                if(Player.Main.Equipment[i].MultiSlot)
            foreach (int k in Player.Main.Equipment[i].slots)
                foreach (int h in slots)
                    if (h == k)

                    {
                                if(Player.Main.Equipment[i]!= null)
                                Player.Main.Equipment[i].Owner = null;
                                Player.Main.Equipment[i] = null;

                            }
        }
       
        if (MultiSlot)
            foreach (int i in slots)
                if (Player.Main.Equipment[i] != null)
                {
                    Player.Main.Equipment[i].Owner = null;
                    Player.Main.Equipment[i] = null;
                }
    }

    public virtual string GenerateDescription()
    {
        string s = "";
        s += Name;
        s += '\n';
        s += Type;
        s += '\n';
        s += '\n';
        s += Description;
        return s;
    }
	
	// Update is called once per frame
	public virtual void Update () {
        transform.position = coords.ToVector3() - Vector3.forward * 0.1f; ;
        if(Owner != null)
        {
            this.coords = Owner.Coords;

            ren.enabled = false;
            
        }
        else
        {
            ren.enabled = true;
            
           
        }
	
	}


    public static Anim[] GetAnim(string ans)
    {
        using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
        {
            Anim[] output = new Anim[18];
            MobAnimData animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + Core.mainCharacter.Sprite + "'")[0];

            Texture2D f = Core.GetTextureFromFile(ans);
            Debug.Log(ans);
            output[0] = Core.ImportMobAnim(f, animData.Idle, 32, false);
            output[1] = Core.ImportMobAnim(f, animData.Walk, 32, false);
            output[2] = Core.ImportMobAnim(f, animData.Attack, 32, false);
            output[3] = Core.ImportMobAnim(f, animData.Death, 32, false);
            output[4] = Core.ImportMobAnim(f, animData.Corpse, 32, false);
            output[5] = Core.ImportMobAnim(f, animData.Ranged, 32, false);
            return output;

        }
        return null;
    }



    public virtual string SaveFormat()
    {
        string output;
        if(Description!=""&&Description!=null)
       output = "Item#"+coords.x+"#"+coords.y+"#"  + Name +"#" +Description+"#"+MultiSlot.ToString()+"#";
        else
            output = "Item#" + coords.x + "#" + coords.y + "#" + Name + "#" + "null" + "#" + MultiSlot.ToString() + "#";
        output += (ren as SpriteRenderer).sprite.texture.name + "#";

        if (anims[0] != null)
            output += anims[0].sprites[0].texture.name + "#";
        else
            output += "null#";
        for (int i = 0; i < slots.Length; i++)
            output += slots[i] + "+";

        output = output.Remove(output.Length - 1);
        output += "#";


        int count = 0;
        foreach (Effect ef in effects)
            if (ef != null)
            {
                if (ef.SaveFormat() != null)
                {
                    output += ef.SaveFormat() + "@";
                    count++;
                }
            }
        if (count > 0)
            output = output.Remove(output.Length - 1);
        if (count == 0)
            output += "null";
        return output;

    }


    public virtual void LoadFormat(string info)
    {
        string[] data = info.Split('#');
        // output = "Item#"  + Name +"#" +Description+"#"+MultiSlot.ToString()+"#";
        coords = new IntegerVector(int.Parse(data[1]),int.Parse(data[2]));
        this.transform.position = coords.ToVector3();
        Name = data[3];
        Description = data[4];
        MultiSlot = bool.Parse(data[5]);
        if (data[7] != "null")

        {
            Debug.Log("Animdata" + data[7]);
            anims = Item.GetAnim(data[7]);
        }
        Texture2D txt = Core.GetTextureFromFile(data[6]);

        GetComponent<SpriteRenderer>().sprite = Sprite.Create(txt, new Rect(0, 0, txt.width, txt.height), Vector2.one /2,50f);


        string[] sldt = data[8].Split('+');
        slots = new int[sldt.Length];
        for (int i = 0; i < slots.Length; i++)
        {

            slots[i] = int.Parse(sldt[i]);
        }

        List<Effect> efs = Effect.Parse(data[9]);
        foreach (Effect ef in efs)
            AddEffect(ef);



    }



}
