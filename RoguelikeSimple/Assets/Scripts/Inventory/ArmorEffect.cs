﻿using UnityEngine;
using System.Collections;

public class ArmorEffect :Effect {

    public int MaxDamage;
    public int MinDamage;
    
    public Armor owner;


    public  ArmorEffect(int a, int b)
    {
        MinDamage = a;
        MaxDamage = b;
    }

    public override void OnDefence(Creature Attacker,Creature Defender ,ref int damage, ref string type,float armorignore = 0)
    {

              if(!owner.MultiSlot)
            foreach(int i in owner.slots)
            {
                if (i == 1)
                    Player.Main.StrExp += 70;
                else
                {
                    if (owner.IsHeavy)
                    {
                        Player.Main.StrExp += 35;
                    }
                    else
                    {
                        Player.Main.AgiExp += 35;
                    }

                }
                
            }

        damage -=(int) ((1-armorignore)*(float)Random.Range(MinDamage, MaxDamage));
        if (damage < 0)
            damage = 0;
        base.OnDefence(Attacker,Defender, ref damage, ref type,armorignore);
    }


}
