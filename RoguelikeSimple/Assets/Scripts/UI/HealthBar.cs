﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class HealthBar : MonoBehaviour {
    public Creature Aim;
    Renderer ar;
   Image r;
    Image cr;
    Scrollbar scr;

	// Use this for initialization
	void Start () {
        scr = this.GetComponent<Scrollbar>();
        ar = Aim.GetComponent<Renderer>();
        r = GetComponent<Image>();
        cr = GetComponentInChildren<Image>();
	
	}
	
	// Update is called once per frame
	void Update () {
        
        if(Aim== null)
        {
            Destroy(gameObject);
            return;
        }
        if (!ar.enabled)
        {
            (scr.transform as RectTransform).localScale = Vector3.zero;
        }
        else
        {
            (scr.transform as RectTransform).localScale = Vector3.one * 0.002f;
        }
 
       
       transform.position = Aim.transform.position;
        scr.value = 0;
        scr.size = (float)Aim.Health / (float)Aim.MaxHealth;
       // (this.transform as RectTransform).sizeDelta = new Vector2(50 * Aim.Health / Aim.MaxHealth, (this.transform as RectTransform).rect.y);
        
	}
}
