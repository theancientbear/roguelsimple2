﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SQLite;

public class PlayerSelectWindow : MonoBehaviour
{

    public AnimatedUI im;
    public CharacterData chr;
    public Text Descr;
    public bool Initialize;

    // Use this for initialization
    void Start()
    {
        
    }

    public void Init(CharacterData t)
    {
        chr = t;
        Descr.text = t.History;
        MobAnimData animData;

        using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
        {
            animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + t.Sprite + "'")[0];

        }
        im.mainAnim = Core.ImportMobAnim(Core.GetTextureFromFile(t.Sprite), animData.Idle, animData.Size, true);

    }



    public void StartNewGame()
    {
        Core.mainCharacter = chr;
        Application.LoadLevel(1);

    }
    // Update is called once per frame
    void Update()
    {

    }
}
