﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ItemUseButton : MonoBehaviour, IPointerClickHandler {


    public void OnPointerClick(PointerEventData dat)
    {
        if(!Core.Main.GamePause)
        if(dat.button == PointerEventData.InputButton.Right)
        {
          
            for(int i =0; i< InventoryManager.Main.ItemIcons.Length;i++)
            {
                if(this.gameObject== InventoryManager.Main.ItemIcons[i].gameObject)
                {
                    Debug.Log("Using " + Player.Main.Equipment[i].Name);
                    Player.Main.Equipment[i].Use();
                   
                }
            }
        }
    }

	// Use this for initialization

}
