﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnimatedUI : MonoBehaviour {

    public Anim mainAnim;

    int animstep = 0;
    float temp = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (mainAnim != null)
        {

            if (temp >= mainAnim.period)
            {
                animstep++;
                temp = 0;
                if (animstep >= mainAnim.sprites.Length)
                    animstep = 0;
            }
            temp += Time.deltaTime;
            this.GetComponent<Image>().sprite = mainAnim.sprites[animstep];
        }
	
	}
}
