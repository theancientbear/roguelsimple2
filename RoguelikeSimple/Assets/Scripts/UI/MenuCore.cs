﻿using UnityEngine;
using System.Collections;
using SQLite;
using System.Collections.Generic;
using UnityEngine.UI;
public class MenuCore : MonoBehaviour {



    public GameObject PlayerSelectionPrefab;
    public Canvas main;

    // Use this for initialization
    void Start()
    {

        Debug.Log(System.Environment.GetCommandLineArgs()[0]);

        List<CharacterData> chars = new List<CharacterData>();
        using (var CharDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Core.sqlite"))
        {
            chars = CharDB.Query<CharacterData>("SELECT * FROM PlayerCharacters");

        }
        int num = 0;
        foreach(CharacterData i in chars)
        {
            
            GameObject pref = Instantiate(PlayerSelectionPrefab) as GameObject;
            PlayerSelectWindow temp = pref.GetComponent<PlayerSelectWindow>();
            temp.Init(i);
            pref.transform.position = new Vector3( 100,main.transform.position.y+100);
            pref.transform.position += num * Vector3.right*250;
            pref.transform.parent = main.transform;
            
            num++;
        }



    }

    public void ExitFunc()
    {

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
