﻿using UnityEngine;
using System.Collections;

public class DMGText : MonoBehaviour {


    float temp = 0;
    public float period = 1;
    public string text = " ";
    public Color col;

    TextMesh txt;

	// Use this for initialization
	void Start () {
        txt = this.GetComponent<TextMesh>();

    }
	
	// Update is called once per frame
	void Update () {
        txt.text = text;
        txt.color = new Color(col.r, col.g,col.b, 1-temp / period);
        temp += Time.deltaTime;
        transform.position += Vector3.up * Time.deltaTime;
	    if(temp>period)
        {
            Destroy(gameObject);
        }
	}
}
