﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ItemInfo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{


    public Item it;
    public int slotnum;


    public void OnPointerEnter(PointerEventData eventData)
    {
        if (it != null)
        {
            InventoryManager.Main.Descr.transform.position = this.transform.position;
            InventoryManager.Main.Descr.GetComponentInChildren<Text>().text = it.GenerateDescription();

            InventoryManager.Main.Descr.SetAsLastSibling();
        }
        Core.Main.OnInterface = true;

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        InventoryManager.Main.Descr.transform.position = Vector3.down * -1000;
        Core.Main.OnInterface = false; 
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Equip()
    {
        OnPointerExit(null);
        if (Player.Main.Equipment[slotnum] != null)
        {
            Player.Main.Equipment[slotnum].Owner = null;
            Player.Main.Equipment[slotnum] = null;

        }



        Player.Main.EquipItem(it, slotnum);
    }

}