﻿using UnityEngine;
using System.Collections;

public class LadderUpTile : MonoBehaviour, IOnNextTurn {

	// Use this for initialization
	void Start () {
        Core.Main.NextTurn.Add(this);
	
	}
	
    public void OnNextTurn()
    {
        if(Player.Main.Coords==this.transform.position.ToVectorInteger())
        Core.Main.LoadFloor(Core.FloorNum++);
    }

	// Update is called once per frame
	void Update () {


	
	}
}
