﻿using UnityEngine;
using System.Collections;
using SQLite;
using System.Collections.Generic;


public class Player : Creature {

    public static Player Main;



    public Creature DMGAim;

    public List<IntegerVector> FieldOfView = new List<IntegerVector>();
    public int StrExp = 0;
    public int AgiExp = 0;
    public int IntExp = 0;
    public int BaseHP = 100;
    public Item[] Equipment = new Item[8];

    public bool[] Occupied
    {
        get
        {
            bool[] occ = new bool[Equipment.Length];
            for(int i =0; i<Equipment.Length; i++)
            {
                if(Equipment[i]!=null)
                {
                    occ[i] = true;
                    if (Equipment[i].MultiSlot)
                        foreach (int k in Equipment[i].slots)
                            occ[k] = true;
                }
            }

            return occ;
        }
    }
    public AnimatedSprite[] ItemAvatars = new AnimatedSprite[8];
    public Anim[,] defaultanims = new Anim[18, 8];





    public override void PlayAnim(int id)
    {
        base.PlayAnim(id);
        for(int i = 0; i< Equipment.Length;i++)
        {
            if (Occupied[i])
            {
                if (Equipment[i] != null)
                {
                    if (Equipment[i].anims[id] != null)
                    {
                        ItemAvatars[i].PlayAnim(Equipment[i].anims[id]);

                    }
                }
                else
                {
                    ItemAvatars[i].Stop();
                }
            }
            else
            {
                if(defaultanims[id,i]!=null)

                    ItemAvatars[i].PlayAnim(defaultanims[id,i]);
            }
            
            


        }

    }


    public override void OnNextTurn()
    {

       


        if (Turns < 0)
            Turns = 0;

        if (Health <= 0)
        {
            OnDeath();
        }
  


    }



    public void CountEffects()
    {

        for (int i = 0; i < Equipment.Length; i++)
            if (Equipment[i] != null)
                foreach (Effect ef in Equipment[i].effects)
                    if(ef!=null)
                    ef.OnNextTurn(this);


        if (Core.MainGrid.PointToTile.ContainsKey(Coords)) 
        foreach (Effect ef in Core.MainGrid.PointToTile[Coords].effects)
            ef.OnStep(this);


        MaxHealth = BaseHP + (Strength - 10) * 5 / 2;

        for (int i = 0; i < effects.Count; i++)
            effects[i].OnNextTurn(this);
    }



    public void CheckView()
    {
      


        for (int i = -FOV / 10 + 1; i < FOV / 10; i++)
            for (int k = -FOV / 10 + 1; k < FOV / 10; k++)
            {

                    if (Core.MainGrid.PointToTile.ContainsKey(Coords + new IntegerVector(i, k)))
                    {
                        Tile t = Core.MainGrid.PointToTile[Coords + new IntegerVector(i, k)];

                        t.GetComponent<Collider2D>().enabled = true;
                    }


            }
        FieldOfView = Core.MainGrid.CheckVisibility(Coords, FOV / 10);
       Core.MainGrid.ChangeVisibility(FieldOfView );
        
    }


    public void OnDidSomething()
    {
        Turns--;
        Action();
    }


    public override void Action()
    {
        
        

        if(DMGAim!= null)
        {
            Attack(DMGAim);
            DMGAim = null;
            Path = null;
            Aim = Coords;
            Turns--;
        }
        if (Path != null)
        {
            if (Path.Count > 0)
            {

                if(!Core.MainGrid.PointToTile[Path[Path.Count-1]].Passable)
                {
                    Path.RemoveAt(Path.Count - 1);
                }
                if (Path.Count > 0)
                {

                    if (Move(Path[0] - Coords))
                    {
                        Path.RemoveAt(0);
                        Turns--;
                      
                        CheckView();

                    }
                    else
                    {


                        Path = Astar.FindThePath(Coords, Aim, 1000);

                    }
                }
            }
        }
        if(InventoryManager.Main!=null)
        InventoryManager.Main.UpdateInv();

        if (Turns < 0)
            Core.Main.MakeTurn();

    }

    public bool init = false;




    public override void Start()
    {
        base.Start();
  





        Main = this;
        if(!init)
        if(Core.mainCharacter !=null)
        {


                //-------------------------
                //ПОдгрузка звуков
                // ---------------------

                using (var SoundDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Sounds.sqlite"))
                {
                    List<SoundSequence> SoundList = SoundDB.Query<SoundSequence>("SELECT * FROM " + Core.mainCharacter.SoundPack);
                    foreach(SoundSequence snd in SoundList)
                    {
                        SoundPack.Add(snd.Action, snd.SoundList.Split(','));
                    }
                
                }






                init = true;
            Strength = Core.mainCharacter.Strength;
            Agility = Core.mainCharacter.Agility;
            Wisdom = Core.mainCharacter.Intellect;
            if (Core.mainCharacter.Slot1 != null && Core.mainCharacter.Slot1 != "")
            {
                Debug.Log("Inventory starting item 1 : " + Core.mainCharacter.Slot1);
                Item i = Core.Main.CreateItem(Core.mainCharacter.Slot1, Coords);
                EquipItem(i, 0);
            }
            if (Core.mainCharacter.Slot2 != null && Core.mainCharacter.Slot2 != "")
            {
                Debug.Log("Inventory starting item 2: " + Core.mainCharacter.Slot2);
                Item i = Core.Main.CreateItem(Core.mainCharacter.Slot2, Coords);
                EquipItem(i, 1);
            }
            if (Core.mainCharacter.Slot3 != null && Core.mainCharacter.Slot3 != "")
            {
                Debug.Log("Inventory starting item 3: " + Core.mainCharacter.Slot3);
                Item i = Core.Main.CreateItem(Core.mainCharacter.Slot3, Coords);
                EquipItem(i, 2);
            }
            if (Core.mainCharacter.Slot4 != null && Core.mainCharacter.Slot4 != "")
            {
                    Debug.Log("Inventory starting item 4: " + Core.mainCharacter.Slot3);
                    Item i = Core.Main.CreateItem(Core.mainCharacter.Slot4, Coords);
                EquipItem(i, 3);
            }
            if (Core.mainCharacter.Slot5 != null && Core.mainCharacter.Slot5 != "")
            {
                    Debug.Log("Inventory starting item5: " + Core.mainCharacter.Slot3);
                    Item i = Core.Main.CreateItem(Core.mainCharacter.Slot5, Coords);
                EquipItem(i, 4);
            }
            if (Core.mainCharacter.Slot6 != null && Core.mainCharacter.Slot6 != "")
            {
                    Debug.Log("Inventory starting item 6: " + Core.mainCharacter.Slot3);
                    Item i = Core.Main.CreateItem(Core.mainCharacter.Slot6, Coords);
                EquipItem(i, 5);
            }
            if (Core.mainCharacter.Slot7 != null && Core.mainCharacter.Slot7 != "")
            {
                    Debug.Log("Inventory starting item 7: " + Core.mainCharacter.Slot3);
                    Item i = Core.Main.CreateItem(Core.mainCharacter.Slot7, Coords);
                EquipItem(i, 6);
            }
            if (Core.mainCharacter.Slot8 != null && Core.mainCharacter.Slot8!= "")
            {
                    Debug.Log("Inventory starting item 8: " + Core.mainCharacter.Slot3);
                    Item i = Core.Main.CreateItem(Core.mainCharacter.Slot8, Coords);
                EquipItem(i, 7);
            }

                MobAnimData animData;

                using (var AnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Graphics.sqlite"))
                {
                    animData = AnimDB.Query<MobAnimData>("SELECT * FROM Mobs WHERE SpriteSheet='" + Core.mainCharacter.Sprite + "'")[0];

                }
                Texture2D m = Core.GetTextureFromFile(Core.mainCharacter.Sprite);
                animations[0] = Core.ImportMobAnim(m, animData.Idle, animData.Size, true);
                animations[1] = Core.ImportMobAnim(m, animData.Walk, animData.Size, false);
                animations[2] = Core.ImportMobAnim(m, animData.Attack, animData.Size, false);
                animations[3] = Core.ImportMobAnim(m, animData.Death, animData.Size, false);
                animations[4] = Core.ImportMobAnim(m, animData.Corpse, animData.Size, true);
                animations[5] = Core.ImportMobAnim(m, animData.Ranged, animData.Size, false);
                List<DefAnimData> anims;
                using (var DefAnimDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Core.sqlite"))
                {
                    anims = DefAnimDB.Query<DefAnimData>(" SELECT * FROM DefaultSprites");
                }
                    animated = true;

                foreach(DefAnimData i in anims)
                {
                     m = Core.GetTextureFromFile(i.SpriteSheet);
                    defaultanims[0,i.Slot] = Core.ImportMobAnim(m, animData.Idle, animData.Size, true);
                    defaultanims[1, i.Slot] = Core.ImportMobAnim(m, animData.Walk, animData.Size, false);
                    defaultanims[2, i.Slot] = Core.ImportMobAnim(m, animData.Attack, animData.Size, false);
                    defaultanims[3, i.Slot] = Core.ImportMobAnim(m, animData.Death, animData.Size, false);
                    defaultanims[4, i.Slot] = Core.ImportMobAnim(m, animData.Corpse, animData.Size, true);
                    defaultanims[5, i.Slot] = Core.ImportMobAnim(m, animData.Ranged, animData.Size, false);
                }

            }

        init = true;
    }

   

    protected override void FixedUpdate()
    {

        if (Moving)
        {
            PlayAnim(1);
            if (Vector3.Distance(transform.position, Coords.x * Vector3.right + Coords.y * Vector3.up) < 0.1f)
            {
                transform.position = Coords.x * Vector3.right + Coords.y * Vector3.up;
                Moving = false;

                PlaySoundActionOnPos("footstep");




            }
            else
            {
                transform.position -= (transform.position - (Coords.x * Vector3.right + Coords.y * Vector3.up)).normalized * 10 * Time.deltaTime;

            }
        }
        else
        {

        
            if (!Core.Main.MakingTurn)
            {

                if (Path != null)
                {
                    if (Path.Count > 0)

                        Action();
                }
                if(Path==null||Path.Count==0)
                {

                    if (skipping)
                        Core.Main.MakeTurn();
                    skipping = false;
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        Core.Main.MakeTurn();
                    }

                    if (Input.GetKey(KeyCode.UpArrow))
                    {
                        if (Path != null)
                            Path.Clear();
                        MoveTo(Coords + new IntegerVector(0, 1));
                        Action();
                    }
                    if (Input.GetKey(KeyCode.DownArrow))
                    {
                        if (Path != null)
                            Path.Clear();
                        if (MoveTo(Coords + new IntegerVector(0, -1)))
                            Action();
                    }
                    if (Input.GetKey(KeyCode.LeftArrow))
                    {
                        if (Path != null)
                            Path.Clear();
                        if (MoveTo(Coords + new IntegerVector(-1, 0)))
                            Action();
                    }
                    if (Input.GetKey(KeyCode.RightArrow))
                    {
                        if (Path != null)
                            Path.Clear();
                        if (MoveTo(Coords + new IntegerVector(1, 0)))
                            Action();
                    }
                }
            }
        }
    }


    // Use this for initialization

    public override bool Move(IntegerVector am)
    {
        
        return base.Move(am);




    }

    public override void OnDeath()
    {
        Debug.Log("ALLO YOBA TI DEAD!");
        Core.Main.GamePause = true;
        PlayAnim(3);
        DefaultAnim = 4;
        Core.Main.DeathScreen.SetActive(true);
        
   
 
    }

    public void Attack(Creature target)
    {
        if (IntegerVector.Distance(Coords, target.Coords) > 1)
            PlayAnim(5);
        else
            PlayAnim(2);
        string t = "";
        int b = 0;
        float ig = 0;
        Debug.Log("Attacking" + target.name);
        
        foreach (Item i in Equipment)
        {
            if (i != null)
                foreach (Effect ef in i.effects)
                    if(ef!=null)
                    ef.OnAttack(target, ref b,ref t,ref ig, this);

        }
        b = b + Strength;

    }


    public void DropItem(int slotnum)
    {
        Equipment[slotnum].Owner = null;
        Equipment[slotnum] = null;
        
        ItemAvatars[slotnum].Stop();
        if(defaultanims[0, slotnum]!=null)
        ItemAvatars[slotnum].GetComponent<SpriteRenderer>().sprite = defaultanims[0,slotnum].sprites[0];
        PlayAnim(0);
        OnDidSomething();

    }

    public void EquipItem(Item it,int slotnum)
    {
        if (it == null)
            Debug.Log("Fail");
         it.OnEquip();
        Equipment[slotnum] = it;
      
        it.Owner = this;
        it.LoadAnims();

        OnDidSomething();
    }


    public override void Damage(Creature attacker, int damage, string type,float armorignore = 0, Item weapon = null)
    {
        float dodgesum=0;
        for(int i =0; i<Equipment.Length;i++)
        {
            if (Equipment[i] != null)
                dodgesum+=Equipment[i].DodgeBonus;
        }

        if (0.1f +dodgesum+ ((float)Agility-10) * 0.005f < Random.Range(0.0f, 1.0f))
        {
            foreach (Item i in Equipment)
            {
                if (i != null)
                    foreach (Effect ef in i.effects)
                        if(ef!=null)
                        ef.OnDefence(attacker,this, ref damage, ref type,armorignore);

            }
            base.Damage(attacker, damage, type);

            Health -= damage;
        }
        else
        {
            Core.Main.AddFloatText("Dodge", this.transform.position,Color.cyan);
            
        }
        
    }




}
