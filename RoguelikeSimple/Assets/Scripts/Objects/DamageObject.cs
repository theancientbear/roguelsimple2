﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SQLite;

public class DamageObject : Creature {

    public int turns;
    public int MinDamage;
    public string DamageType;
    public int MaxDamage;

    public override void OnNextTurn()
    {
        base.OnNextTurn();
        foreach (Creature cr in GameObject.FindObjectsOfType<Creature>() as Creature[])
        {
            if (cr.Coords == Coords)
            {
                cr.Damage(this, Random.Range(MinDamage, MaxDamage), DamageType);
            
            }
        }

        turns--;
        if (turns <= 0)
            Destroy(this.gameObject);
           
        
    }
}
