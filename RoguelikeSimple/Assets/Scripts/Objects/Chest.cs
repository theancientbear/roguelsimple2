﻿using UnityEngine;
using System.Collections.Generic;
using SQLite;

public class Chest : Decoration {


    public override void Start()
    {
        UseText = "Open";
        base.Start();

    }

    public override void Action()
    {
        
    }


    public override void Use()
    {
        base.OnNextTurn();
        if (IntegerVector.Distance(Player.Main.Coords, Coords) <= 1)
        {
            Debug.Log("GOTCHA!");
            using (var EntDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/LootTables.sqlite"))
            {
                List<FloorItemData> i = EntDB.Query<FloorItemData>("SELECT * FROM " + this.LootTable);
                float sum = 0;
                float r = Random.Range(0.0f, 1.0f);
                foreach (FloorItemData temp in i)
                {
                    sum += temp.Chance;
                    if (sum > r)
                    {
                        Core.Main.CreateItem(temp.ItemId, this.Coords);
                        break;
                    }
                }

            }

            InventoryManager.Main.UpdateInv();
            Core.Main.NextTurn.Remove(this);
            Destroy(UseButton.gameObject);
            Core.MainGrid.PointToTile[Coords].Passable = true;
            Destroy(this.gameObject);
        }


    }

    public override string SaveFormat()
    {
        string output = base.SaveFormat().Replace("Decoration", "Chest") ;
       


        return output;
    }
    }





