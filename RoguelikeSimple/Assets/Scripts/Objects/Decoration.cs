﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Decoration : Creature {


    [SerializeField]
    public bool Passable;
    [SerializeField]
    public bool Destructable;

    public string UseText;
    public UseDecorationButton UseButton;

    public override void Start()
    {
        base.Start();
        if (UseText != null)
        {
            UseButton = Core.Main.AddDecorationButton(this);
            UseButton.GetComponentInChildren<Text>().text = UseText;
        }
            Core.MainGrid.PointToTile[Coords].Passable = Passable;
    }


    public virtual void Use()
    {
        foreach(Effect i in  effects)
        {
            if(i!=null)
            i.OnUse(Player.Main);
        }

    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        UseButton.transform.position = this.transform.position;

    }


    public override void OnNextTurn()
    {
        base.OnNextTurn();

        if (IntegerVector.Distance(Player.Main.Coords, Coords) <= 1)
        {
            UseButton.gameObject.SetActive(true);
        }
        else
            UseButton.gameObject.SetActive(false);
    }

    public override void Damage(Creature attacker, int damage, string type, float armorignore = 0, Item weapon = null)
    {
        base.Damage(attacker, damage, type, armorignore, weapon);
        if (Destructable)
            Health--;
    }



    public override string SaveFormat()
    {
        
        string output = "Decoration#" + Coords.x.ToString() + "#" + Coords.y.ToString() + "#" + Passable.ToString() + "#"
            + Destructable.ToString() + "#" + UseText + "#" + Health.ToString() + "#" +LootTable+"#" + GetComponent<SpriteRenderer>().sprite.texture.name+"#";

        int count = 0;
        foreach (Effect ef in effects)
        {
            if (ef != null)
            {
                if (ef.SaveFormat() != null)
                {
                    output += ef.SaveFormat() + "@";
                    count++;
                }
            }
        }
        if (count > 0)
            output = output.Remove(output.Length - 1);
        if (count == 0)
            output += "null";



        return output;
    }


    public virtual void LoadFormat(string input)
    {
        string[] data = input.Split('#');

        Coords = new IntegerVector(int.Parse(data[1]),int.Parse(data[2]));

        Passable = bool.Parse(data[3]);
        Destructable = bool.Parse(data[4]);

        UseText = data[5];
        Health = int.Parse(data[6]);
        LootTable = data[7];
        Texture2D txt = Core.GetTextureFromFile(data[8]);
        this.GetComponent<SpriteRenderer>().sprite = Sprite.Create(txt,new Rect(0,0,txt.width,txt.height),Vector2.one/2,32);
    
        if(data[9]!="null")
        {

            List<Effect> efs = Effect.Parse(data[9]);

             foreach(Effect k in efs)
                this.AddEffect(k);

        }


        
    }

}
