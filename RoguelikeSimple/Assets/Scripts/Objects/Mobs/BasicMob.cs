﻿using UnityEngine;
using SQLite;
using System.Collections.Generic;

public class BasicMob : Creature {

    public int MinDamage;
    public int MaxDamage = 3;
    public int Armor = 2;
    public HealthBar healthBar;
    public int Range = 1;
    public string WeaponType = "Physical";
    
    public override void Start()
    {
        base.Start();
        
        Coords = IntegerVector.ConvertFromVector3(transform.position);
        Core.MainGrid.PointToTile[Coords].Passable = false;
       healthBar =  Core.Main.AddHealthBar(this);
        if (Health <= 0)
        {
         //   LootTable = "Default";
            OnDeath();
        }
    }

    public override void OnDeath()
    {
        base.OnDeath();

        using (var EntDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/LootTables.sqlite"))
        {
            try
            {
                Debug.Log(this.LootTable);

                List<FloorItemData> i = EntDB.Query<FloorItemData>("SELECT * FROM " + this.LootTable);


                foreach (FloorItemData temp in i)
                    if (temp.Chance > Random.Range(0.0f, 1.0f))
                    {
                        Core.Main.CreateItem(temp.ItemId, this.Coords);
                    }

                LootTable = "Default";

                
            }
            catch
            {
                Debug.LogWarning("Could not load " + this.LootTable + " loottable");
            }
            

        }
            Core.Main.OnNextTurn -= OnNextTurn;
        Core.MainGrid.PointToTile[Coords].Passable = true;
        this.GetComponent<Collider>().enabled = false;
        Destroy(healthBar.gameObject);
        
        PlayAnim(3);
        animations[0] = animations[4];
    }



    public override void Action()
    {

        bool SeenByPlayer = false;
        if (Player.Main.FieldOfView.Contains(this.Coords))
            SeenByPlayer = true;
        else
            SeenByPlayer = false;
       
        if (Turns < 0)
            Turns = 0;
        if (Path != null)
            if (Path.Count > 0)
            {

                if (Move(Path[0] - Coords))
                {
                    Path.RemoveAt(0);
                    Turns--;
                }
               
            }

        {
           if(Path!=null)
            { 

                if (IntegerVector.Distance(Coords, Player.Main.Coords) <=Range)
                {
                   
                    if (Turns >= 0)
                    {
                        if (IntegerVector.Distance(Coords, Player.Main.Coords) == 1)
                        {
                            Player.Main.Damage(this, (int)Random.Range(MinDamage, MaxDamage), WeaponType);
                            PlayAnim(2);
                            Turns--;
                        }
                        else
                        {
                            Debug.Log(name + " attacks from the distance");

                            if (SeenByPlayer)
                            {
                                Player.Main.Damage(this, (int)Random.Range(MinDamage, MaxDamage), WeaponType);
                                PlayAnim(5);
                                Turns--;
                            }


                        }

                       
                    }
                }
            }

            if (IntegerVector.Distance(Coords, Player.Main.Coords) < FOV / 10)
            {
                if (SeenByPlayer)
                {
                    if (IntegerVector.Distance(Coords, Player.Main.Coords) > Range)
                    {
                        MoveTo(Player.Main.Coords);
                    }
                    else
                    {
                        Path = new List<IntegerVector>();
                    }

                }
            }
            
            Turns--;
        }
        
        if (Turns > 0)
        {
            Action();
        }


    }


    public override void Damage(Creature attacker, int damage, string type, float armorignore = 0,Item weapon = null)
    {
        base.Damage(attacker, damage, type,armorignore,weapon);
        Health -= damage;
        if (Health <= 0)
            OnDeath();
    }
    void OnMouseDown()
    {
       

         
        
    }
    void OnMouseOver()
    {
        if (!Core.Main.Editing)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (!Core.Main.SelectionInProgress)
                {
                    if (!Core.Main.MakingTurn)
                    {
                        Player.Main.DMGAim = this;
                        //  this.Damage(Player.Main, Player.Main.DMG, "ss");

                        Player.Main.Action();
                    }
                }
                else
                {
                    // Core.Main.SelectionDone(Core.MainGrid.PointToTile[Coords]);
                }
            }
        }
    }

    public override string SaveFormat()
    {
        string output = "BasicMob#" + Coords.x.ToString() + "#" + Coords.y.ToString() + "#" + MinDamage.ToString() + "#" + MaxDamage.ToString() + "#" + Health.ToString() + "#" +
            MaxHealth.ToString() + "#" + Range.ToString() + "#" + WeaponType + "#" + LootTable + "#" + animations[0].sprites[0].texture.name + "#";
        int count = 0;



        foreach (Effect ef in effects)
        { 
            if (ef != null)
            {
                if (ef.SaveFormat() != null)
                {
                    output += ef.SaveFormat() + "@";
                    count++;
                }
            }
    }
        if (count > 0)
            output =output.Remove(output.Length - 1);
        if (count == 0)
            output += "null";

        return output;
    }
}
