﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltMob : BasicMob
{

    public int BoltCharges = 2;
    public int BoltRange = 3;
    public int BoltReload = 4;
    public int BoltRLD = 0;
    public int MaxBoltDamage=0;
    public int MinBoltDamage;
    public Sprite BoltProjectile;
    public string DamageType;
    public int ExplosionRadius;
    public string ExplosionEffect;

    public override void OnNextTurn()
    {
        if(BoltRLD>0)
        BoltRLD--;
        
        base.OnNextTurn();

    }

    public override void Action()
    {
        bool SeenByPlayer = false;
        if (Player.Main.FieldOfView.Contains(this.Coords))
            SeenByPlayer = true;
        else
            SeenByPlayer = false;

        if (Turns < 0)
            Turns = 0;
        if (Path != null)
            if (Path.Count > 0)
            {

                if (Move(Path[0] - Coords))
                {
                    Path.RemoveAt(0);
                    Turns--;
                }

            }

        {
            if (Path != null)
            {
                if(SeenByPlayer)
                    if (IntegerVector.Distance(Coords, Player.Main.Coords) <= BoltRange)
                    {
                    if (Turns >= 0)
                    {
                        if(BoltRLD<=0)
                        if (BoltCharges > 0)
                        {
                                BoltRLD = BoltReload;
                            BoltCharges--;
                            Turns--;

                            IntegerVector aim = Player.Main.Coords;
                            for (int i = -ExplosionRadius / 2; i < ExplosionRadius / 2 + 1; i++)
                                for (int k = -ExplosionRadius / 2; k < ExplosionRadius / 2 + 1; k++)
                                {
                                    IntegerVector newaim = aim + new IntegerVector(i, k);
                                    if (Core.MainGrid.PointToTile.ContainsKey(newaim))
                                    {
                                            Effect ef = Core.Main.CreateTierEffect(ExplosionEffect);
                                            Core.MainGrid.PointToTile[newaim].AddEffect(ef);
                                            ef.Launch(Core.MainGrid.PointToTile[newaim]);


                                            foreach (Creature creat in GameObject.FindObjectsOfType<Creature>() as Creature[])
                                        {
                                            if (creat.Coords == newaim)
                                                if (IntegerVector.Distance(creat.Coords, Player.Main.Coords) <= BoltRange)
                                                {
                                                    creat.Damage(this, (int)Random.Range(MinBoltDamage, MaxBoltDamage), DamageType, 0);
                                                    


                                                }

                                        }

                                    }
                                }
                            if (BoltProjectile != null)
                                Core.Main.CreateProjectile(this.Coords.ToVector3(), aim.ToVector3(), 10, BoltProjectile);

                        }
                    }
                    }
                if (Turns >= 0)
                {

                    if (IntegerVector.Distance(Coords, Player.Main.Coords) <= Range)
                    {


                        if (IntegerVector.Distance(Coords, Player.Main.Coords) == 1)
                        {
                            Player.Main.Damage(this, (int)Random.Range(MinDamage, MaxDamage), WeaponType);
                            PlayAnim(2);
                            Turns--;
                        }
                        else
                        {
                            Debug.Log(name + " attacks from the distance");

                            Player.Main.Damage(this, (int)Random.Range(MinDamage, MaxDamage), WeaponType);
                            PlayAnim(5);
                            Turns--;


                        }


                    }


                   
                }

                if (IntegerVector.Distance(Coords, Player.Main.Coords) < FOV / 10)
                {
                    if (SeenByPlayer)
                    {
                        if (IntegerVector.Distance(Coords, Player.Main.Coords) > Range)
                        {
                            MoveTo(Player.Main.Coords);
                        }
                        else
                        {
                            Path = new List<IntegerVector>();
                        }

                    }
                }

                Turns--;
            }

            if (Turns > 0)
            {
                Action();
            }


        }
    }


    public override string SaveFormat()
    {
        string output = base.SaveFormat().Replace("BasicMob","BoltMob")+ "#" + BoltCharges.ToString()+"#"+
    BoltRange.ToString()+"#"+BoltReload.ToString()+"#"+BoltRLD.ToString()+"#"+MaxBoltDamage.ToString()+"#"+MinBoltDamage.ToString()+"#"+BoltProjectile.texture.name+"#"+
    DamageType.ToString()+"#"+ExplosionEffect+"#"+ExplosionRadius.ToString();


 




        
        return output;
    }
}
