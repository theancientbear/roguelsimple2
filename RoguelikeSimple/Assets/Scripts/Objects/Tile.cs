﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tile : MonoBehaviour,IOnNextTurn,IEffectStuff {

    public bool Passable;
    public bool Transparent;
    private float temp = 0;
    public float period = 0.1f;
    public int maxstep;
    public int minstep=0;
     public int step = 0;
    public string texture;
    public bool Animated = true;
    public List<Effect> effects = new List<Effect>();
    Renderer r;
   


	// Use this for initialization
	void Start () {
        r = this.GetComponent<Renderer>();
        step = minstep;
        maxstep = (int)(1/r.material.mainTextureScale.x);
        if (r.material.mainTextureScale.x == 1)
            Animated = false;
       
       // Core.Main.NextTurn.Add(this);
        	
	}


    public virtual Effect AddEffect(string eff,string param,string Sprite)
    {
        Effect temp = Core.Main.CreateEffect(eff, param, Sprite);
        effects.Add(temp);
        return temp;
    }

    public virtual void Effect(Effect ef)
    {
        ef.AssocCreat = null;
    }


    public virtual void AddEffect(Effect s)
    {
      
        effects.Add(s);
        
    }


    public virtual Effect GetEffect(string descr)
    {
        foreach (Effect i in effects)
            if (i.type == descr)
                return i;
        return null;
    }

    public virtual void OnStep(Creature cr)
    {
        foreach (Effect f in effects)
            if (f != null)
                f.OnStep(cr);
            else
                Debug.Log(cr.name);
    }

    void OnMouseDown()
    {

        if (!Core.Main.Editing)
        {
            if (!Core.Main.MakingTurn)
            {
                if (!Core.Main.OnInterface && !Core.Main.SelectionInProgress)
                {
                    Player.Main.MoveTo(Core.MainGrid.TileToPoint[this]);

                    Player.Main.Action();
                }
                if (!Core.Main.OnInterface && Core.Main.SelectionInProgress)
                {
                    Core.Main.SelectionDone(this);
                }
            }
        }
    }
    void OnMouseOver()
    {

        if (!Core.Main.SelectionInProgress)
        {

            if (Passable)
                this.GetComponent<Renderer>().material.color = Color.green;
            else
            {
                this.GetComponent<Renderer>().material.color = Color.red;
            }
        }
        else
        {
            if(IntegerVector.Distance(Core.MainGrid.TileToPoint[this],Player.Main.Coords)<9)
            {
                this.GetComponent<Renderer>().material.color = Color.blue;

            }
            else
            {
                this.GetComponent<Renderer>().material.color = Color.black;
            }
        }
    }
    void OnMouseExit()
    {
        this.GetComponent<Renderer>().material.color = Color.white;
    }

    public void  OnNextTurn()
    {
        if (!found)
        {
            if (Player.Main.FOV < (Core.MainGrid.TileToPoint[this] - Player.Main.Coords).Trace())
                r.enabled = false;
            else
            {
                r.enabled = true;
                found = true;

            }
        }

        foreach (Effect ef in effects)
            ef.OnNextTurn(this);
        


    }
    bool found = false;


 protected virtual void FixedUpdate()
    {
      
        if(Animated)
        if (r.material.mainTextureScale.x != 1)
        {
            temp += Time.fixedDeltaTime;
            if (temp >= period)
            {

                temp = 0;
                step++;
                if (step >= maxstep)
                    step = minstep;

                r.material.mainTextureOffset = r.material.mainTextureScale * (float)step;
                

            }
        }
       
    }


}
