﻿using UnityEngine;
using System.Collections.Generic;
using System;
using SQLite;

public class Creature : MonoBehaviour, IEffectStuff,IOnNextTurn {
    public IntegerVector Coords = new IntegerVector(0, 0);
    public int FOV = 500;
    public int Turns = 1;
    public string ID = "";
    public string LootTable;
    public List<Effect> effects = new List<Effect>();
    public bool Moving = false;
    public Dictionary<string, string[]> SoundPack = new Dictionary<string, string[]>();
    public int Strength;
    public int Agility;
    public int Wisdom;
    public int DefaultAnim = 0;
    Renderer r;


    public bool animated = false;
    public Anim[] animations = new Anim[18];

    public int AnimPlaying = 0;
    protected int animstep;
    protected float animtime = 0;
    

    public bool skipping;


    public virtual void PlayAnim(int id)
    {
        if (animated)
        {
            if (id != AnimPlaying)
            {
                animstep = 0;
                animtime = 0;
                AnimPlaying = id;
            }

            else if (animstep >= animations[AnimPlaying].sprites.Length)
                animstep = 0;
        }
        
    }


    public void Update()
    {
        if(animated)
        {
            if(animtime> animations[AnimPlaying].period)
            {
                animtime = 0;
                animstep++;
                if(animstep>=animations[AnimPlaying].sprites.Length)
                {
                    if (animations[AnimPlaying].Loop)
                        PlayAnim(AnimPlaying);
                    else
                    {
                       
                        PlayAnim(DefaultAnim);
                    }
                }

                   (r as SpriteRenderer).sprite = animations[AnimPlaying].sprites[animstep];

       
            }
            else
            {
                animtime += Time.deltaTime;
            }

        }
    }

    public virtual void SkipTurn()
    {
        skipping = true;
    }


    public Tile CurrentTile
    {
        get { return Core.MainGrid.PointToTile[Coords]; }
        set { Coords = Core.MainGrid.TileToPoint[value]; }
    }


    public int Health = 10;
    public int MaxHealth = 10;

    public virtual void OnDeath()
    {

        

        Core.Main.NextTurn.Remove(this);

            foreach (Effect f in effects)
        {
            f.OnDeath(this);
        }
    }





   public virtual void AddEffect(Effect ef)
    {
        if (ef != null)
        {
            if (ef != null)
            {
                effects.Add(ef);


                ef.enabled = true;
            }
            ef.AssocCreat = this;
        }
       
    }


    public virtual void RemoveEffect(Effect ef)
    {
        effects.Remove(ef);
        ef.enabled = false;
       
        ef.AssocCreat = null;
        
    }


    public virtual void OnNextTurn()
    {
        Core.MainGrid.PointToTile[Coords].OnStep(this);
        for (int i = 0; i < effects.Count; i++)
            effects[i].OnNextTurn(this);

        if (Player.Main.FOV/10 < IntegerVector.Distance(Coords,Player.Main.Coords))
                r.enabled = false;
            else
            {
                r.enabled = true;

            }

            if(!skipping)
        Action();
        skipping = false;
        if (Health <= 0)
        {
            OnDeath();
        }

        
    }

    public virtual void Action()
    {
        
        if (Turns < 0)
            Turns = 0;
        if (Path != null)
            if (Path.Count > 0)
            {

                if (Move(Path[0] - Coords))
                {
                    Path.RemoveAt(0);
                    Turns--;
                }
                else
                    Path = Astar.FindThePath(Coords, Aim, FOV);
            }
        if (Turns > 0)
        {
            Action();
        }

    }


    public List<IntegerVector> Path = new List<IntegerVector>();
    public IntegerVector Aim;
	// Use this for initialization
	public virtual void Start () {

        Core.Main.NextTurn.Add(this);
        r = this.GetComponent<Renderer>();
        if (this.GetComponent <Player>() == null)
        this.transform.Rotate(-15, 0, 0);

    }
	
    public virtual bool MoveTo(IntegerVector vec)
    {
        Aim = vec;
        var Path1 = Astar.FindThePath(Coords, Aim,FOV);
        if(Path1!= null)
            Path = Path1 as List<IntegerVector>;
        if (Path1 == null)
            return false;
        else
            return true;
    }

    public virtual void Damage(Creature attacker, int damage, string type, float armorignore = 0,Item weapon = null)
    {
        foreach (Effect f in effects)
            f.OnDefence(attacker,this,ref damage, ref type,armorignore);
        
        Core.Main.AddFloatText(damage.ToString(), transform.position);
        if(attacker!=null)
        foreach (Effect f in attacker.effects)
            f.OnDamageDealt(this, damage,attacker);

        if (weapon != null)
        {
            foreach (Effect f in weapon.effects)
            {
                if (f != null)
                {
                    Debug.Log(effects.GetType());
                    f.OnDamageDealt(this, damage, attacker);
                }
                else
                    Debug.LogWarning("Something is wrong with the item");
            }
        }
        else
        {
            Debug.Log(attacker.name);
        }
            
                

    }



    public virtual bool Move(IntegerVector am)
    {
        PlayAnim(1);
        if ((am ).x > 0)
            this.transform.localScale = new Vector3(-1, 1, 1);
        if ((am ).x < 0)
            this.transform.localScale = new Vector3(1, 1, 1);


            foreach (Effect f in effects)
            f.OnMove();
        if (Core.MainGrid.PointToTile[new IntegerVector(Coords.x+ am.x, Coords.y+am.y)].Passable)
        {
            CurrentTile.Passable =true;
            this.Coords = new IntegerVector(Coords.x + am.x, Coords.y+am.y);
            Moving = true;

            CurrentTile.Passable = false;
            return true;
            
        }
        else
        {
            return false;
        }

    }



    public void PlaySoundActionOnPos(string action)
    {

        if (SoundPack.ContainsKey(action))
        {
             
            string[] soundArray = SoundPack[action];
            Core.Main.PlaySound(soundArray[UnityEngine.Random.Range(0, soundArray.Length)], Coords);
        }

    }






	// Update is called once per frame
	protected virtual void FixedUpdate () {





        if (Vector3.Distance(transform.position, Coords.x * Vector3.right + Coords.y * Vector3.up - Vector3.forward * 0.13f) < 0.1f)
        {
            transform.position = Coords.x * Vector3.right + Coords.y * Vector3.up-Vector3.forward*0.13f;
            Moving = false;
        }
        else
        {
            transform.position -= (transform.position-(Coords.x * Vector3.right + Coords.y * Vector3.up - Vector3.forward * 0.13f)).normalized*10*Time.deltaTime ;
            Moving = true;
        }
	
	}



    public virtual Effect GetEffect(string ID)
    {
        return null;
    }

    public virtual Effect AddEffect(string ID, string param,string Sprite)
    {
        Effect ef = Core.Main.CreateEffect(ID, param, Sprite);
        AddEffect(ef);
       return  ef;
    }


    public virtual string SaveFormat()
    {

        return null;
    }

}
