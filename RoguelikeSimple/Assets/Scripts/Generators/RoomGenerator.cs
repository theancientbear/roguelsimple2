﻿using UnityEngine;
using System.Collections;
using System.Linq;
using SQLite;
using System.Collections.Generic;



public static class RoomGenerator  {


    static List<Material> Floor = new List<Material>();
    static List<Material> Wall = new List<Material>();
    static  List<Material> WallSpecialUp = new List<Material>();
    static List<Material> Door = new List<Material>();
    static Material LadderUp;
    static int RoomNumber = 15;
    static int RoomSize = 20;
    static int RoomSizeDelt = 10;
    static int CorrWidth = 1;
    public static LevelData LevelData;

    public static void SetGen(int r, int k, int d, int w)
    {
        RoomNumber = r;
        RoomSize = k;
        RoomSizeDelt = d;
        CorrWidth = w;


    }



    static Tile AddTile(IntegerVector vec, bool Passable, bool Transparent, Material tex)
    {
        Tile t = (GameObject.Instantiate(Core.Main.tileprefab) as GameObject).GetComponent<Tile>();
        t.GetComponent<Renderer>().material = tex;
        
        //Core.MainGrid.Fog.Add(vec, (GameObject.Instantiate(Core.MainGrid.FogPrefab, vec.ToVector3(), new Quaternion) as GameObject).GetComponent<FogOfWar>());
        t.Transparent = Transparent;
        t.Passable = Passable;
        Core.MainGrid.InsertTile(vec, t);
        return t;
    }


    public static void GenerateMap(string Tileset, int seed)
    {

        //Random.seed = seed;


      
        
        using (var ItDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Floors.sqlite"))
        using (var TileDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/TileSets.sqlite"))
        {
            //Подгрузка информации о тайлах и спрайтов
            List<FloorItemData> ItemDB = ItDB.Query<FloorItemData>("SELECT * FROM " + LevelData.Itemset);
            List<FloorMobData> MobDB = ItDB.Query<FloorMobData>("SELECT * FROM " + LevelData.Mobset);
            List<TileData> list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'FLOOR'");
            List<FloorChestData> ChestDB = ItDB.Query<FloorChestData>("SELECT * FROM " + LevelData.ChestSet);

            foreach (TileData o in list)
            {
                Material m = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
                if (!o.Sprite.StartsWith("anim:"))
                {
                    m.mainTexture = Core.GetTextureFromFile(o.Sprite);
                    m.name = o.Sprite;
                    Floor.Add(m);
                }
                else
                {
                    m.mainTexture = Core.GetTextureFromFile(o.Sprite.Replace("anim:",""));
                    m.mainTextureScale = new Vector2((float)m.mainTexture.height / (float)m.mainTexture.width, 1);
                    Debug.Log(o.Sprite + m.mainTextureScale.ToString());
                    m.name = o.Sprite;
                    Floor.Add(m);

                }
            }
            list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'WALL'");
            

            foreach (TileData o in list)
            {
                Material m = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
                m.name = o.Sprite;
                if (!o.Sprite.StartsWith("anim:"))
                {
                    m.mainTexture = Core.GetTextureFromFile(o.Sprite);
  
                    Wall.Add(m);
                }
                else
                {
                    m.mainTexture = Core.GetTextureFromFile(o.Sprite.Replace("anim:", ""));
                    m.mainTextureScale = new Vector2((float)m.mainTexture.height / (float)m.mainTexture.width, 1);
                    Debug.Log(o.Sprite + m.mainTextureScale.ToString());

                    Wall.Add(m);

                }

            }
            list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'LADDERUP'");
            LadderUp = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
            LadderUp.mainTexture = Core.GetTextureFromFile(list[0].Sprite);
            LadderUp.name = list[0].Sprite;


            list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'WALLSPECIALUP'");

            foreach (TileData o in list)
            {

                Material m = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
                m.name = o.Sprite;
                m.mainTexture = Core.GetTextureFromFile(o.Sprite);
                WallSpecialUp.Add(m);
          

            }

            list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'DOOR'");

            foreach (TileData o in list)
            {
                Material m = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
                m.mainTexture = Core.GetTextureFromFile(o.Sprite);
                Door.Add(m);
                m.name = o.Sprite;

            }

            Debug.Log(list.Count);
            IntegerVector prevpos = new IntegerVector(25, 25) * (RoomSize + RoomSizeDelt);
          
            bool[,] rooms = new bool[50, 50];
            List<IntegerVector> Rooms = new List<IntegerVector>();
            IntegerVector shift = new IntegerVector(25, 25);
            List<IntegerVector> RoomsSize = new List<IntegerVector>();
            IntegerVector prevsize = new IntegerVector(0, 0);
            //Непосредственно генерация
            for (int i = 0; i < RoomNumber; i++)
            {

                IntegerVector size = new IntegerVector((int)Random.Range(RoomSize - RoomSizeDelt, RoomSize + RoomSizeDelt), (int)Random.Range(RoomSize - RoomSizeDelt, RoomSize + RoomSizeDelt));

                CreateRoom(shift*(RoomSize + RoomSizeDelt) - new IntegerVector(25,25)* (RoomSize + RoomSizeDelt ), shift * (RoomSize + RoomSizeDelt) - new IntegerVector(25, 25) * (RoomSize + RoomSizeDelt) + size);
                Rooms.Add(shift);
                RoomsSize.Add(size);
                rooms[shift.x,shift.y] = true;
                ConnectPoints(shift * (RoomSize + RoomSizeDelt) - new IntegerVector(25, 25) * (RoomSize + RoomSizeDelt) + size/2, prevpos - new IntegerVector(25, 25) * (RoomSize + RoomSizeDelt) + prevsize/2, CorrWidth);
                bool foundroom = false;
                Debug.Log("Room created at" + shift.y.ToString() + ":" + shift.x.ToString());

                IntegerVector pos = shift * (RoomSize + RoomSizeDelt) - new IntegerVector(25, 25) * (RoomSize + RoomSizeDelt);
                IntegerVector pos2 = pos + size;

                if(i==RoomNumber-1)
                {
                  Tile t =  AddTile(pos + size / 2, true, true, LadderUp);
                    t.gameObject.AddComponent<LadderUpTile>();
                }
                //int fin = (int)Random.Range(0, LevelData.ItemNumber);

                //do
                //{
                //    fin--;
                //    FloorItemData b = ItemDB[(int)Random.Range(0, ItemDB.Count)];
                //    Debug.Log(b.Chance);

                //    if (Random.Range(0, 1) < b.Chance)
                //    {
                //        Debug.Log(b.ItemId);
                //        Core.Main.CreateItem(b.ItemId, new IntegerVector((int)Random.Range(pos.x,pos2.x), (int)Random.Range(pos.y,pos2.y)));
                //    }
                //}
                //while (fin > 0);



                //fin = (int)Random.Range(LevelData.MobAmount, LevelData.MobAmountDelta);
                //do
                //{
                //    fin--;
                //    FloorMobData b = MobDB[(int)Random.Range(0, MobDB.Count)];
                //    Debug.Log(b.Chance);

                //    if (Random.Range(0, 1) < b.Chance)
                //    {
                //        Debug.Log(b.ID);
                //        Core.Main.CreateMob(b.ID, new IntegerVector((int)Random.Range(pos.x, pos2.x), (int)Random.Range(pos.y, pos2.y)));
                //    }
                //}
                //while (fin > 0);


                for (int k = 0; k<Rooms.Count;k++)
                {
                    
                    for(int y = 0; y<3;y++)
                    {
                        IntegerVector b = shift + new IntegerVector(Random.Range(-1, 2), Random.Range(-1, 2));
                        if(!rooms[b.x,b.y])
                        {
                           
                            shift = b;
                            foundroom = true;
                            rooms[b.x, b.y] = true;
                            prevsize = RoomsSize[k];
                            prevpos = Rooms[k]* (RoomSize + RoomSizeDelt);
                            break;
                        }
                        if (foundroom)
                            break;

                    }
                    if (foundroom)
                        break;
                }

                
             

               
            }




            int fin = (int)Random.Range(LevelData.ItemNumber, LevelData.ItemNumber+5);
            List<IntegerVector> coords = Core.MainGrid.PointToTile.Keys.ToList<IntegerVector>();
            while(fin>0)
            {
                fin--;



                float sum = 0;
                float r = Random.Range(0.0f, 1.0f);
                foreach (FloorItemData temp in ItemDB)
                {
                    sum += temp.Chance;
                    if (sum > r)
                    {
                        Core.Main.CreateItem(temp.ItemId, coords[(int)Random.Range(0, coords.Count)]);
                        break;
                    }
                }


            }


            fin = (int)Random.Range(LevelData.MobAmountAv, LevelData.MobAmountAv+ LevelData.MobAmountDelta);
            Debug.Log(fin);
            do
            {
                fin--;
                FloorMobData b = MobDB[(int)Random.Range(0, MobDB.Count)];
              






                if (Random.Range(0.0f, 1.0f) < b.Chance)
                {
                    Debug.Log("Spawned" +b.ID);
                    Core.Main.CreateMob(b.ID, coords[(int)Random.Range(0, coords.Count)]);
                }
                else
                    fin++;
            }
            while (fin > 0);

           

            fin = (int)Random.Range(LevelData.AvChestNumber, LevelData.AvChestNumber + LevelData.DeltaChestNumber);
            Debug.Log(fin);


            do
            {
                fin--;
                FloorChestData b = ChestDB[(int)Random.Range(0, ChestDB.Count)];


                if (Random.Range(0.0f, 1.0f) < b.Chance)
                {
                    Debug.Log("Chest " + b.ID);
                    Core.Main.CreateChest(b.ID, coords[(int)Random.Range(0, coords.Count)]);
                }

            }
            while (fin > 0);


            // ConnectPoints(new IntegerVector(4, 4), new IntegerVector(-1, -1), 2);
            GenerateWalls();

        }
    }


    static void ConnectPoints(IntegerVector a, IntegerVector b, int width)
    {

        for (int i = 0; i < (int)Mathf.Abs(a.x - b.x); i++)
        {
            for (int j = 0; j < width; j++)
            {
               


                    AddTile(new IntegerVector(b.x + i * (int)Mathf.Sign(a.x - b.x), a.y + j), true, true, Floor[0]);

            }

        }
       
        for (int i = 0; i < (int)Mathf.Abs(a.y - b.y); i++)
        {
            for (int j = 0; j < width; j++)
                AddTile(new IntegerVector(b.x + j, b.y + i * (int)Mathf.Sign(a.y - b.y)), true, true, Floor[0]);

        }




    }


    static void CreateRoom(IntegerVector a, IntegerVector b)
    {
        Debug.Log("Startig room generation at" + a.x + ":" + a.y + "  -  " + b.x.ToString() + ":" + b.y.ToString());
        for (int i = a.x; i <  b.x; i++)
        {
            for (int j = a.y; j <  b.y; j++)
            {
                AddTile(new IntegerVector( i, j), true, true, Floor[Random.Range(0,Floor.Count)]);
            }

        }

        
    }

    static void GenerateWalls()
    {
        List<IntegerVector> list = Core.MainGrid.PointToTile.Keys.ToList<IntegerVector>();
        foreach (IntegerVector i in list)
        {
            for (int j = -1; j <= 1; j++)
                for (int k = -1; k <= 1; k++)
                    if (!Core.MainGrid.PointToTile.ContainsKey(new IntegerVector(i.x + k, i.y + j)))
                        if ( j == 1)
                        {
                            if (WallSpecialUp.Count > 0)
                            {
                                if (Random.Range(0.0f, 1.0f) > 0.1f)
                                    AddTile(new IntegerVector(i.x + k, i.y + j), false, false, Wall[(int)Random.Range(0, Wall.Count )]);
                                else
                                {
                                    
                                    AddTile(new IntegerVector(i.x + k, i.y + j), false, false, WallSpecialUp[(int)Random.Range(0, WallSpecialUp.Count)]);
                                }

                            }
                            else
                            {
                                AddTile(new IntegerVector(i.x + k, i.y + j), false, false, Wall[(int)Random.Range(0, Wall.Count)]);
                            }
                        }
                         else
                            AddTile(new IntegerVector(i.x + k, i.y + j), false, false, Wall[(int)Random.Range(0, Wall.Count)]);


        }
    }
}
