﻿using UnityEngine;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using SQLite;
using System.Collections;
using System.Linq;

public class BaseGenerator2 : MonoBehaviour {

    static List<Material> Floor = new List<Material>();
    static List<Material> Wall = new List<Material>();
    static Material LadderUp;
    static int RoomNumber = 15;
    static int RoomSize = 20;
    static int RoomSizeDelt = 10;
    static int CorrWidth = 1;
    public static LevelData LevelData;

    public static void SetGen(int r, int k, int d, int w)
    {
        RoomNumber = r;
        RoomSize = k;
        RoomSizeDelt = d;
        CorrWidth = w;


    }



    static Tile AddTile(IntegerVector vec, bool Passable, bool Transparent, Material tex)
    {
        Tile t = (GameObject.Instantiate(Core.Main.tileprefab) as GameObject).GetComponent<Tile>();
        t.GetComponent<Renderer>().material = tex;


        t.Transparent = Transparent;
        t.Passable = Passable;
        Core.MainGrid.InsertTile(vec, t);
        return t;
    }

    public static void GenerateMap(string Tileset, int seed)
    {

        Random.seed = seed;
        using (var ItDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Floors.sqlite"))
        using (var TileDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/TileSets.sqlite"))
        {

            List<FloorItemData> ItemDB = ItDB.Query<FloorItemData>("SELECT * FROM " + LevelData.Itemset);
            List<FloorMobData> MobDB = ItDB.Query<FloorMobData>("SELECT * FROM " + LevelData.Mobset);
            List<TileData> list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'FLOOR'");
            foreach (TileData o in list)
            {
                Material m = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
                m.mainTexture = Core.GetTextureFromFile(o.Sprite);
                Floor.Add(m);
            }
            list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'WALL'");

            foreach (TileData o in list)
            {
                Material m = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
                m.mainTexture = Core.GetTextureFromFile(o.Sprite);
                Wall.Add(m);

            }
            list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'LADDERUP'");
            LadderUp = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
            LadderUp.mainTexture = Core.GetTextureFromFile(list[0].Sprite);


            Debug.Log(list.Count);
            int xx = 0;
            int xy = 0;
            for (int i = 0; i < RoomNumber; i++)
            {
                int x = (int)Random.Range(-30, 30);
                int y = (int)Random.Range(-30, 30);
                int w = (int)Random.Range(RoomSize, RoomSize + RoomSizeDelt);
                int h = (int)Random.Range(RoomSize, RoomSize + RoomSizeDelt);

                CreateRoom(new IntegerVector(x + w / 2, y + h / 2), new IntegerVector(x - w / 2, y - h / 2));
                int fin = (int)Random.Range(0, LevelData.ItemNumber);

                do
                {
                    fin--;
                    FloorItemData b = ItemDB[(int)Random.Range(0, ItemDB.Count)];
                    Debug.Log(b.Chance);

                    if (Random.Range(0, 1) < b.Chance)
                    {
                        Debug.Log(b.ItemId);
                        Core.Main.CreateItem(b.ItemId, new IntegerVector((int)Random.Range(x - w / 2, x + w / 2), (int)Random.Range(y - h / 2, y + h / 2)));
                    }
                }
                while (fin > 0);



                fin = (int)Random.Range(LevelData.MobAmountAv, LevelData.MobAmountAv+ LevelData.MobAmountDelta);
                Debug.Log(fin);
                do
                {
                    fin--;
                    FloorMobData b = MobDB[(int)Random.Range(0, MobDB.Count)];
                    Debug.Log(b.Chance);

                    if (Random.Range(0.0f, 1.0f) < b.Chance)
                    {
                        Debug.Log("spawned " +b.ID);
                        Core.Main.CreateMob(b.ID, new IntegerVector((int)Random.Range(x - w / 2, x + w / 2), (int)Random.Range(y - h / 2, y + h / 2)));
                    }
                    else
                        fin++;
                }
                while (fin > 0);


                ConnectPoints(new IntegerVector(x, y), new IntegerVector(xx, xy), CorrWidth);
                xx = x;
                xy = y;
                if (i >= RoomNumber - 1)
                {
                    Tile t = AddTile(new IntegerVector(x, y), true, true, LadderUp);
                    t.gameObject.AddComponent<LadderUpTile>();
                }
            }


            // ConnectPoints(new IntegerVector(4, 4), new IntegerVector(-1, -1), 2);
            GenerateWalls();

        }
    }


    static void ConnectPoints(IntegerVector a, IntegerVector b, int width)
    {

        for (int i = 0; i < (int)Mathf.Abs(a.x - b.x); i++)
        {
            for (int j = 0; j < width; j++)
                AddTile(new IntegerVector(b.x + i * (int)Mathf.Sign(a.x - b.x), a.y + j), true, true, Floor[0]);

        }
        for (int i = 0; i < (int)Mathf.Abs(a.y - b.y); i++)
        {
            for (int j = 0; j < width; j++)
                AddTile(new IntegerVector(b.x + j, b.y + i * (int)Mathf.Sign(a.y - b.y)), true, true, Floor[0]);

        }

      

    }


    static void CreateRoom(IntegerVector a, IntegerVector b)
    {
        for (int i = 0; i < a.x - b.x; i++)
        {
            for (int j = 0; j < a.y - b.y; j++)
            {
                AddTile(new IntegerVector(b.x + i, b.y + j), true, true, Floor[Random.Range(0,Floor.Count)]);
            }

        }
    }

    static void GenerateWalls()
    {
        List<IntegerVector> list = Core.MainGrid.PointToTile.Keys.ToList<IntegerVector>();
        foreach (IntegerVector i in list)
        {
            for (int j = -1; j <= 1; j++)
                for (int k = -1; k <= 1; k++)
                    if (!Core.MainGrid.PointToTile.ContainsKey(new IntegerVector(i.x + k, i.y + j)))
                        AddTile(new IntegerVector(i.x + k, i.y + j), false, false, Wall[0]);
        }
    }
}
