﻿using UnityEngine;
using System.Collections;

public class Rechargeable : Effect {


    public int MaxCharges;
    public int Charges;


    public Rechargeable(int a)
    {
        MaxCharges = a;
        Charges = a;
    }

    private int t = 0;
    public override void OnNextTurn(Creature owner)
    {
        if (Charges < MaxCharges)
            t++;
        if (t > 2)

        {
            Charges++;
            t = 0;
        }
        base.OnNextTurn(owner);
        if (Charges > 0)
            foreach (Effect i in this.owner.effects)
                i.enabled = true;
         else
            foreach (Effect i in this.owner.effects)
                i.enabled = false;



    }


    public override void OnUse(Creature c)
    {
        Charges--;
        Player.Main.OnDidSomething();
    }

}
