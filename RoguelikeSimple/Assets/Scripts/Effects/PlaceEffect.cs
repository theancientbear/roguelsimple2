﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceEffect : Effect {


    public PlaceEffect(string info)
    {

        LoadFormat(info);
    }


    public string placedeffect;


    public PlaceEffect(string ef, bool tm = false, int trn = 0)
    {
        placedeffect = ef;
        if(tm)
        {
            Timed = tm;
            Turns = trn;
        }
    }

    public override void OnUse(Creature c)
    {

        c.AddEffect(Core.Main.CreateTierEffect(placedeffect));
        
        base.OnUse(c);
    }


    public override string SaveFormat()
    {
        string output = "dummyuse|" + placedeffect;
        return output;
    }
    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        placedeffect = data[1];


    }

}
