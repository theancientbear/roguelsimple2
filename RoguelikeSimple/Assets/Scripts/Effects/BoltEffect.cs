﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltEffect : Effect {



    public int MinDamage = 1;
    public int MaxDamage = 3;
    public int MinHealth;
    public int MaxHealth;
    public string DamageType;
    public int Distance = 9;
    public Sprite Projectile;
    public float ArmorIgnore = 0;
    public string AdjointedEffect;
    Creature temp;


    public BoltEffect(string info)
    {
        LoadFormat(info);
    }


    public BoltEffect(int MinDam, int MaxDam,int MinHp, int MaxHP,int Dist, string eff,string DmgType,float ArmorIgnore,Texture2D projectile = null )
    {
        MinDamage = MinDam;
        MaxDamage = MaxDam;
        DamageType = DmgType;
        Debug.Log("Created BoltSpell with effect " + eff);
        MinHealth = MinHp;
        MaxHealth = MaxHP;
        Distance = Dist;
        AdjointedEffect = eff;
        if(projectile!=null)
        Projectile = Sprite.Create(projectile, new Rect(0, 0, projectile.width, projectile.height), Vector2.one/2,32f);
    }

    public override void OnUse(Creature c)
    {
        
        base.OnUse(c);
        temp = c;
        Debug.Log("Using boltspell");
        Core.Main.AskForSelection(this);
    }

    public override void OnSelection(Tile t)
    {
        base.OnSelection(t);
        Debug.Log("boltspell got target");
        foreach (Creature creat in GameObject.FindObjectsOfType<Creature>() as Creature[])
        {
            if (creat.Coords == Core.MainGrid.TileToPoint[t])
                if (IntegerVector.Distance(creat.Coords, temp.Coords) <= Distance)
                {
                    creat.Damage(temp, (int)Random.Range(MinDamage, MaxDamage), "DmgType",ArmorIgnore);
                    temp.Damage(temp, Mathf.Max((int)Random.Range(MinHealth, MaxHealth) - Player.Main.Wisdom/5,0), "sacrifice",1);
                    Player.Main.IntExp += 50;
                    if(AdjointedEffect!=null)
                        if(AdjointedEffect!="")
                    creat.AddEffect( Core.Main.CreateTierEffect(AdjointedEffect));
                    if (Projectile != null)
                        Core.Main.CreateProjectile(temp.Coords.ToVector3(), creat.Coords.ToVector3(), 10, Projectile);
                    Player.Main.OnDidSomething();
                }

        }
        
    }

    public override string SaveFormat()
    {
        string output = "bolteffect|" + MinDamage.ToString() + "|" + MaxDamage.ToString() + "|" + MinHealth.ToString() + "|" + MaxHealth.ToString() + "|" +
            Distance.ToString() + "|" + AdjointedEffect + "|" + DamageType + "|" + ArmorIgnore.ToString() + "|"  + Projectile.name;
        return output;
    }
    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        MinDamage = int.Parse(data[1]);
        MaxDamage = int.Parse(data[2]);
        MinHealth = int.Parse(data[3]);
        MaxHealth = int.Parse(data[4]);
        Distance = int.Parse(data[5]);
        AdjointedEffect = data[6];
        DamageType = data[7];
        ArmorIgnore = float.Parse(data[8]);

        Texture2D txt = Core.GetTextureFromFile(data[9]);
        Projectile = Sprite.Create(txt, new Rect(0, 0, txt.width, txt.height), Vector2.one / 2);
    }
}
