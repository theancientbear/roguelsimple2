﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaEffect : Effect {
    public int MinDamage = 1;
    public int MaxDamage = 3;
    public int MinHealth;
    public int MaxHealth;
    public string DamageType;
    public int Distance = 9;
    public Sprite Projectile;
    public float ArmorIgnore = 0;

    public int Radius = 3;
    public string TileEffect;

    Creature temp;


    public AreaEffect(string info)
    {

        LoadFormat(info);
    }

    public AreaEffect(int MinDam, int MaxDam, int MinHp, int MaxHP, int Dist, string eff, string DmgType, float ArmorIgnore, int radius, Texture2D projectile = null)
    {
        MinDamage = MinDam;
        MaxDamage = MaxDam;
        DamageType = DmgType;
        MinHealth = MinHp;
        MaxHealth = MaxHP;
        Distance = Dist;
        TileEffect = eff;
        Radius = radius;
        if (projectile != null)
            Projectile = Sprite.Create(projectile, new Rect(0, 0, projectile.width, projectile.height), Vector2.zero);
    }

    public override void OnUse(Creature c)
    {
        base.OnUse(c);
        temp = c;
        Core.Main.AskForSelection(this);
    }

    public override void OnSelection(Tile t)
    {
        base.OnSelection(t);
        IntegerVector aim = Core.MainGrid.TileToPoint[t];
        for (int i = -Radius / 2; i < Radius / 2 + 1; i++)
            for (int k = -Radius / 2; k < Radius / 2 + 1; k++)
            {
                IntegerVector newaim = aim + new IntegerVector(i, k);
                if (Core.MainGrid.PointToTile.ContainsKey(newaim))
                {
                    Effect ef = Core.Main.CreateTierEffect(TileEffect);
                    Core.MainGrid.PointToTile[newaim].AddEffect(ef);
                    ef.Launch(Core.MainGrid.PointToTile[newaim]);


                    foreach (Creature creat in GameObject.FindObjectsOfType<Creature>() as Creature[])
                    {
                        if (creat.Coords == newaim)
                            if (IntegerVector.Distance(creat.Coords, temp.Coords) <= Distance)
                            {
                                creat.Damage(temp, (int)Random.Range(MinDamage, MaxDamage), DamageType, ArmorIgnore);
                                temp.Damage(temp, (int)Random.Range(MinHealth, MaxHealth), "sacrifice", 1);
                                Player.Main.IntExp += 50;


                            }

                    }

                }
            }
        if (Projectile != null)
            Core.Main.CreateProjectile(temp.Coords.ToVector3(), aim.ToVector3(), 10, Projectile);
        Player.Main.OnDidSomething();
    }


    public override string SaveFormat()
    {
        string output = "areaeffect|"+MinDamage.ToString()+"|"+MaxDamage.ToString()+"|"+ MinHealth.ToString()+"|"+MaxHealth.ToString()+"|"+
            Distance.ToString()+"|"+TileEffect+"|"+DamageType+"|"+ArmorIgnore.ToString()+"|"+Radius.ToString()+"|"+Projectile.name;
        return output;
    }

    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        MinDamage = int.Parse(data[1]);
        MaxDamage = int.Parse(data[2]);
        MinHealth = int.Parse(data[3]);
        MaxHealth = int.Parse(data[4]);
        Distance = int.Parse(data[5]);
        TileEffect = data[6];
        DamageType = data[7];
        ArmorIgnore = float.Parse(data[8]);
        Radius = int.Parse(data[9]); ;
        Texture2D txt = Core.GetTextureFromFile(data[10]);
        Projectile = Sprite.Create(txt, new Rect(0, 0, txt.width, txt.height), Vector2.one / 2);
    }
}
