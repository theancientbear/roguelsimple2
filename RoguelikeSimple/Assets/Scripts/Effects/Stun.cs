﻿using UnityEngine;
using System.Collections;

public class Stun : Effect{

    public int turns;
    public AnimatedSprite spr;
    public Texture2D txt;


    public Stun(string info)
    {

        LoadFormat(info);
    }

    public Stun(int tu, Texture2D tx)
    {
        turns = tu;
        txt = tx;
    }


    public override void OnDeath(Creature owner)
    {

        if(spr!=null)
            GameObject.Destroy(spr.gameObject);
        Debug.Log("Stun target DED!1111");
        turns = 0;
        
        base.OnDeath(owner);
    }

    public override void OnNextTurn(Creature owner)
    {




        if (txt != null)
            if (spr == null)
                spr = Core.Main.CreateFollowingSprite(txt, owner.gameObject);
        turns--;
        owner.SkipTurn();
        base.OnNextTurn(owner);
        if (turns <= 0)
        {
            GameObject.Destroy(spr.gameObject);
            owner.RemoveEffect(this);
        }
    }


    public override string SaveFormat()
    {
        string output = "stun|" + turns.ToString() + "|" + txt.name;
        return output;
    }

    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        turns = int.Parse(data[1]);
        txt = Core.GetTextureFromFile(data[2]);
    }

}
