﻿using UnityEngine;
using System.Collections;

public class Poisoned :Effect {

    public int turns = 3;
    public int Damage = 1;
    public string DamageType;
    public Texture2D txt;
    public AnimatedSprite spr;

    public Poisoned(string info)
    {

        LoadFormat(info);
    }


    public Poisoned(int turns, int DMG,string type,Texture2D tt)
    {
        this.turns = turns;
        Damage = DMG;
        DamageType = type;
        txt = tt;
    }


    public override void OnNextTurn(Creature owner)
    {

        if (txt != null)
            if (spr == null)
                spr = Core.Main.CreateFollowingSprite(txt, owner.gameObject);

        turns--;
        owner.Damage(null, Damage, DamageType);
        if (turns <= 0)
        {
            GameObject.Destroy(spr.gameObject);
            owner.RemoveEffect(this);
            
        }
        
        base.OnNextTurn(owner);

    }
    public override string SaveFormat()
    {
        string output = "damageovertime|" + turns.ToString() + "|" + Damage.ToString() + "|" + DamageType + "|" + txt.name;
        return output;
    }


    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        turns = int.Parse(data[1]);
        Damage = int.Parse(data[2]);
        DamageType = data[3];
        txt = Core.GetTextureFromFile(data[4]);


    }
}
