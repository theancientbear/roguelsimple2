﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorIgnore : Effect {


    public float Decrease;
    public ArmorIgnore (float dcr)
    {
        Decrease =  dcr;
    }
    public override void OnAttack(Creature Aim, ref int damage, ref string type, ref float armorignore,  Creature attacker)
    {
        if (enabled)
        {
            armorignore = (1 - (1 - armorignore) * Decrease);
            base.OnAttack(Aim, ref damage, ref type, ref armorignore, attacker);
        }
    }





       


}
