﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyAttack : Effect {


    public float chance;
    public string effect;
   

    public DummyAttack(string info)
    {

        LoadFormat(info);
    }


    public DummyAttack(float ch, string ef)
    {
        effect = ef;
        chance = ch;
    }

    public override void OnDamageDealt(Creature aim, int damage, Creature attack,Item weapon = null)
    {

        Debug.Log("DummyAttack worked");
    
        if(attack==null || attack!=aim)
        if (Random.Range(0.0f, 0.0f) < chance)
        {
            aim.AddEffect(Core.Main.CreateTierEffect(effect));
            Debug.Log("Adding effect on attack " + effect);
        }
        base.OnDamageDealt(aim, damage,attack,weapon);
    }



    public override void OnAttack(Creature Aim, ref int damage, ref string type, ref float armorignore, Creature attacker)
    {



        base.OnAttack(Aim, ref damage, ref type, ref armorignore, attacker);

    }


    public override string SaveFormat()
    {
        string result = "dummyattack|" + chance.ToString() + "|" + effect;
        return result;
    }

    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        chance =float.Parse(data[1]);
        effect = data[2];

    }
}
