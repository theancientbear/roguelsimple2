﻿using UnityEngine;
using System.Collections;

public interface IEffectStuff  {

    Effect AddEffect(string ID, string Param,string Sprite);
    void AddEffect(Effect ef);

    Effect GetEffect(string ID);

}
