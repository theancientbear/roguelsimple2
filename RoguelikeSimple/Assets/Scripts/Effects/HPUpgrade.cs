﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPUpgrade : Effect{

    public int hp;


    public HPUpgrade(string info)
    {
        LoadFormat(info);
    }

    public HPUpgrade(int health)
    {
        hp = health;
        
    }

    public override void OnUse(Creature c)
    {
        Player.Main.BaseHP += hp;
        base.OnUse(c);
    }
    public override string SaveFormat()
    {
        string result = "hpupgrade|" + hp.ToString();
        return result;
    }

    public override void LoadFormat(string info)
    {
        hp = int.Parse(info.Split('|')[1]);
        base.LoadFormat(info);
    }
}
