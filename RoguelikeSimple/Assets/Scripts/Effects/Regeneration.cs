﻿using UnityEngine;
using System.Collections;

public class Regeneration : Effect {

    public int turns = 3;
    public int Damage = 1;
    public AnimatedSprite spr;
    public Texture2D txt;



    public Regeneration(string info)
    {

        LoadFormat(info);
    }

    public Regeneration(int turns, int DMG,Texture2D sp)
    {
        this.turns = turns;
        Damage = DMG;
        if(sp!=null)
        txt = sp;
       

    }


    public override void OnNextTurn(Creature owner)
    {
        if(txt!=null)
        if (spr == null)
            spr = Core.Main.CreateFollowingSprite(txt, owner.gameObject);
        turns--;
        owner.Health += Damage;
        if (owner.Health > owner.MaxHealth)
            owner.Health = owner.MaxHealth;
        if (turns <= 0)
        {
            GameObject.Destroy(spr.gameObject);
            owner.RemoveEffect(this);
            
        }

        base.OnNextTurn(owner);

    }


    public override string SaveFormat()
    {
        string output = "regeneration|" + turns.ToString() + "|" + Damage.ToString() + "|" + txt.name;
        return output;
    }

    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        turns = int.Parse(data[1]);
        Damage = int.Parse(data[2]);
        txt = Core.GetTextureFromFile(data[3]);
    }
}
