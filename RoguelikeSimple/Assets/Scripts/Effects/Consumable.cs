﻿using UnityEngine;
using System.Collections;

public class Consumable : Effect {
    public int Charges = 3;

    public Consumable(string info)
    {

        LoadFormat(info);
    }

    public Consumable(int a)
    {
        Charges = a;
    }

    public override void OnUse(Creature c)
    {
        Charges--;
        Debug.Log(Charges);
        if (Charges <= 0)
            GameObject.Destroy(this.owner.gameObject);
        Player.Main.OnDidSomething();
       
    }


    public override string SaveFormat()
    {
        string output = "consumable|"+Charges.ToString();
        return output;
    }


    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        Charges = int.Parse(data[1]);
        
    }
}
