﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class InflamationEffect : Effect {


    public int turns;
    public int MinDamage;
    public int MaxDamage;
    public string DamageType = "Fire";
    public Texture2D anim;
    public AnimatedSprite spr;
    public Texture2D txt;
    public Tile Owner;


    public InflamationEffect(string info)
    {
        LoadFormat(info);
    }


    public InflamationEffect(int t, int mnd,int mxd,string DamageType ,Texture2D spr)
    {
        anim = spr;
        turns = t;
        MinDamage = mnd;
        MaxDamage = mxd;
        txt = spr;
        this.DamageType = DamageType;
        Core.Main.NextTurn.Add(this);

    }


    public override void OnNextTurn(Tile tile)
    {




    }


    public override void Launch(MonoBehaviour b)
    {
        Owner = b as Tile;
        base.Launch(b);
    }

    public override void OnNextTurn()
    {

        
        Debug.Log("tick on tile " + Core.MainGrid.TileToPoint[Owner].x + "/" + Core.MainGrid.TileToPoint[Owner].x);
        if (txt != null)
            if (spr == null)
                spr = Core.Main.CreateFollowingSprite(txt, Owner.gameObject);
       base.OnNextTurn();
        turns--;
        if (turns <= 0)
        {
            GameObject.Destroy(spr.gameObject);
            Owner.effects.Remove(this);
        }


    }


    public override void OnStep(Creature cr)
    {
        base.OnStep(cr);
        cr.Damage(null, Random.Range(MinDamage, MaxDamage), DamageType, 0);
    }

    public override string SaveFormat()
    {
        string result = "inflamation|" + turns.ToString()+"|"+MinDamage.ToString()+"|"+MaxDamage.ToString()+"|"
            + DamageType.ToString()+"|"+txt.name;
        return result;
    }


    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        turns = int.Parse(data[1]);
        MinDamage = int.Parse(data[2]);
        MaxDamage = int.Parse(data[3]);
        DamageType = data[4];
        txt = Core.GetTextureFromFile(data[5]);

    }

}
