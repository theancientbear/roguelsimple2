﻿using UnityEngine;
using System.Collections;

public class Fireball :Effect{


    public int MinDamage = 1;
    public int MaxDamage = 3;
    public int Dist = 9;
    Creature temp;


    public Fireball(int Min, int Max)
    {
        MinDamage = Min;
        MaxDamage = Max;
    }

    public override void OnUse(Creature c)
    {


        Debug.Log("Fireball used");
        base.OnUse(c);
        temp = c;
        Core.Main.AskForSelection(this);
    }

    public override void OnSelection(Tile t)
    {
        base.OnSelection(t);
        foreach(Creature creat in GameObject.FindObjectsOfType<Creature>() as Creature[])
        {
            if(creat.Coords == Core.MainGrid.TileToPoint[t])
                if(IntegerVector.Distance(creat.Coords, temp.Coords)<=9)
                {
                    creat.Damage(temp, (int)Random.Range(MinDamage, MaxDamage), "Fire");
                }
            
        }
        Player.Main.OnDidSomething();
    }

    // Use this for initialization

}
