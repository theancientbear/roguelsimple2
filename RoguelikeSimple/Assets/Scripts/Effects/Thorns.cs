﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thorns : Effect {

      public int MaxDamage;
    public int MinDamage;


    public Thorns(string info)
    {

        LoadFormat(info);
    }

    public  Thorns(int a, int b)
    {
        MinDamage = a;
        MaxDamage = b;
    }

    public override void OnDefence(Creature Attacker, Creature Defender, ref int damage, ref string type,float armorignore = 0)
    {
     
        if(Attacker!=null)
        if(IntegerVector.Distance(Attacker.Coords,Defender.Coords)<=1)
        {
            Attacker.Damage(null, Random.Range(MinDamage,MaxDamage), "Physical");
        }
       

        base.OnDefence(Attacker,Defender, ref damage, ref type,armorignore);
    }

    public override string SaveFormat()
    {
        string output = "Thorns|" + MinDamage.ToString() + "|" + MaxDamage.ToString();
        return output;
    }

    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        MinDamage = int.Parse(data[1]);
        MaxDamage = int.Parse(data[2]);
    }


}
