﻿using UnityEngine;
using System.Collections;

public class PoisonousTouch : Effect {


    public int Time;
    public int Dmg;
    public string sprite;

    public PoisonousTouch(int t, int dm,string spr)
    {
        Time = t;
        Dmg = dm;
        sprite = spr;
        Debug.Log("Poisonous touch initialized");
    }

    public override void OnHit(Creature Aim, ref int damage, ref string type, Creature attacker)
    {
        base.OnHit(Aim, ref damage, ref type, attacker);

        Aim.AddEffect("poisoned", Time.ToString() + ',' + Dmg.ToString()+",Poison",sprite);
    }
}
