﻿using UnityEngine;
using System.Collections;

public class Heal : Effect {


    public int amount;

    public Texture2D txt;



    public Heal(string info)
    {
        LoadFormat(info);
    }

    public Heal(int a, Texture2D tt)
    {
        amount = a;
        txt = tt;

    }

    public override void OnUse(Creature c)
    {
        base.OnUse(c);
        c.Health += amount;
        if (txt != null)
               Core.Main.CreateFollowingSprite(txt,c.gameObject,true);

        if (c.Health > c.MaxHealth)
            c.Health = c.MaxHealth;
        
        Player.Main.OnDidSomething();
        
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public override string SaveFormat()
    {
        string output = "heal|" + amount.ToString()+"|" + txt.name;
        return output;
    }



    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        amount= int.Parse(data[1]);
        txt = Core.GetTextureFromFile(data[2]);


    }

}
