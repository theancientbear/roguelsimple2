﻿using UnityEngine;
using System.Collections.Generic;

public class Effect:IToTile, IOnNextTurn {

   
    public Creature AssocCreat;
    public bool enabled = true;
    public int Power;
    public Item owner;
    public string type;
    public int Turns;
    public bool Timed = false;

    public  Effect()
    {

    }
   
  
    public virtual string SaveFormat()
    {
        return null;
    }

    public virtual void ToTile(IntegerVector vec)
    {

    }
    public virtual void OnStart()
    {
       

        
    }

    public virtual void OnUse(Creature c)
    {
        if (!enabled)
            return;
    }

    public virtual void OnHit(Creature Aim, ref int damage, ref string type, Creature attacker)
    {

    }
    public virtual void OnNextTurn(Creature owner)
    {

    }
    public virtual void OnNextTurn(Tile tile)
    {
      
    }

    public virtual void OnAttack(Creature Aim, ref int damage, ref string type, ref float armorignore, Creature attacker)
    {
 
    }

    public virtual void OnDamageDealt(Creature aim, int damage,Creature attack,Item weapon = null)
    {

    }

    public virtual void OnDefence(Creature Attacker, Creature Defender,ref int damage, ref string type, float armorignore)
    {

 
    }

    public virtual void OnSelection(Tile t)
    {

    }


    public virtual void OnEnd()
    {

    }

    public virtual void OnDeath(Creature owner)
    {
    }

    public virtual void OnKill()
    {

    }
    public virtual void OnMove()
    {


    }
   public virtual void OnStep(Creature cr)
    {

    }

    public virtual void Launch(MonoBehaviour b)
    {

    }

    public virtual void OnNextTurn()
    {
        if (!enabled)
            return;
        if(Timed)
        Turns--;
        if (Turns <= 0)
            Dispose();
          

    }

    public virtual void Dispose()
    {
        if (AssocCreat != null)
            AssocCreat.RemoveEffect(this);
        Debug.Log("Disposed" + this.GetType());
    }

    public virtual void LoadFormat(string info)
    {

    }

    // Use this for initialization
   protected virtual void Start () {

       
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



    public static List<Effect> Parse(string info)
    {
        List<Effect> output = new List<Effect>();
       
        string[] effects = info.Split('@');
        Debug.Log(info);
        foreach(string ef in effects)
        {

            

            string id = ef.Split('|')[0];
           
            switch(id)
            {

                case "areaeffect":
                    output.Add(new AreaEffect(ef));
                    break;
                case "bolteffect":
                    output.Add(new BoltEffect(ef));
                    break;
                case "consumable":
                    output.Add(new Consumable(ef));
                    break;
                case "food":
                    output.Add(new Food(ef));
                    break;
                case "dummyuse":
                    output.Add(new PlaceEffect(ef));
                    break;
                case "damageovertime":
                    output.Add(new Poisoned(ef));
                    break;
                case "regeneration":
                    output.Add(new Regeneration(ef));
                    break;
                case "heal":
                    output.Add(new Heal(ef));
                    break;
                case "hpupgrade":
                    output.Add(new HPUpgrade(ef));
                    break;

                case "resisstance":
                    output.Add(new Resisstance(ef));
                    break;
                case "stun":
                    output.Add(new Stun(ef));
                    break;
                case "thorns":
                    output.Add(new Thorns(ef));
                    break;



            }



        }


        return output;

    }

    
}
