﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : Effect {
    public int turns = 3;
    public int Damage = 1;
    bool Stop = false;
    public AnimatedSprite spr;
    public Texture2D txt;
    public bool Launched = false;





    public Food(string info)
    {

        LoadFormat(info);
    }

    public Food(int turns, int DMG, Texture2D t)
    {
        this.turns = turns;
        Damage = DMG;
        txt = t;

    }



    public override void OnDefence(Creature Attacker, Creature Defender ,ref int damage, ref string type, float armorignore)
    {
        if(spr!=null)
        GameObject.Destroy(spr.gameObject);
        base.OnDefence(Attacker, Defender, ref damage, ref type, armorignore);
  
        Stop = true;
    }

    public override void OnNextTurn(Creature owner)
    {
        if (enabled)
        {
            Debug.Log(turns);
            if (!Stop)
            {


                if (txt != null)
                    if (spr == null)
                        spr = Core.Main.CreateFollowingSprite(txt, owner.gameObject);
            }
            if (Stop)
            {
                owner.RemoveEffect(this);
                if (spr != null)
                    GameObject.Destroy(spr.gameObject);
            }
            if (!Stop)
            {
                turns--;
                owner.Health += Damage;
                if (owner.Health > owner.MaxHealth)
                    owner.Health = owner.MaxHealth;
                if (turns <= 0)
                {
                    owner.RemoveEffect(this);
                    GameObject.Destroy(spr.gameObject);
                }


                base.OnNextTurn(owner);
            }
        }

    }


    public override string SaveFormat()
    {
        string output = "food|" + turns.ToString() + "|" + Damage.ToString() + "|" + txt.name;
        return output;
    }
    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        turns = int.Parse(data[1]);
        Damage = int.Parse(data[2]);
        txt = Core.GetTextureFromFile(data[3]);

    }
}
