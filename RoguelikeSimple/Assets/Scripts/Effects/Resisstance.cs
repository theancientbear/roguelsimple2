﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resisstance : Effect {
    public string DamageType;
    public float Decrease;


    public Resisstance(string info)
    {

        LoadFormat(info);
    }

    public  Resisstance(string dmgtype, float decr)
    {
        DamageType = dmgtype;
        Decrease = decr;
        Debug.Log("Resist created;");
    }

    public override void OnDefence(Creature Attacker,Creature Defender, ref int damage, ref string type, float armorignore = 0)
    {

        Debug.Log("Resist defence");
        if (type == DamageType)
            damage -= (int)((float)damage * Decrease/100);
        if (damage < 0)
            damage = 0;
        
        base.OnDefence(Attacker,Defender, ref damage, ref type, armorignore);
    }


    public override void Dispose()
    {
        base.Dispose();
        enabled = false;
        //this.AssocCreat.effects.Remove(this);
    }


    public override string SaveFormat()
    {
        string output = "resisstance|" + DamageType + "|" + Decrease.ToString();
        return output;
    }


    public override void LoadFormat(string info)
    {
        string[] data = info.Split('|');
        DamageType = data[1];
        Decrease = float.Parse(data[2]);
    }

}
