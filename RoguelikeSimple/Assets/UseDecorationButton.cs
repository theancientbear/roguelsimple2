﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseDecorationButton : MonoBehaviour {


    public Decoration Owner;


    public void Use()
    {
        Owner.Use();
    }
}
