﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructTimed : MonoBehaviour {

    public float period;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


        period -= Time.deltaTime;
        if (period <= 0)
            Destroy(gameObject);
	}
}
