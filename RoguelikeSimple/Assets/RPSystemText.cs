﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RPSystemText : MonoBehaviour {


   

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        string s = "HP: " + Player.Main.Health.ToString() + "/" +
            Player.Main.MaxHealth.ToString() + "(" + Player.Main.BaseHP.ToString() + ") \n" +
            "Strength: " + Player.Main.Strength.ToString() + "(" + Player.Main.StrExp.ToString() + "/10000)\n"
            +
            "Agility: " + Player.Main.Agility.ToString() + "(" + Player.Main.AgiExp.ToString() + "/10000)\n"
            +
            "Intellect: " + Player.Main.Wisdom.ToString() + "(" + Player.Main.IntExp.ToString() + "/10000)\n" +
            "Effects: "

            ;

        foreach (Effect ef in Player.Main.effects)
            s += "  " + ef.GetType().ToString();

        s += "\n";


        for(int i =0; i<Player.Main.Equipment.Length;i++)
        {
            if (Player.Main.Equipment[i] != null)
            {
                s += Player.Main.Equipment[i].Name + ": ";
                foreach (Effect ef in Player.Main.Equipment[i].effects)
                    if(ef!=null)
                    s += "  " + ef.SaveFormat();
                s += "\n";
            }

        }
        this.GetComponent<Text>().text = s;

	}
}
