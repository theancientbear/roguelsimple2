﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileToggle : MonoBehaviour {


    public TileData AttachedTile;
    public int TileNumber;
    public void OnTileClicked()
    {
        EditorCore.Main.SelectedTileNumber = TileNumber;
        EditorCore.Main.SelectedTile = AttachedTile;
    }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
