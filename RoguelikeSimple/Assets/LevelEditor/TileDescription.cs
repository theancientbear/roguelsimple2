﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TileDescription : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        this.GetComponentInChildren<Text>().text = EditorCore.Main.SelectedTile.Sprite + "\n" + "\n" + "\n" + EditorCore.Main.SelectedTile.Type;
		
	}
}
