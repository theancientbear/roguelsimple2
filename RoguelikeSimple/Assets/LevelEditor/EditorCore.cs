﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorCore : MonoBehaviour {


    public static EditorCore Main;
    public TileData SelectedTile;
    public List<Material> MaterialCollection;
    public int SelectedTileNumber;
    public List<TileData> TileDataCollection;
	// Use this for initialization
	void Start () {
       Main = this;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
