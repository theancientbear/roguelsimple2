﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SQLite;

public class TileSetChoose : MonoBehaviour
{


    public class Table
    {

        public string name { get; set; }

    }

    public GameObject TogglePrefab;
    public Transform Anchor;
    public RectTransform Context;
    public List<GameObject> Toggles = new List<GameObject>();


    // Use this for initialization
    void Start()
    {

        Dropdown.OptionData t = this.GetComponent<Dropdown>().options[0];
        List<Dropdown.OptionData> TilesetData = new List<Dropdown.OptionData>();
        using (var TileDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/TileSets.sqlite"))
        {

            List<Table> datasets = TileDB.Query<Table>("SELECT name FROM sqlite_master WHERE type='table'");
            this.GetComponent<Dropdown>().options[0].text = datasets[0].name;
            this.GetComponent<Dropdown>().ClearOptions();

            foreach (Table i in datasets)
            {
                Dropdown.OptionData t1 = new Dropdown.OptionData();
                t1.text = i.name;
                TilesetData.Add(t1);
            }

            this.GetComponent<Dropdown>().AddOptions(TilesetData);


        }

        OnTilesetChanged();
        //    this.GetComponent<Dropdown>().options
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void OnTilesetChanged()
    {
        using (var TileDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/TileSets.sqlite"))
        {
            List<TileData> tiles = TileDB.Query<TileData>("SELECT * FROM " + this.GetComponent<Dropdown>().options[this.GetComponent<Dropdown>().value].text);

            foreach (GameObject i in Toggles)
            {
                Destroy(i.gameObject);
                
            }
            EditorCore.Main.MaterialCollection.Clear();
            Toggles.Clear();
            for (int i = 0; i < tiles.Count; i++)
            {

                Toggles.Add(Instantiate(TogglePrefab, Anchor.position+i%4*Vector3.right*47 + i/4*Vector3.down*47, TogglePrefab.transform.rotation, Anchor) as GameObject);
                string spr = tiles[i].Sprite;
                Toggles[i].GetComponent<TileToggle>().AttachedTile = tiles[i];
                Toggles[i].GetComponent<Toggle>().group = this.GetComponent<ToggleGroup>();

                Texture2D txt;


                EditorCore.Main.MaterialCollection.Add(Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial));
                EditorCore.Main.MaterialCollection[i].name = tiles[i].Sprite;
           //     EditorCore.Main.MaterialCollection[i].mainTexture = Core.GetTextureFromFile(tiles[i].Sprite);

                if (spr.StartsWith("anim:"))
                {
                    EditorCore.Main.MaterialCollection[i].name = tiles[i].Sprite;
                    EditorCore.Main.MaterialCollection[i].mainTexture = Core.GetTextureFromFile(tiles[i].Sprite.Replace("anim:",""));
                    txt = Core.GetTextureFromFile(tiles[i].Sprite.Replace("anim:",""));
                    Toggles[i].GetComponentInChildren<Image>().overrideSprite = Sprite.Create(txt, new Rect(0, 0, txt.height, txt.height), Vector2.one / 2);
                }
                else
                {
                    EditorCore.Main.MaterialCollection[i].name = tiles[i].Sprite;
                    EditorCore.Main.MaterialCollection[i].mainTexture = Core.GetTextureFromFile(tiles[i].Sprite);
                    txt = Core.GetTextureFromFile(tiles[i].Sprite);
                    Toggles[i].GetComponentInChildren<Image>().overrideSprite = Sprite.Create(txt, new Rect(0, 0, txt.width, txt.height), Vector2.one / 2);
                }

                Toggles[i].GetComponent<TileToggle>().TileNumber = i;
                



            }
            Context.sizeDelta = new Vector2(200, 46 + tiles.Count/5 * 46);
            EditorCore.Main.TileDataCollection = tiles;
            Toggles[0].GetComponent<Toggle>().isOn = true;



        }

    }
}
