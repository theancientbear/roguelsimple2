﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePlacer : MonoBehaviour {

    public LayerMask mask;



    // Use this for initialization
    void Start() {

    }



    // Update is called once per frame
    void Update() {


            RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit,1000, mask))
        {



            Vector3 pos = hit.point + Vector3.one / 2;

            Debug.Log(pos.ToVectorInteger().x.ToString() + ":" + pos.ToVectorInteger().y.ToString() + "/" + pos.x.ToString() + ":" + pos.y.ToString());

            IntegerVector vec = pos.ToVectorInteger();
            if (Input.GetButton("Fire1"))

                Core.Main.AddTile(EditorCore.Main.SelectedTile, vec, EditorCore.Main.MaterialCollection[EditorCore.Main.SelectedTileNumber]);
        }
        else
            Debug.Log("No hit on plane");
        



    }
}
    

