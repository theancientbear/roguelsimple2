﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowEnabler : MonoBehaviour {

	// Use this for initialization
	void Start () {
        this.GetComponent<Renderer>().castShadows = true;
        this.GetComponent<Renderer>().receiveShadows = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
