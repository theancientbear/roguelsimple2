﻿using UnityEngine;
using System.Collections;
using System.Linq;
using SQLite;
using System.Collections.Generic;



public static class RoomGenerator  {


    static List<Material> Floor = new List<Material>();
    static List<Material> Wall = new List<Material>();
    static Material LadderUp;
    static int RoomNumber = 15;
    static int RoomSize = 20;
    static int RoomSizeDelt = 10;
    static int CorrWidth = 1;
    public static LevelData LevelData;

    public static void SetGen(int r, int k, int d, int w)
    {
        RoomNumber = r;
        RoomSize = k;
        RoomSizeDelt = d;
        CorrWidth = w;


    }



    static Tile AddTile(IntegerVector vec, bool Passable, bool Transparent, Material tex)
    {
        Tile t = (GameObject.Instantiate(Core.Main.tileprefab) as GameObject).GetComponent<Tile>();
        t.GetComponent<Renderer>().material = tex;
        Debug.Log("Placed tile at" + vec.x.ToString() + ":" + vec.y.ToString());

        t.Transparent = Transparent;
        t.Passable = Passable;
        Core.MainGrid.InsertTile(vec, t);
        return t;
    }

    public static void GenerateMap(string Tileset, int seed)
    {

        Random.seed = seed;
        using (var ItDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/Floors.sqlite"))
        using (var TileDB = new SQLiteConnection(Application.dataPath + "/StreamingAssets/TileSets.sqlite"))
        {
            //Подгрузка информации о тайлах и спрайтов
            List<FloorItemData> ItemDB = ItDB.Query<FloorItemData>("SELECT * FROM " + LevelData.Itemset);
            List<FloorMobData> MobDB = ItDB.Query<FloorMobData>("SELECT * FROM " + LevelData.Mobset);
            List<TileData> list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'FLOOR'");
            foreach (TileData o in list)
            {
                Material m = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
                m.mainTexture = Core.GetTextureFromFile(o.Sprite);
                Floor.Add(m);
            }
            list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'WALL'");

            foreach (TileData o in list)
            {
                Material m = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
                m.mainTexture = Core.GetTextureFromFile(o.Sprite);
                Wall.Add(m);

            }
            list = TileDB.Query<TileData>("SELECT * FROM " + Tileset + " WHERE Type = 'LADDERUP'");
            LadderUp = Material.Instantiate(Core.Main.tileprefab.GetComponent<Renderer>().sharedMaterial);
            LadderUp.mainTexture = Core.GetTextureFromFile(list[0].Sprite);


            Debug.Log(list.Count);
            IntegerVector prevpos = new IntegerVector(0, 0);
            IntegerVector pos = new IntegerVector(0, 0);
            bool[,] rooms = new bool[50, 50];
            List<IntegerVector> Rooms = new List<IntegerVector>();
            IntegerVector shift = new IntegerVector(25, 25);
            //Непосредственно генерация
            for (int i = 0; i < RoomNumber; i++)
            {

                CreateRoom(pos - new IntegerVector(25,25)* (RoomSize + RoomSizeDelt) * 2, pos - new IntegerVector(25, 25) * (RoomSize + RoomSizeDelt) * 2 + new IntegerVector((int)Random.Range(RoomSize - RoomSizeDelt, RoomSize + RoomSizeDelt), (int)Random.Range(RoomSize - RoomSizeDelt, RoomSize + RoomSizeDelt)));
                Rooms.Add(shift);

                rooms[shift.x,shift.y] = true;
                //ConnectPoints(pos, prevpos,CorrWidth);
                bool foundroom = false;
                Debug.Log("Room created at" + shift.y.ToString() + ":" + shift.x.ToString());
                for (int k = Rooms.Count-1; k>=0;k++)
                {
                    
                    for(int y = 0; y<8;y++)
                    {
                        IntegerVector b = shift + new IntegerVector(Random.Range(-1, 1), Random.Range(-1, 1));
                        if(!rooms[b.x,b.y])
                        {
                           
                            shift = b;
                            foundroom = true;
                            rooms[b.x, b.y] = true;
                            prevpos = Rooms[k]* (RoomSize + RoomSizeDelt)*2;
                            break;
                        }
                        if (foundroom)
                            break;

                    }
                    if (foundroom)
                        break;
                }

               
                pos = shift * (RoomSize + RoomSizeDelt)*2;

               
            }

            ConnectPoints(new IntegerVector(0, 0), prevpos, 2);

            // ConnectPoints(new IntegerVector(4, 4), new IntegerVector(-1, -1), 2);
            GenerateWalls();

        }
    }


    static void ConnectPoints(IntegerVector a, IntegerVector b, int width)
    {

        for (int i = 0; i < (int)Mathf.Abs(a.x - b.x); i++)
        {
            for (int j = 0; j < width; j++)
                AddTile(new IntegerVector(b.x + i * (int)Mathf.Sign(a.x - b.x), a.y + j), true, true, Floor[0]);

        }
        for (int i = 0; i < (int)Mathf.Abs(a.y - b.y); i++)
        {
            for (int j = 0; j < width; j++)
                AddTile(new IntegerVector(b.x + j, b.y + i * (int)Mathf.Sign(a.y - b.y)), true, true, Floor[0]);

        }




    }


    static void CreateRoom(IntegerVector a, IntegerVector b)
    {
        Debug.Log("Startig room generation at" + a.x + ":" + a.y + "  -  " + b.x.ToString() + ":" + b.y.ToString());
        for (int i = a.x; i <  b.x; i++)
        {
            for (int j = a.y; j < b.y; j++)
            {
                AddTile(new IntegerVector(b.x + i, b.y + j), true, true, Floor[0]);
            }

        }

        
    }

    static void GenerateWalls()
    {
        List<IntegerVector> list = Core.MainGrid.PointToTile.Keys.ToList<IntegerVector>();
        foreach (IntegerVector i in list)
        {
            for (int j = -1; j <= 1; j++)
                for (int k = -1; k <= 1; k++)
                    if (!Core.MainGrid.PointToTile.ContainsKey(new IntegerVector(i.x + k, i.y + j)))
                        AddTile(new IntegerVector(i.x + k, i.y + j), false, false, Wall[0]);
        }
    }
}
